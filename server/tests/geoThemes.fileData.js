import tableJSON from './data/tablejson';
import fs from 'fs';
import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import chai, { expect } from 'chai';
import config from '../../config/config';
import { rejectionHandler } from './testTools';


const list = (app, done, tokens) => {
  request(app).get(`/api/geoThemes/datafile/list?f=l1_Y29vcmRzLmNzdg&first_is_head=true`)
  .set('Authorization', `Bearer ${tokens.accessToken}`)
  .expect(httpStatus.OK)
  .then((res) => {
    expect(parseInt(res.body.iTotalRecords)).to.equal(res.body.aaData.length);
    done();
  }).catch(rejectionHandler);
};

const meta = (app, done, tokens) => {
  request(app).get(`/api/geoThemes/datafile/meta?f=l1_bWV0YS50aWY&first_is_head=true`)
  .set('Authorization', `Bearer ${tokens.accessToken}`)
  .expect(httpStatus.OK)
  .then((res) => {
    expect(parseInt(res.body.data.column_count)).to.equal(1040);
    expect(parseInt(res.body.data.row_count)).to.equal(297);
    expect(parseInt(res.body.data.band_count)).to.equal(1);
    done();
  }).catch(rejectionHandler);
};

const info = (app, done, tokens) => {
  if (fs.existsSync('S:/users/data_point.json')) fs.unlinkSync('S:/users/data_point.json');
  request(app).get(`/api/geoThemes/datafile/info?f=l1_ZGF0YV9wb2ludC5zaHA&first_is_head=true`)
  .set('Authorization', `Bearer ${tokens.accessToken}`)
  .expect(httpStatus.OK)
  .then((res) => {
    expect(res.body.columns.length).to.equal(2);
    expect(res.body.columns[0].fieldname).to.equal('ID');
    expect(res.body.columns[1].fieldname).to.equal('geom');
    done();
  }).catch(rejectionHandler);
};

const blFile = (app, done, tokens) => {
  request(app).post('/api/geoThemes/dataset/list?f=185&count_rows=true&unique=motKFp7Nfx&iDisplayStart=0&iDisplayLength=10').send({
    sort: []
  }).set('Authorization', `Bearer ${tokens.accessToken}`)
  .expect(httpStatus.OK)
  .then((res) => {
    let parsedReply = JSON.parse(res.text);
    let rowId = parsedReply.aaData.pop().id;
    if (fs.existsSync('S:/users/saved_theme.json')) fs.unlinkSync('S:/users/saved_theme.json');
    request(app).get(`/api/geoThemes/bl_file?filehesh=l1_dGVzdGltYWdlLmpwZw&dataset_id=185&row_id=${rowId}&fieldname=id`)
    .set('Authorization', `Bearer ${tokens.accessToken}`)
    .expect(httpStatus.OK)
    .then((res) => {
      expect(res.headers['content-disposition']).to.equal('attachment; filename=testimage.jpg');
      done();
    }).catch(rejectionHandler);
  }).catch(rejectionHandler);
};

export default {
  list,
  meta,
  info,
  blFile
}