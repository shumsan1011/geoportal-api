import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import jwt from 'jsonwebtoken';
import chai, { expect } from 'chai';
import bootstrapApp from '../../index';
import app from './../../config/express';
import config from '../../config/config';
import { authenticateTest } from './testTools';

import proxy from './servicesManager.proxy';
import WPSServerBridge from './servicesManager.WPSServerBridge';
import management from './servicesManager.management';

chai.config.includeStack = true;

describe('## Services Manager', () => {
  let tokens = false;

  before(() => bootstrapApp());

  /**
   * Authentication routine.
   *
   * @param {*} done Fired when the test is over
   */
  const authenticationRoutine = (done) => {
    authenticateTest((resultTokens) => {
      tokens = resultTokens;
      setTimeout(done, 1000);
    });
  };

  describe('# CORS-workaround proxy', () => {
    it('authentication', authenticationRoutine);
    it('should be unavaialble for anonymous users', done => proxy.shouldBeUnavailableForAnonymousUsers(app, done, tokens));
    it('should be avaialble for authenticated users', done => proxy.shouldBeAvailableForAuthenticatedUsers(app, done, tokens));
    it('should return non-JSON content', done => proxy.shouldReturnNonJSONContent(app, done, tokens));
    it('should process non-200 HTTP codes', done => proxy.shouldProcessNon200HTTPCodes(app, done, tokens));
  });

  describe('# WPS server bridge', () => {
    it('authentication', authenticationRoutine);
    it('should process GetCapabilities request', done => WPSServerBridge.shouldProcessGetCapabilities(app, done, tokens));
    it('should process DescribeProcess request', done => WPSServerBridge.shouldProcessDescribeProcess(app, done, tokens));
    it('should execute scenarios', done => WPSServerBridge.shouldExecuteScenarios(app, done, tokens));
  });

  describe('# Services and scenarios management', () => {
    it('authentication', authenticationRoutine);
    it('should register service', done => management.shouldRegisterService(app, done, tokens));
    it('should register scenario', done => management.shouldRegisterScenario(app, done, tokens));
    it('should register complex scenario', done => management.shouldRegisterComplexScenario(app, done, tokens));
    it('should execute service', done => management.shouldExecuteService(app, done, tokens));
    it('should execute scenario', done => management.shouldExecuteScenario(app, done, tokens));
    it('should execute complex scenario', done => management.shouldExecuteComplexScenario(app, done, tokens));
    it('should check if scenario can be executed in current environment', done => management.shouldCheckIfScenarioCanBeExecutedInCurrentEnvironment(app, done, tokens));
    it('should update service', done => management.shouldUpdateService(app, done, tokens));
    it('should update scenario', done => management.shouldUpdateScenario(app, done, tokens));
    it('should get info about service or scenario', done => management.shouldGetInfoAboutServiceOrScenario(app, done, tokens));
  });
});
