var MailParser = require("mailparser-mit").MailParser;

import POP3Client from 'poplib';
import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import jwt from 'jsonwebtoken';
import chai, { expect } from 'chai';
import app from './../../config/express';
import config from '../../config/config';

/**
 * Authenticates via REST API and returns access tokens
 * 
 * @param {Function} onAuthenticationComplete Authentication complete callback
 */
const authenticateTest = (onAuthenticationComplete) => {
  request(app).post('/api/auth/login').send({
    username: 'admin',
    password: '511396ok'
  })
  .expect(httpStatus.OK)
  .then((res) => {
    expect(res.body).to.have.property('accessToken');
    expect(res.body).to.have.property('refreshToken');
    jwt.verify(res.body.accessToken, config.jwtSecret, (err, decoded) => {
      expect(decoded.username).to.equal('admin');
      onAuthenticationComplete(res.body);
    });
  });
}

/**
 * Fetches last emails from the specified mailbox
 * 
 * @param {*} host 
 * @param {*} port 
 * @param {*} username 
 * @param {*} password 
 * 
 * @return {Promise}
 */
const fetchEmail = (host, port, username, password) => {
  let result = new Promise((resolve, reject) => {

    var client = new POP3Client(port, host, {
      tlserrs: false,
      enabletls: false,
      debug: false
    });

    client.on("error", function(err) {
      if (err.errno === 111) console.log("Unable to connect to server");
      else console.log("Server error occurred");
      console.log(err);
    });

    client.on("connect", function() {
      console.log("CONNECT success");
      client.login(username, password);
    });

    client.on("invalid-state", function(cmd) {
      console.log("Invalid state. You tried calling " + cmd);
    });

    client.on("locked", function(cmd) {
      console.log("Current command has not finished yet. You tried calling " + cmd);
    });

    client.on("login", function(status, rawdata) {
      if (status) {
        console.log("LOGIN/PASS success", rawdata);
        client.list();
      } else {
        console.log("LOGIN/PASS failed");
        client.quit();
      }
    });

    client.on("list", function(status, msgcount, msgnumber, data, rawdata) {
      if (status === false) {
        console.log("LIST failed");
        client.quit();
      } else {
        console.log("LIST success with " + msgcount + " element(s), current one " + msgnumber);
        if (msgcount > 0) {
          client.retr(1);
        } else {
          client.quit();
        }
      }
    });

    client.on("retr", function(status, msgnumber, data, rawdata) {
      if (status === true) {
        console.log("RETR success for msgnumber " + msgnumber);

        var mailparser = new MailParser();
        mailparser.on("end", function(mail_object){
          resolve(mail_object);
        });
      
        mailparser.write(rawdata);
        mailparser.end();

        client.dele(msgnumber);
        client.quit();
      } else {
        console.log("RETR failed for msgnumber " + msgnumber);
        client.quit();
      }
    });

    /*
    let imap = new Imap({ user, password, host, port, tls: false });
    
    const openInbox = (cb) => {
      imap.openBox('INBOX', true, cb);
    };

    let emails = [];
    imap.once('ready', () => {
      console.log('Connection to IMAP server was created');
    });

    imap.once('mail', () => {
      console.log('New mail arrived, checking');
      openInbox((err, box) => {
        if (err) throw err;
        imap.search([ 'UNSEEN' ], (err, results) => {
          var f = imap.fetch(results, {
            bodies: ['HEADER.FIELDS (FROM SUBJECT DATE)','TEXT'],
            struct: true
          });

          f.on('message', (msg, seqno) => {
            let email = {
              headers: false,
              body: false
            };

            msg.on('body', (stream, info) => {
              var buffer = '';
              stream.on('data', (chunk) => {
                buffer += chunk.toString('utf8');
              });

              stream.once('end', () => {
                if (info.which !== 'TEXT') {
                  email.headers = Imap.parseHeader(buffer);
                } else {
                  email.body = buffer;
                }
              });
            });

            msg.once('end', () => {
              emails.push(email);
            });
          });

          f.once('error', (err) => reject(err));

          f.once('end', () => {
            imap.end();
            resolve(emails.pop());
          });
        });
      });
    });

    imap.once('error', function(err) {
      console.log(err);
    });

    imap.connect();

    */
  });

  return result;
};

/**
 * Handles test rejection cases
 * 
 * @param {*} err Error object
 * @param {*} data Optional data
 */
const rejectionHandler = (err, data) => {
  console.log(err, data);
  throw err;
};

export default {
  authenticateTest,
  rejectionHandler,
  fetchEmail
}