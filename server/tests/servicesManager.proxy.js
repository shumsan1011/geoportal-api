import fs from 'fs';
import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import chai, { expect } from 'chai';
import config from '../../config/config';
import { rejectionHandler } from './testTools';

const shouldBeUnavailableForAnonymousUsers = (app, done, tokens,) => {
  request(app).get('/api/servicesManager/proxy?url=https://jsonplaceholder.typicode.com/posts/1')
  .expect(httpStatus.FORBIDDEN)
  .then(() => {
    done();
  }).catch(rejectionHandler);
};

const shouldBeAvailableForAuthenticatedUsers = (app, done, tokens) => {
  request(app).get('/api/servicesManager/proxy?url=https://jsonplaceholder.typicode.com/posts/1')
  .set('Authorization', `Bearer ${tokens.accessToken}`)
  .expect(httpStatus.OK)
  .then((res) => {
    expect(res.body.id).to.equal(1);
    done();
  }).catch(rejectionHandler);
};

const shouldReturnNonJSONContent = (app, done, tokens) => {
  request(app).get('/api/servicesManager/proxy?url=https://www.w3schools.com/xml/note.xml')
  .set('Authorization', `Bearer ${tokens.accessToken}`)
  .set('Content-Type', 'text/xml')
  .expect(httpStatus.OK)
  .then((res) => {
    expect(res.headers).to.have.property('content-type', 'text/xml');
    expect(res.text).to.contain('<?xml');
    done();
  }).catch(rejectionHandler);
};

const shouldProcessNon200HTTPCodes = (app, done, tokens) => {
  request(app).get('/api/servicesManager/proxy?url=https://jsonplaceholder.typicode.com/notexisting')
  .set('Authorization', `Bearer ${tokens.accessToken}`)
  .expect(httpStatus.NOT_FOUND)
  .then(() => {
    done();
  }).catch(rejectionHandler);
};

export default {
  shouldBeUnavailableForAnonymousUsers,
  shouldBeAvailableForAuthenticatedUsers,
  shouldReturnNonJSONContent,
  shouldProcessNon200HTTPCodes
}