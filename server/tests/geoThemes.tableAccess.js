import tableJSON from './data/tablejson';
import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import chai, { expect } from 'chai';
import config from '../../config/config';
import { rejectionHandler } from './testTools';

const guestShouldAccessPublicTable = (app, done, tokens) => {
  request(app).get('/api/geoThemes/dataset/list?f=100&iDisplayStart=0&iDisplayLength=100&ajax=true').send({
  }).expect(httpStatus.OK)
  .then((res) => {
    expect(res.body.aaData.length).to.equal(5);
    done();
  });
};

const guestShouldNotAccessSystemTable = (app, done, tokens) => {
  request(app).get('/api/geoThemes/dataset/list?f=9&iDisplayStart=0&iDisplayLength=100&ajax=true').send({
  }).expect(httpStatus.FORBIDDEN)
  .then((res) => {
    done();
  });
};

export default {
  guestShouldAccessPublicTable,
  guestShouldNotAccessSystemTable
};