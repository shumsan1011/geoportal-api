import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import jwt from 'jsonwebtoken';
import chai, { expect } from 'chai';
import bootstrapApp from '../../index';
import app from './../../config/express';
import config from '../../config/config';
import { authenticateTest, rejectionHandler } from './testTools';

chai.config.includeStack = true;

describe('## General test', () => {
  let tokens = false;

  before(() => bootstrapApp());

  /**
   * Authentication routine.
   *
   * @param {*} done Fired when the test is over
   */
  const authenticationRoutine = (done) => {
    authenticateTest((resultTokens) => {
      tokens = resultTokens;
      setTimeout(done, 1000);
    });
  };

  describe('# non-existing routes processing', () => {  
    it('authentication', authenticationRoutine);
    it('should return 404 for non-existing route', (done) => {
      request(app).get('/someRoute_xvsdgdgt34')
      .expect(httpStatus.NOT_FOUND)
      .then(() => {
        request(app).get('/someRoute_xvsdgdgt34')
        .expect(httpStatus.NOT_FOUND)
        .then(() => {
          request(app).get('/someRoute_dfgfhftyh34')
          .set('Authorization', `Bearer ${tokens.accessToken}`)
          .expect(httpStatus.NOT_FOUND)
          .then(() => {
            done();
          }).catch(rejectionHandler);
        }).catch(rejectionHandler);
      }).catch(rejectionHandler);
    });
  });

  describe('# health check', () => {  
    it('should perform health check', (done) => {
      request(app).get('/api/health-check')
      .expect(httpStatus.OK)
      .then(() => {
        done();
      }).catch(rejectionHandler);
    });
  });
});
