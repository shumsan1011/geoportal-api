import fs from 'fs';
import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import chai, { expect } from 'chai';
import config from '../../config/config';
import { rejectionHandler } from './testTools';

const list = (app, done, tokens) => {
  request(app).get(`/api/geoThemes/user_list/list`)
  .expect(httpStatus.OK)
  .then((res) => {
    expect(res.body.aaData[0].email === undefined).to.equal(true);

    request(app).get(`/api/geoThemes/user_list/list`)
    .set('Authorization', `Bearer ${tokens.accessToken}`)
    .expect(httpStatus.OK)
    .then((res) => {
      expect(res.body.aaData[0].email.length > 0).to.equal(true);
      done();
    }).catch(rejectionHandler);
  }).catch(rejectionHandler);
};

const listWithCriteria = (app, done, tokens) => {
  request(app).get(`/api/geoThemes/user_list/list?f_username=adm`)
  .set('Authorization', `Bearer ${tokens.accessToken}`)
  .expect(httpStatus.OK)
  .then((res) => {
    expect(parseInt(res.body.iTotalRecords)).to.equal(1);
    done();
  }).catch(rejectionHandler);
};

export default {
  list,
  listWithCriteria
}