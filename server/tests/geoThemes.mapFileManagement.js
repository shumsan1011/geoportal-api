import fs from 'fs';
import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import jwt from 'jsonwebtoken';
import chai, { expect } from 'chai';
import config from '../../config/config';
import { authenticateTest, rejectionHandler } from './testTools';

chai.config.includeStack = true;

const shouldGenerateMAPFileForTheme = (app, done, tokens) => {
  request(app).post('/api/geoThemes/create_map_for_field')
  .send({
    metaid: 20,
    unique: 'abc123',
    fieldname: 'geom',
  })
  .set('Authorization', `Bearer ${tokens.accessToken}`)
  .expect(httpStatus.OK)
  .then((res) => {
    expect(res.body.status).to.equal('success');
    expect(fs.existsSync(res.body.wms_link.split('=')[1].replace('/s', 'S:'))).to.equal(true);
    done();
  });
}

const shouldGenerateMAPFileForFile = (app, done, tokens) => {
  request(app).post('/api/geoThemes/createmap')
  .send({
    filehesh: 'l1_bWV0YS50aWY',
    unique: 'asdasdasd',
    from_color: '#000000',
    to_color: '#FFFFFF'
  })
  .set('Authorization', `Bearer ${tokens.accessToken}`)
  .expect(httpStatus.OK)
  .then((res) => {
    expect(res.body.status).to.equal('success');
    expect(fs.existsSync(res.body.mapfilesobject.wms_link.split('=')[1].replace('/s', 'S:'))).to.equal(true);
    done();
  }).catch(rejectionHandler);
}

export default {
  shouldGenerateMAPFileForTheme,
  shouldGenerateMAPFileForFile
}
