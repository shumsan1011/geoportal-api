import fs from 'fs';
import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import chai, { expect } from 'chai';
import config from '../../config/config';
import { rejectionHandler } from './testTools';

let serviceId = false;
let scenarioId = false;
let complexScenarioId = false;
const runId = (+ new Date());

const shouldRegisterService = (app, done, tokens) => {
  request(app).post('/api/servicesManager/create')
  .send({
    data: `{"name":"test_wps_service${runId}",
"description":"Test WPS service description",
"type":"wps",
"params":[
  {"fieldname":"number1","title":"number1","description":"input field description","visible":true,"type":"string","widget":{"name":"edit","properties":{"tsvector":""}}},
  {"fieldname":"number2","title":"number2","description":"input field description","visible":true,"type":"string","widget":{"name":"edit","properties":{"tsvector":""}}}
],
"output_params":[
  {"fieldname":"ResultNumber","title":"output field","description":"output field description","visible":true,"type":"string","widget":{"name":"edit","properties":{"tsvector":""}}}
],
"servers":[{"host":"84.237.16.46","port":"8800","path":"/cgi-bin/wps/zoo_loader.cgi?","cpunumber":"","cpufrequency":"","ram":"","bandwidth":"","method":"simplesummator"}],
"method":"simplesummator",
"map_reduce_specification":""}`
  })
  .set('Authorization', `Bearer ${tokens.accessToken}`)
  .expect(httpStatus.OK)
  .then((res) => {
    expect(res.body.status).to.equal("success");
    expect(res.body.id > 0).to.equal(true);
    serviceId = res.body.id;
    done();
  }).catch(rejectionHandler);
};

const shouldRegisterScenario = (app, done, tokens) => {
  request(app).post('/api/servicesManager/create')
  .send({
    data: `{"name":"test_service${runId}",
"description":"test service description",
"type":"js",
"funcbody":"function test_service(input, mapping){XXNEWLINEXX    var i; XXNEWLINEXX    mapping.output.set(1);XXNEWLINEXX}",
"params":[{"fieldname":"input","title":"input field","description":"input field description","visible":true,"type":"string","widget":{"name":"edit","properties":{"tsvector":""}}}],
"output_params":[{"fieldname":"output","title":"output field","description":"output field description","visible":true,"type":"string","widget":{"name":"edit","properties":{"tsvector":""}}}],
"map_reduce_specification":""}`
  })
  .set('Authorization', `Bearer ${tokens.accessToken}`)
  .expect(httpStatus.OK)
  .then((res) => {
    expect(res.body.status).to.equal("success");
    expect(res.body.id > 0).to.equal(true);
    scenarioId = res.body.id;
    done();
  }).catch(rejectionHandler);
};

const shouldRegisterComplexScenario = (app, done, tokens) => {
  request(app).post('/api/servicesManager/create')
  .send({
    data: `{"name":"test_complex_scenario${runId}",
"description":"test complex scenario description",
"type":"js",
"funcbody":"function test_complex_scenario(input, mapping){  var result1 = new ValueStore();  var result2 = new ValueStore();  simplesummator({    number1: 1,    number2: 2,    numbernotrequired: 0  }, {    ResultNumber: result1  });  simplesummator_first_copy({    number1: 1,    number2: 2,    numbernotrequired: 0  }, {    ResultNumber: result2  });  if (result1.get() > 0 && result2.get() > 0) {    simplesummator_second_copy({      number1: result1,      number2: result2,      numbernotrequired: 0    }, {      ResultNumber: mapping.output    });     } else {    mapping.output.set(-1);  }}",
"params":[{"fieldname":"input","title":"input field","description":"input field description","visible":true,"type":"string","widget":{"name":"edit","properties":{"tsvector":""}}}],
"output_params":[{"fieldname":"output","title":"output field","description":"output field description","visible":true,"type":"string","widget":{"name":"edit","properties":{"tsvector":""}}}],
"map_reduce_specification":""}`
  })
  .set('Authorization', `Bearer ${tokens.accessToken}`)
  .expect(httpStatus.OK)
  .then((res) => {
    expect(res.body.status).to.equal("success");
    expect(res.body.id > 0).to.equal(true);
    complexScenarioId = res.body.id;
    done();
  }).catch(rejectionHandler);
};

const shouldExecuteService = (app, done, tokens) => {
  request(app).get(`/api/servicesManager/execute?id=${serviceId}&inputparams=${encodeURIComponent('{"number1":"2","number2":"3"}')}&outputparams=${encodeURIComponent('{}')}`)
  .set('Authorization', `Bearer ${tokens.accessToken}`)
  .expect(httpStatus.OK)
  .then((res) => {
    expect(res.body.status).to.equal("executing");
    let instanceId = res.body.data;
    request(app).get(`/api/servicesManager/consoleoutput?id=${instanceId}`)
    .set('Authorization', `Bearer ${tokens.accessToken}`)
    .expect(httpStatus.OK)
    .then((res) => {
      expect(res.body.data.lines[0].status).to.equal("METHOD_EXAMPLE_CREATED");
      setTimeout(() => {
        request(app).get(`/api/servicesManager/consoleoutput?id=${instanceId}`)
        .set('Authorization', `Bearer ${tokens.accessToken}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.data.lines[0].status).to.equal("METHOD_EXAMPLE_SUCCEEDED");
          expect(res.body.data.lines[0].result).to.contain(`{"ResultNumber":"5"}`);
          done();
        });
      }, 7000);
    });
  }).catch(rejectionHandler);
};

const shouldExecuteScenario = (app, done, tokens) => {
  request(app).get(`/api/servicesManager/execute?id=${scenarioId}&inputparams=${encodeURIComponent('{"input":"1"}')}&outputparams=${encodeURIComponent('{}')}`)
  .set('Authorization', `Bearer ${tokens.accessToken}`)
  .expect(httpStatus.OK)
  .then((res) => {
    expect(res.body.status).to.equal("executing");
    let instanceId = res.body.data;
    request(app).get(`/api/servicesManager/consoleoutput?id=${instanceId}`)
    .set('Authorization', `Bearer ${tokens.accessToken}`)
    .expect(httpStatus.OK)
    .then((res) => {
      expect(res.body.data.lines[0].status).to.equal("METHOD_EXAMPLE_CREATED");
      setTimeout(() => {
        request(app).get(`/api/servicesManager/consoleoutput?id=${instanceId}`)
        .set('Authorization', `Bearer ${tokens.accessToken}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.data.lines[0].status).to.equal("METHOD_EXAMPLE_SUCCEEDED");
          expect(res.body.data.lines[0].result).to.contain(`{"output":"1"}`);
          done();
        });
      }, 4000);
    });
  }).catch(rejectionHandler);
};


const shouldExecuteComplexScenario = (app, done, tokens) => {
  request(app).get(`/api/servicesManager/execute?id=${complexScenarioId}&inputparams=${encodeURIComponent('{"input":"1"}')}&outputparams=${encodeURIComponent('{}')}`)
  .set('Authorization', `Bearer ${tokens.accessToken}`)
  .expect(httpStatus.OK)
  .then((res) => {
    expect(res.body.status).to.equal("executing");
    let instanceId = res.body.data;
    request(app).get(`/api/servicesManager/consoleoutput?id=${instanceId}`)
    .set('Authorization', `Bearer ${tokens.accessToken}`)
    .expect(httpStatus.OK)
    .then((res) => {
      expect(res.body.data.lines[0].status).to.equal("METHOD_EXAMPLE_CREATED");
      setTimeout(() => {
        request(app).get(`/api/servicesManager/consoleoutput?id=${instanceId}`)
        .set('Authorization', `Bearer ${tokens.accessToken}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.data.lines[0].status).to.equal("METHOD_EXAMPLE_SUCCEEDED");
          expect(res.body.data.lines[0].result).to.contain(`{"output":"6"}`);
          done();
        });
      }, 10000);
    });
  }).catch(rejectionHandler);
};


const shouldCheckIfScenarioCanBeExecutedInCurrentEnvironment = (app, done, tokens) => {
  request(app).post('/api/servicesManager/create')
  .send({
    data: `{"name":"test_environment_check${runId}",
"description":"test environment check",
"type":"js",
"funcbody":"function test_environment_check(input, mapping){  var result1 = new ValueStore();  var result2 = new ValueStore();  simplesummator({    number1: 1,    number2: 2,    numbernotrequired: 0  }, {    ResultNumber: result1  });  simplesummator_third_copy({    number1: 1,    number2: 2,    numbernotrequired: 0  }, {    ResultNumber: result2  });  if (result1.get() > 0 && result2.get() > 0) {    simplesummator_second_copy({      number1: result1,      number2: result2,      numbernotrequired: 0    }, {      ResultNumber: mapping.output    });     } else {    mapping.output.set(-1);  }}",
"params":[{"fieldname":"input","title":"input field","description":"input field description","visible":true,"type":"string","widget":{"name":"edit","properties":{"tsvector":""}}}],
"output_params":[{"fieldname":"output","title":"output field","description":"output field description","visible":true,"type":"string","widget":{"name":"edit","properties":{"tsvector":""}}}],
"map_reduce_specification":""}`
  })
  .set('Authorization', `Bearer ${tokens.accessToken}`)
  .expect(httpStatus.OK)
  .then((res) => {
    expect(res.body.status).to.equal("success");
    expect(res.body.id > 0).to.equal(true);
    let scenarioId = res.body.id;

    request(app).get(`/api/servicesManager/execute?id=${scenarioId}&inputparams=${encodeURIComponent('{"input":"1"}')}&outputparams=${encodeURIComponent('{}')}`)
    .set('Authorization', `Bearer ${tokens.accessToken}`)
    .expect(httpStatus.OK)
    .then((res) => {
      expect(res.body.status).to.equal("error");
      done();
    }).catch(rejectionHandler);
  }).catch(rejectionHandler);
};


const shouldUpdateService = (app, done, tokens) => {
  request(app).post('/api/servicesManager/create')
  .send({
    data: `{"name":"test_wps_service${runId}",
"previd": ${serviceId},
"description":"Updated test WPS service description",
"type":"wps",
"params":[
  {"fieldname":"input1","title":"number1","description":"input field description","visible":true,"type":"string","widget":{"name":"edit","properties":{"tsvector":""}}},
  {"fieldname":"input2","title":"number2","description":"input field description","visible":true,"type":"string","widget":{"name":"edit","properties":{"tsvector":""}}}
],
"output_params":[
  {"fieldname":"ResultNumber","title":"output field","description":"output field description","visible":true,"type":"string","widget":{"name":"edit","properties":{"tsvector":""}}}
],
"servers":[
  {"host":"84.237.16.46","port":"8800","path":"/cgi-bin/wps/zoo_loader.cgi?","cpunumber":"","cpufrequency":"","ram":"","bandwidth":"","method":"simplesummator"},
  {"host":"84.237.16.47","port":"8800","path":"/cgi-bin/wps/zoo_loader.cgi?","cpunumber":"","cpufrequency":"","ram":"","bandwidth":"","method":"simplesummator"}
],
"method":"simplesummator",
"map_reduce_specification":""}`
  })
  .set('Authorization', `Bearer ${tokens.accessToken}`)
  .expect(httpStatus.OK)
  .then((res) => {
    expect(res.body.status).to.equal("success");
    expect(res.body.id > 0).to.equal(true);
    done();
  }).catch(rejectionHandler);
};

const shouldUpdateScenario = (app, done, tokens) => {
  request(app).post('/api/servicesManager/create')
  .send({
    data: `{"name":"test_service${runId}",
"previd": ${scenarioId},
"description":"updated test service description",
"type":"js",
"funcbody":"function test_service(input, mapping){XXNEWLINEXX    var i; XXNEWLINEXX    mapping.output.set(1);XXNEWLINEXX}",
"params":[
  {"fieldname":"input1","title":"input field 1","description":"input field description","visible":true,"type":"string","widget":{"name":"edit","properties":{"tsvector":""}}},
  {"fieldname":"input2","title":"input field 2","description":"input field description","visible":true,"type":"string","widget":{"name":"edit","properties":{"tsvector":""}}}
],
"output_params":[{"fieldname":"output","title":"output field","description":"output field description","visible":true,"type":"string","widget":{"name":"edit","properties":{"tsvector":""}}}],
"map_reduce_specification":""}`
  })
  .set('Authorization', `Bearer ${tokens.accessToken}`)
  .expect(httpStatus.OK)
  .then((res) => {
    expect(res.body.status).to.equal("success");
    expect(res.body.id > 0).to.equal(true);
    done();
  }).catch(rejectionHandler);
};

const shouldGetInfoAboutServiceOrScenario = (app, done, tokens) => {
  request(app).get('/api/servicesManager/getinfo?id=' + serviceId)
  .set('Authorization', `Bearer ${tokens.accessToken}`)
  .expect(httpStatus.OK)
  .then((res) => {
    expect(res.body.status).to.equal("success");
    expect(res.body.data.id > 0).to.equal(true);
    expect(res.body.data.params.length === 2).to.equal(true);
    expect(res.body.data.params[0].fieldname).to.equal('input1');
    expect(res.body.data.params[1].fieldname).to.equal('input2');
    expect(res.body.data.wpsservers.length).to.equal(2);
    expect(res.body.data.wpsservers[1].host).to.equal('84.237.16.47');

    request(app).get('/api/servicesManager/getinfo?id=10101010')
    .set('Authorization', `Bearer ${tokens.accessToken}`)
    .expect(httpStatus.OK)
    .then((res) => {   
      expect(res.body.status).to.equal("error");
      request(app).get('/api/servicesManager/getinfo?id=' + scenarioId)
      .set('Authorization', `Bearer ${tokens.accessToken}`)
      .expect(httpStatus.OK)
      .then((res) => {
        expect(res.body.data.params.length === 2).to.equal(true);
        expect(res.body.data.params[0].fieldname).to.equal('input1');
        expect(res.body.data.params[1].fieldname).to.equal('input2');

        done();
      });
    });
  }).catch(rejectionHandler);
};

export default {
  shouldRegisterService,
  shouldRegisterScenario,
  shouldRegisterComplexScenario,
  shouldExecuteService,
  shouldExecuteScenario,
  shouldExecuteComplexScenario,
  shouldUpdateService,
  shouldUpdateScenario,
  shouldCheckIfScenarioCanBeExecutedInCurrentEnvironment,
  shouldGetInfoAboutServiceOrScenario
}