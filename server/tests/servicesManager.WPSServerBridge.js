import fs from 'fs';
import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import chai, { expect } from 'chai';
import config from '../../config/config';
import { rejectionHandler } from './testTools';

const shouldProcessGetCapabilities = (app, done, tokens) => {
  request(app).get('/api/servicesManager/wpsserver?request=GetCapabilities&service=WPS')
  .set('Authorization', `Bearer ${tokens.accessToken}`)
  .expect(httpStatus.OK)
  .then((res) => {
    expect(res.headers['content-type']).to.equal('text/xml; charset=utf-8');
    expect(res.text).to.contain("<ows:Identifier>GoogleElevation</ows:Identifier>");
    expect(res.text).to.contain("<ows:Title>Geoportal WPS server</ows:Title>");
    done();
  }).catch(rejectionHandler);
}

const shouldProcessDescribeProcess = (app, done, tokens) => {
  request(app).get('/api/servicesManager/wpsserver?request=DescribeProcess&service=WPS&version=1&identifier=GoogleElevation')
  .set('Authorization', `Bearer ${tokens.accessToken}`)
  .expect(httpStatus.OK)
  .then((res) => {
    expect(res.headers['content-type']).to.equal('text/xml; charset=utf-8');
    expect(res.text).to.contain("<ows:Identifier>coordx</ows:Identifier>");
    expect(res.text).to.contain("<ows:Identifier>coordy</ows:Identifier>");
    expect(res.text).to.contain("<ows:Identifier>elevation</ows:Identifier>");
    done();
  }).catch(rejectionHandler);
}


const shouldExecuteScenarios = (app, done, tokens) => {
  request(app).get('/api/servicesManager/wpsserver?request=Execute&service=WPS&version=1&identifier=GoogleElevation&DataInputs=coordx=54.01;coordy=54.01')
  .set('Authorization', `Bearer ${tokens.accessToken}`)
  .expect(httpStatus.OK)
  .then((res) => {
    expect(res.headers['content-type']).to.equal('text/xml; charset=utf-8');
    expect(res.text).to.contain("<ows:Identifier>elevation</ows:Identifier>");
    expect(res.text).to.contain("<wps:LiteralData>308.472900390625</wps:LiteralData>");
    done();
  }).catch(rejectionHandler);
};

export default {
  shouldProcessGetCapabilities,
  shouldProcessDescribeProcess,
  shouldExecuteScenarios
}