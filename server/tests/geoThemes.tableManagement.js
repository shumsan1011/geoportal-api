import tableJSON from './data/tablejson';
import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import chai, { expect } from 'chai';
import config from '../../config/config';
import { rejectionHandler } from './testTools';

const shouldFetchTheTableDescription = (app, done, tokens) => {
  request(app).get('/api/geoThemes/dataset/list?f=100&iDisplayStart=0&iDisplayLength=100&s_fields=JSON&f_id=20&ajax=true').send({
  }).set('Authorization', `Bearer ${tokens.accessToken}`)
  .expect(httpStatus.OK)
  .then((res) => {
    expect(res.body.aaData.length).to.equal(1);
    expect(res.body.aaData[0].JSON.length > 0).to.be.true;
    done();
  });
};

const shouldAddColumnAndDeleteItAfterwards = (app, done, tokens, testRunId) => {
  request(app).post('/api/geoThemes/processtheme').send({
    tablejson: tableJSON.generateTableJSONWithField(testRunId)
  })
  .set('Authorization', `Bearer ${tokens.accessToken}`)
  .expect(httpStatus.OK)
  .then((res) => {
    request(app).post('/api/geoThemes/dataset/add?f=20').send({
      "document": {
        "dataset_id":20,
        "columns":[],
        "name":"1n",
        "value":"1v",
        "id":"NULL"
      }
    }).set('Authorization', `Bearer ${tokens.accessToken}`)
    .expect(httpStatus.OK)
    .then(() => {
      request(app).post('/api/geoThemes/dataset/list?f=20&count_rows=true&unique=motKFp7Nfx&iDisplayStart=0&iDisplayLength=10').send({
        sort: []
      }).set('Authorization', `Bearer ${tokens.accessToken}`)
      .expect(httpStatus.OK)
      .then((res) => {
        let parsedReply = JSON.parse(res.text);
        let item = parsedReply.aaData.pop();
        expect(item).to.include.all.keys('name', 'value', `testvalue${testRunId}`);

        request(app).post('/api/geoThemes/processtheme').send({
          tablejson: tableJSON.generateTableJSONWithOutField()
        })
        .set('Authorization', `Bearer ${tokens.accessToken}`)
        .expect(httpStatus.OK)
        .then((res) => {
          request(app).post('/api/geoThemes/dataset/list?f=20&count_rows=true&unique=motKFp7Nfx&iDisplayStart=0&iDisplayLength=10').send({
            sort: []
          }).set('Authorization', `Bearer ${tokens.accessToken}`)
          .expect(httpStatus.OK)
          .then((res) => {
            let parsedReply = JSON.parse(res.text);
            let item = parsedReply.aaData.pop();
            expect(item).to.not.have.keys(`value${testRunId}`, 'name');

            done();
          });
        });
      });
    }).catch(rejectionHandler);
  }).catch(rejectionHandler);
};

const shouldCreateDataTableFromJSONDescription = (app, done, tokens, testRunId) => {
  request(app).post('/api/geoThemes/processtheme').send({
    tablejson: tableJSON.generateTableJSONForCreation(testRunId)
  })
  .set('Authorization', `Bearer ${tokens.accessToken}`)
  .expect(httpStatus.OK)
  .then((res) => {
    request(app).get(`/api/geoThemes/dataset/list?f=100&iDisplayStart=0&iDisplayLength=100&s_fields=JSON&f_id=${res.body.data}&ajax=true`).send({
    }).set('Authorization', `Bearer ${tokens.accessToken}`)
    .expect(httpStatus.OK)
    .then((res) => {
      let parsedDescription = JSON.parse(res.body.aaData[0].JSON);
      expect(parsedDescription.columns[0].fieldname).to.equal('name');
      expect(parsedDescription.columns[1].fieldname).to.equal('id');
      expect(parsedDescription.columns[2].fieldname).to.equal('pol');
      done();
    });
  });
};

const shouldGenerateMetaDescriptionForCreatedTable = (app, done, tokens) => {
  request(app).get(`/api/geoThemes/genjson?tablename=public.sample_theme`)
  .set('Authorization', `Bearer ${tokens.accessToken}`)
  .expect(httpStatus.OK)
  .then((res) => {
    expect(res.body.title).to.equal('public.sample_theme');
    expect(res.body.columns.length > 0).to.equal(true);
    done();
  }).catch(rejectionHandler);
};

const shouldReturnListOfTables = (app, done, tokens, testRunId) => {
  request(app).get(`/api/geoThemes/gettables`)
  .set('Authorization', `Bearer ${tokens.accessToken}`)
  .expect(httpStatus.OK)
  .then((res) => {
    let themeExists = false;
    res.body.map(item => {
      if (item.name === 'public.sample_theme') {
        themeExists = true;
        return false;
      }
    });

    expect(themeExists).to.equal(true);
    done();
  }).catch(rejectionHandler);
};

export default {
  shouldFetchTheTableDescription,
  shouldAddColumnAndDeleteItAfterwards,
  shouldCreateDataTableFromJSONDescription,
  shouldGenerateMetaDescriptionForCreatedTable,
  shouldReturnListOfTables
}