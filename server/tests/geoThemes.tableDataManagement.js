import tableJSON from './data/tablejson';
import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import chai, { expect } from 'chai';
import config from '../../config/config';
import { rejectionHandler } from './testTools';

const resetTableToPreviousColumnSet = (app, done, tokens) => {
  request(app).post('/api/geoThemes/processtheme').send({
    tablejson: tableJSON.generateTableJSONOriginal()
  })
  .set('Authorization', `Bearer ${tokens.accessToken}`)
  .expect(httpStatus.OK)
  .then((res) => {
    done();
  });
}

const shouldAddDataToTable = (app, done, tokens, setInsertedId) => {
  request(app).post('/api/geoThemes/dataset/add?f=20').send({
    "document": {
      "dataset_id":20,
      "columns":[],
      "name":"1n",
      "value":"1v",
      "id":"NULL"
    }
  }).set('Authorization', `Bearer ${tokens.accessToken}`)
  .expect(httpStatus.OK)
  .then((res) => {
    expect(res.body.status).to.equal('success');
    expect(parseInt(res.body.id) > 0).to.be.true;
    setInsertedId(res.body.id);
    done();
  });
}

const shouldAddNonStringDataToTable = (app, done, tokens, setInsertedId) => {
  request(app).post('/api/geoThemes/dataset/add?f=20').send({
    "document": {
      "dataset_id":20,
      "name":"2n",
      "value":"2v",
      "geom":"MULTIPOINT(1.2 1.4)",
      "datetime":"2017-01-01 20:20:20"
    }
  }).set('Authorization', `Bearer ${tokens.accessToken}`)
  .expect(httpStatus.OK)
  .then((res) => {
    expect(res.body.status).to.equal('success');
    expect(parseInt(res.body.id) > 0).to.be.true;
    done();
  });
}

const shouldUpdateDataInTable = (app, done, tokens, testRunId, lastInsertedId) => {
  request(app).post('/api/geoThemes/dataset/update?f=20').send({
    "document": {
      "dataset_id": 20,
      "name": "1n updated with testRunId=" + testRunId,
      "f_id": lastInsertedId
    }
  }).set('Authorization', `Bearer ${tokens.accessToken}`)
  .expect(httpStatus.OK)
  .then((res) => {
    expect(res.body.status).to.equal('success');
    done();
  });
}

const shouldSelectDataFromTable = (app, done, tokens, testRunId, lastInsertedId) => {
  request(app).post('/api/geoThemes/dataset/list?f=20&sort=%5B%7B%22fieldname%22:%22value%22,%22dir%22:%22DESC%22%7D%5D&count_rows=true&unique=motKFp7Nfx&iDisplayStart=0&iDisplayLength=10').set('Authorization', `Bearer ${tokens.accessToken}`)
  .expect(httpStatus.OK)
  .then((res) => {
    let parsedReply = JSON.parse(res.text);
    let itemWasFound = false;
    let valueFieldValues = [];
    parsedReply.aaData.map((item) => {
      valueFieldValues.push(item.value);
      if (item.name.indexOf(testRunId) !== -1) {
        itemWasFound = true;
      }
    });

    expect(itemWasFound).to.equal(true);
    request(app).post('/api/geoThemes/dataset/list?f=20&sort=%5B%7B%22fieldname%22:%22value%22,%22dir%22:%22ASC%22%7D%5D&count_rows=true&unique=motKFp7Nfx&iDisplayStart=0&iDisplayLength=10').set('Authorization', `Bearer ${tokens.accessToken}`)
    .expect(httpStatus.OK)
    .then((res) => {
      parsedReply = JSON.parse(res.text);
      expect(valueFieldValues.indexOf(parsedReply.aaData[1].value) <= valueFieldValues.indexOf(parsedReply.aaData[0].value)).to.be.true;
      done();
    });
  });
}

const shouldDeleteItemsFromTable = (app, done, tokens, testRunId, lastInsertedId) => {
  request(app).post('/api/geoThemes/dataset/delete?f=20').send({
    "document": {
      "dataset_id": 20,
      "f_id": lastInsertedId
    }
  }).set('Authorization', `Bearer ${tokens.accessToken}`)
  .expect(httpStatus.OK)
  .then((res) => {
    request(app).post('/api/geoThemes/dataset/list?f=20&count_rows=true&unique=motKFp7Nfx&iDisplayStart=0&iDisplayLength=10').send({
      sort: []
    }).set('Authorization', `Bearer ${tokens.accessToken}`)
    .expect(httpStatus.OK)
    .then((res) => {
      let parsedReply = JSON.parse(res.text);

      let itemWasFound = false;
      parsedReply.aaData.map((item) => {
        if (item.name.indexOf(testRunId) !== -1) {
          itemWasFound = true;
        }
      });

      expect(itemWasFound).to.be.false;
      done();
    });
  });
}

const shouldClearTable = (app, done, tokens) => {
  request(app).post('/api/geoThemes/dataset/clear?f=20').send({})
  .set('Authorization', `Bearer ${tokens.accessToken}`)
  .expect(httpStatus.OK)
  .then((res) => {
    request(app).post('/api/geoThemes/dataset/list?f=20&count_rows=true&unique=motKFp7Nfx&iDisplayStart=0&iDisplayLength=10').send({
      sort: []
    }).set('Authorization', `Bearer ${tokens.accessToken}`)
    .expect(httpStatus.OK)
    .then((res) => {
      let parsedReply = JSON.parse(res.text);
      expect(parseInt(parsedReply.iTotalRecords)).to.equal(0);
      done();
    });
  });
}

const shouldDropTable = (app, done, tokens) => {
  request(app).get('/api/geoThemes/droptable?dataset_id=20')
  .set('Authorization', `Bearer ${tokens.accessToken}`)
  .expect(httpStatus.OK)
  .then((res) => {
    expect(res.body.status).to.equal('success');
    done();
  });
}

export default {
  resetTableToPreviousColumnSet,
  shouldAddDataToTable,
  shouldAddNonStringDataToTable,
  shouldUpdateDataInTable,
  shouldSelectDataFromTable,
  shouldDeleteItemsFromTable,
  shouldClearTable,
  shouldDropTable
}