require('dotenv').config();

import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import jwt from 'jsonwebtoken';
import chai, { expect } from 'chai';
import bootstrapApp from '../../index';
import app from './../../config/express';
import config from '../../config/config';
import { fetchEmail } from './testTools';
import { Error } from 'mongoose';

chai.config.includeStack = true;

const mailConfig = {
  from: 'geoportaltest@alexshumilov.ru',
  pop3: {
    host: 'smtp.mailtrap.io',
    port: 1100,
    secure: false,
    auth: {
      user: '15563f9607df31',
      pass: process.env.GEOPORTAL_TEST_MAIL_PASSWORD
    }
  }
};

const user1 = {
  "document": {
    "dataset_id": 5,
    "id": "NULL",
    "columns": [],
    "about": "Test 1 about",
    "email": mailConfig.from,
    "fullname": "Jordan Belfort",
    "username": "jordanbelfort",
    "password": "12345678",
    "language": "en"
  }
};

let activationEmail = false;

describe('## User management', () => {
  before(() => bootstrapApp());

  let tokens = false;
  describe('# Authentication', () => {
    it('should fail when invalid credentials are provided', (done) => {
      request(app).post('/api/auth/login').send({
        username: 'invalid',
        password: '511396ok'
      }).expect(httpStatus.UNAUTHORIZED)
      .then(() => done());
    });

    it('should return authentication tokens', (done) => {
      request(app).post('/api/auth/login').send({
        username: 'admin',
        password: '511396ok'
      }).expect(httpStatus.OK)
      .then((res) => {
        expect(res.body).to.have.property('accessToken');
        expect(res.body).to.have.property('refreshToken');
        jwt.verify(res.body.accessToken, config.jwtSecret, (err, decoded) => {
          expect(decoded.username).to.equal('admin');
          tokens = res.body;
          done();
        });
      });
    });

    it('should block access to protected routes for unauthenticated users', (done) => {
      request(app).get('/api/auth/random-number').expect(httpStatus.FORBIDDEN)
      .then(() => {
        done();
      });
    });

    it('should allow access to protected route for authenticated users', (done) => {
      request(app).get('/api/auth/random-number')
      .set('Authorization', `Bearer ${tokens.accessToken}`)
      .expect(httpStatus.OK)
      .then((res) => {
        expect(res.body.num > 0).to.equal(true);
        done();
      });
    });
  });

  describe('# User registration', () => {
    const expectedTimeOfMailDelievery = 120000;
    it ('should succeed if the correct data is provided and result in sending an email with activation link', (done) => {
      request(app).post('/api/geoThemes/dataset/add?f=5')
      .send(user1)
      .expect(httpStatus.OK)
      .then((res) => {
        expect(res.body.id > 0).to.equal(true);
        let emailWasSent = new Date();

        fetchEmail(mailConfig.pop3.host, mailConfig.pop3.port,
          mailConfig.pop3.auth.user, mailConfig.pop3.auth.pass).then((email) => {
          let emailWasReceived = new Date(email.headers.date);
          activationEmail = email;
          let delta = (emailWasReceived.getTime() - emailWasSent.getTime());
          if (delta < expectedTimeOfMailDelievery) {
            done();
          } else {
            throw 'Invalid last received email date';
          }
        }).catch(error => {
          console.log(error);
          throw error;
        });
      });
    });

    it ('should fail if the username already exist', (done) => {
      request(app).post('/api/geoThemes/dataset/add?f=5')
      .send(user1)
      .expect(httpStatus.BAD_REQUEST)
      .then((res) => {
        expect(res.body.message).to.equal('Specified column value already exists');
        expect(res.body.description.dbErrorId).to.equal('unique_violation');
        expect(res.body.description.field).to.equal('username');
        done();
      });
    });

    it ('should fail if the email already exist', (done) => {
      let changedUserData = JSON.parse(JSON.stringify(user1));
      changedUserData.document.username = 'someuniqueusername';
      request(app).post('/api/geoThemes/dataset/add?f=5')
      .send(changedUserData)
      .expect(httpStatus.BAD_REQUEST)
      .then((res) => {
        expect(res.body.message).to.equal('Specified column value already exists');
        expect(res.body.description.dbErrorId).to.equal('unique_violation');
        expect(res.body.description.field).to.equal('email');
        done();
      });
    });

    it ('should be able to activate account using correct key', (done) => {
      if (activationEmail !== false) {
        let regex = /activate-account\/(.*?)"/;
        let matched = regex.exec(activationEmail.html);
        let activationCode = matched[1];
        request(app).get(`/api/users/activate?code=${activationCode}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.status).to.equal('success');
          done();
        });
      } else {
        throw Error('First fetch the email with activation link');
      }
    });

    it ('should fail password recovery for invalid email', (done) => {
      request(app).post('/api/users/reset-password')
      .send({ email: user1.document.email + '.au' })
      .expect(httpStatus.BAD_REQUEST)
      .then((res) => {
        expect(res.body.errorId).to.equal(1000);
        done();
      });
    });

    it ('should reset password by sending and email with new password', (done) => {
      request(app).post('/api/users/reset-password')
      .send({ email: user1.document.email })
      .expect(httpStatus.OK)
      .then((res) => {
        expect(res.body.status).to.equal('success');
        fetchEmail(mailConfig.pop3.host, mailConfig.pop3.port,
          mailConfig.pop3.auth.user, mailConfig.pop3.auth.pass).then((email) => {
          let regex = /monospace">(.*?)<\/span/;
          let matched = regex.exec(email.html);
          let newPassword = matched[1];
          request(app).post('/api/auth/login').send({
            username: user1.document.username,
            password: newPassword
          }).expect(httpStatus.OK)
          .then(() => done());
        }).catch(error => {
          console.log(error);
          throw error;
        });
      });
    });
  });
});
