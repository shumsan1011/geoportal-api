const generateTableJSONForCreation = (testRunId) => {
  return `{"tablename":"gis_admin.test${testRunId}","schema":"gis_admin","title":"test${testRunId}","print_template":[{"name":"","body":"l1_"}],
  "form_template":"l1_","columns":[{"fieldname":"name","title":"name","description":"name","visible":true,"type":"string","required":false,
  "widget":{"name":"edit","properties":{"tsvector":""}},"condition":"","conditions":[],"size":"","tablename":"main","sqltablename":"test${testRunId}",
  "sqlname":"name","parentfield":""},{"fieldname":"id","title":"key","description":"id","visible":false,"type":"serial","required":false,"widget":
  {"name":"number","properties":{"before":"","default_value":"","after":"","measure":1,"suffix_user":""}},"condition":"","conditions":[],"size":
  "","tablename":"main","sqltablename":"test${testRunId}","sqlname":"id","parentfield":""},{"fieldname":"pol","title":"pol","description":"pol","visible":
  true,"type":"line","required":false,"widget":{"name":"line","properties":{"mode":"single","editpointmode":"default","label":"","gtype":"line"}},
  "condition":"","conditions":[],"size":"","tablename":"main","sqltablename":"gis_admin.test${testRunId}","sqlname":"pol","parentfield":""},{"fieldname":
  "published","title":"published","description":"published","visible":true,"type":"bool","required":false,"widget":{"name":"boolean","properties":
  {"options":""}},"condition":"","conditions":[],"size":"","tablename":"main","sqltablename":"gis_admin.test${testRunId}","sqlname":"published","parentfield":
  ""},{"fieldname":"sometextfield","title":"sometextfield","description":"sometextfield","visible":true,"type":"string","required":false,"widget":
  {"name":"edit","properties":{"tsvector":""}},"condition":"","conditions":[],"size":"","tablename":"","sqltablename":"gis_admin.test${testRunId}",
  "sqlname":"sometextfield","parentfield":""}],"userlist":[{"userid":"anonym","accesstype":"100"},{"userid":"51554ec594eb74b007000012","accesstype":
  "221"}],"moderated":false,"description":""}`;
}

const generateTableJSONOriginal = () => {
  return `{
    "tablename": "public.sample_theme",
    "dataset_id": 20,
    "schema": "public",
    "description": "",
    "title": "sample_theme",
    "userlist": [{
      "userid":"anonym",
      "accesstype":"222"
    }],
    "columns": [{
      "fieldname": "name",
      "title": "name",
      "description": "name",
      "visible": true,
      "type": "string",
      "required": false,
      "widget": {
        "name": "edit",
        "properties": {
          "tsvector": ""
        }
      },
      "condition": "",
      "conditions": [],
      "size": "",
      "tablename": "main",
      "sqltablename": "sample_theme",
      "sqlname": "name",
      "parentfield": ""
    }, {
      "fieldname": "datetime",
      "title": "datetime",
      "description": "datetime",
      "visible": true,
      "type": "date",
      "required": false,
      "widget": {
        "name": "edit",
        "properties": {
          "tsvector": ""
        }
      },
      "condition": "",
      "conditions": [],
      "size": "",
      "tablename": "main",
      "sqltablename": "sample_theme",
      "sqlname": "datetime",
      "parentfield": ""
    }, {
      "fieldname": "geom",
      "title": "geom",
      "description": "geom",
      "visible": true,
      "type": "point",
      "required": false,
      "widget": {
        "name": "point",
        "properties": {
          "tsvector": ""
        }
      },
      "condition": "",
      "conditions": [],
      "size": "",
      "tablename": "main",
      "sqltablename": "sample_theme",
      "sqlname": "geom",
      "parentfield": ""
    }, {
      "fieldname": "id",
      "title": "key",
      "description": "id",
      "visible": false,
      "type": "serial",
      "required": false,
      "widget": {
        "name": "number",
        "properties": {
          "before": "",
          "default_value": "",
          "after": "",
          "measure": 1,
          "suffix_user": ""
        }
      },
      "condition": "",
      "conditions": [],
      "size": "",
      "tablename": "main",
      "sqltablename": "sample_theme",
      "sqlname": "id",
      "parentfield": ""
    }, {
      "fieldname": "value",
      "title": "value",
      "description": "value",
      "visible": true,
      "type": "string",
      "required": false,
      "widget": {
        "name": "edit",
        "properties": {
          "tsvector": ""
        }
      },
      "condition": "",
      "conditions": [],
      "size": "",
      "tablename": "main",
      "sqltablename": "sample_theme",
      "sqlname": "value",
      "parentfield": ""
    }, {
      "fieldname": "is_deleted",
      "visible": false,
      "type": "bool",
      "default_value": "false"
    }, {
      "fieldname": "symbol",
      "visible": false,
      "type": "string"
    }, {
      "fieldname": "created_by",
      "visible": false,
      "type": "string"
    }, {
      "fieldname": "edited_by",
      "visible": false,
      "type": "string"
    }, {
      "fieldname": "edited_on",
      "visible": false,
      "type": "date",
      "default_value": "current_timestamp",
      "indexed": "true"
    }, {
      "fieldname": "created_on",
      "visible": false,
      "type": "string"
    }, {
      "fieldname": "published",
      "visible": false,
      "type": "bool",
      "default_value": "false",
      "indexed": "true"
    }],
    "moderated": false,
    "rights": "242"
  }`;
};

const generateTableJSONWithField = (testRunId) => {
  return `{
    "tablename": "public.sample_theme",
    "dataset_id": 20,
    "schema": "public",
    "description": "",
    "title": "sample_theme",
    "userlist": [{
      "userid":"anonym",
      "accesstype":"222"
    }],
    "columns": [{
      "fieldname": "name",
      "title": "name",
      "description": "name",
      "visible": true,
      "type": "string",
      "required": false,
      "widget": {
        "name": "edit",
        "properties": {
          "tsvector": ""
        }
      },
      "condition": "",
      "conditions": [],
      "size": "",
      "tablename": "main",
      "sqltablename": "sample_theme",
      "sqlname": "name",
      "parentfield": ""
    }, {
      "fieldname": "id",
      "title": "key",
      "description": "id",
      "visible": false,
      "type": "serial",
      "required": false,
      "widget": {
        "name": "number",
        "properties": {
          "before": "",
          "default_value": "",
          "after": "",
          "measure": 1,
          "suffix_user": ""
        }
      },
      "condition": "",
      "conditions": [],
      "size": "",
      "tablename": "main",
      "sqltablename": "sample_theme",
      "sqlname": "id",
      "parentfield": ""
    }, {
      "fieldname": "value",
      "title": "value",
      "description": "value",
      "visible": true,
      "type": "string",
      "required": false,
      "widget": {
        "name": "edit",
        "properties": {
          "tsvector": ""
        }
      },
      "condition": "",
      "conditions": [],
      "size": "",
      "tablename": "main",
      "sqltablename": "sample_theme",
      "sqlname": "value",
      "parentfield": ""
    }, {
      "fieldname": "testvalue${testRunId}",
      "title": "testvalue${testRunId}",
      "description": "testvalue${testRunId}",
      "visible": true,
      "type": "string",
      "required": false,
      "widget": {
        "name": "edit",
        "properties": {
          "tsvector": ""
        }
      },
      "condition": "",
      "conditions": [],
      "size": "",
      "tablename": "main",
      "sqltablename": "sample_theme",
      "sqlname": "testvalue${testRunId}",
      "parentfield": ""
    }, {
      "fieldname": "is_deleted",
      "visible": false,
      "type": "bool",
      "default_value": "false"
    }, {
      "fieldname": "symbol",
      "visible": false,
      "type": "string"
    }, {
      "fieldname": "created_by",
      "visible": false,
      "type": "string"
    }, {
      "fieldname": "edited_by",
      "visible": false,
      "type": "string"
    }, {
      "fieldname": "edited_on",
      "visible": false,
      "type": "date",
      "default_value": "current_timestamp",
      "indexed": "true"
    }, {
      "fieldname": "created_on",
      "visible": false,
      "type": "string"
    }, {
      "fieldname": "published",
      "visible": false,
      "type": "bool",
      "default_value": "false",
      "indexed": "true"
    }],
    "moderated": false,
    "rights": "242"
  }`;
};

const generateTableJSONWithOutField = () => {
  return `{
    "tablename": "public.sample_theme",
    "dataset_id": 20,
    "schema": "public",
    "description": "",
    "title": "sample_theme",
    "userlist": [{
      "userid":"anonym",
      "accesstype":"222"
    }],
    "columns": [{
      "fieldname": "name",
      "title": "name",
      "description": "name",
      "visible": true,
      "type": "string",
      "required": false,
      "widget": {
        "name": "edit",
        "properties": {
          "tsvector": ""
        }
      },
      "condition": "",
      "conditions": [],
      "size": "",
      "tablename": "main",
      "sqltablename": "sample_theme",
      "sqlname": "name",
      "parentfield": ""
    }, {
      "fieldname": "value",
      "title": "value",
      "description": "value",
      "visible": true,
      "type": "string",
      "required": false,
      "widget": {
        "name": "edit",
        "properties": {
          "tsvector": ""
        }
      },
      "condition": "",
      "conditions": [],
      "size": "",
      "tablename": "main",
      "sqltablename": "sample_theme",
      "sqlname": "value",
      "parentfield": ""
    }, {
      "fieldname": "id",
      "title": "key",
      "description": "id",
      "visible": false,
      "type": "serial",
      "required": false,
      "widget": {
        "name": "number",
        "properties": {
          "before": "",
          "default_value": "",
          "after": "",
          "measure": 1,
          "suffix_user": ""
        }
      },
      "condition": "",
      "conditions": [],
      "size": "",
      "tablename": "main",
      "sqltablename": "sample_theme",
      "sqlname": "id",
      "parentfield": ""
    }, {
      "fieldname": "is_deleted",
      "visible": false,
      "type": "bool",
      "default_value": "false"
    }, {
      "fieldname": "symbol",
      "visible": false,
      "type": "string"
    }, {
      "fieldname": "created_by",
      "visible": false,
      "type": "string"
    }, {
      "fieldname": "edited_by",
      "visible": false,
      "type": "string"
    }, {
      "fieldname": "edited_on",
      "visible": false,
      "type": "date",
      "default_value": "current_timestamp",
      "indexed": "true"
    }, {
      "fieldname": "created_on",
      "visible": false,
      "type": "string"
    }, {
      "fieldname": "published",
      "visible": false,
      "type": "bool",
      "default_value": "false",
      "indexed": "true"
    }],
    "moderated": false,
    "rights": "242"
  }`;
};

export default {
  generateTableJSONForCreation,
  generateTableJSONOriginal,
  generateTableJSONWithField,
  generateTableJSONWithOutField
}