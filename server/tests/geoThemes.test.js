import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import jwt from 'jsonwebtoken';
import chai, { expect } from 'chai';
import bootstrapApp from '../../index';
import app from './../../config/express';
import config from '../../config/config';
import crypto from 'crypto';

import tableJSON from './data/tablejson';
import tableAccess from './geoThemes.tableAccess';
import tableManagement from './geoThemes.tableManagement';
import tableDataManagement from './geoThemes.tableDataManagement';
import mapFileManagement from './geoThemes.mapFileManagement';
import fileData from './geoThemes.fileData';
import userList from './geoThemes.userList';
import { authenticateTest } from './testTools';

chai.config.includeStack = true;

describe('## GeoThemes', () => {
  let tokens = false;
  let lastInsertedId = false;
  let testRunId = crypto.randomBytes(6).toString('hex');

  before(() => bootstrapApp());

  /**
   * Authentication routine.
   *
   * @param {*} done Fired when the test is over
   */
  const authenticationRoutine = (done) => {
    authenticateTest((resultTokens) => {
      tokens = resultTokens;
      setTimeout(done, 1000);
    });
  };

  describe('# table access', () => {
    it('authentication', authenticationRoutine);
    it('guest should access public table', done => tableAccess.guestShouldAccessPublicTable(app, done, tokens));
    it(`guest should not access system table`, done => tableAccess.guestShouldNotAccessSystemTable(app, done, tokens));
  });

  describe('# table management', () => {
    it('authentication', authenticationRoutine);
    it('should fetch the table description', done => tableManagement.shouldFetchTheTableDescription(app, done, tokens, testRunId));
    it(`should add "testvalue${testRunId}" column and delete it afterwards`, done => tableManagement.shouldAddColumnAndDeleteItAfterwards(app, done, tokens, testRunId));
    it('should create data table from JSON description', done => tableManagement.shouldCreateDataTableFromJSONDescription(app, done, tokens, testRunId));
    it(`should generate meta-description for created table`, done => tableManagement.shouldGenerateMetaDescriptionForCreatedTable(app, done, tokens, testRunId));
    it(`should return list of tables`, done => tableManagement.shouldReturnListOfTables(app, done, tokens, testRunId));
  });

  describe('# [part 1] table data management', () => {
    it('authentication', authenticationRoutine);
    it('reset table to previous column set', done => tableDataManagement.resetTableToPreviousColumnSet(app, done, tokens));
    it('should add data to table', done => tableDataManagement.shouldAddDataToTable(app, done, tokens, (insertedId) => { lastInsertedId = insertedId }));
    it('should add non-string data to table', done => tableDataManagement.shouldAddNonStringDataToTable(app, done, tokens, (insertedId) => { lastInsertedId = insertedId }));
    it('should update data in table', done => tableDataManagement.shouldUpdateDataInTable(app, done, tokens, testRunId, lastInsertedId));
    it('should select data from table', done => tableDataManagement.shouldSelectDataFromTable(app, done, tokens, testRunId, lastInsertedId));
  });

  describe('# file data', () => {
    it('authentication', authenticationRoutine);
    it('should list file data', done => fileData.list(app, done, tokens));
    it('should get meta', done => fileData.meta(app, done, tokens));
    it('should get info', done => fileData.info(app, done, tokens));
    it('should create file for theme', done => fileData.blFile(app, done, tokens));
  });

  describe('# user list', () => {
    it('authentication', authenticationRoutine);
    it('should list users', done => userList.list(app, done, tokens));
    it('should list users with search criteria', done => userList.listWithCriteria(app, done, tokens));
  });

  describe('# MAP file management', () => {
    it('authentication', authenticationRoutine);
    it('should generate MAP file for theme', done => mapFileManagement.shouldGenerateMAPFileForTheme(app, done, tokens));
    it('should generate MAP file for file', done => mapFileManagement.shouldGenerateMAPFileForFile(app, done, tokens));
  });

  describe('# [part 2] table data management', () => {
    it('authentication', authenticationRoutine);
    it('should delete items from table', done => tableDataManagement.shouldDeleteItemsFromTable(app, done, tokens, testRunId, lastInsertedId));
    it('should clear table', done => tableDataManagement.shouldClearTable(app, done, tokens));
    it('should drop table', done => tableDataManagement.shouldDropTable(app, done, tokens));
  });
});
 