import express from 'express';
import Joi from 'joi';
import validate from 'express-validation';
import userCtrl from '../controllers/user';

const router = express.Router();

router.route('/activate').get(validate({
  query: {
    code: Joi.string().guid().required()
  }
}), userCtrl.activate);

router.route('/reset-password').post(validate({
  body: {
    email: Joi.string().email().required()
  }
}), userCtrl.resetPassword);

export default router;
