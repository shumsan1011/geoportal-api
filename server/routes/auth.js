import express from 'express';
import Joi from 'joi';
import validate from 'express-validation';
import authCtrl from '../controllers/auth';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/login').post(validate({
  body: {
    username: Joi.string().regex(/^[a-zA-Z0-9]{4,32}$/).required(),
    password: Joi.string().required()
  }
}), authCtrl.login);

router.route('/random-number').get(authCtrl.getRandomNumber);

export default router;
