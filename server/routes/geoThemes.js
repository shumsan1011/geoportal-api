import express from 'express';
import Joi from 'joi';
import validate from 'express-validation';
import geoThemesController from '../controllers/geoThemes';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/dataset/list').get(validate({}), geoThemesController.bldata_list);

router.route('/dataset/list').post(validate({
  query: {
    f: Joi.number().integer().min(1),
    unique: Joi.string().token()
  }
}), geoThemesController.bldata_list);

router.route('/dataset/add').post(validate({
  query: {
    f: Joi.number().integer().min(1)
  }
}), geoThemesController.bldata_add);

router.route('/dataset/update').post(validate({
  query: {
    f: Joi.number().integer().min(1)
  }
}), geoThemesController.bldata_update);

router.route('/dataset/delete').post(validate({
  query: {
    f: Joi.number().integer().min(1)
  }
}), geoThemesController.bldata_delete);

router.route('/import').get(validate({
  query: {
    f: Joi.number().integer().min(1),
    import_f: Joi.number().integer().min(1)
  }
}), geoThemesController.bldata_import);

router.route('/dataset/clear').post(validate({
  query: {
    f: Joi.number().integer().min(1)
  }
}), geoThemesController.bldata_clearTable);

router.route('/processtheme').post(validate({}), geoThemesController.processTableJson);

router.route('/droptable').get(validate({
  query: {
    dataset_id: Joi.number().integer().min(1)
  }
}), geoThemesController.droptable);

router.route('/genjson').get(validate({
  query: {
    tablename: Joi.string().regex(/^[\w\-\s._]+$/).required()
  }
}), geoThemesController.genJSON);

router.route('/gettables').get(validate({}), geoThemesController.gettablesGeothemes);

router.route('/datafile/list').get(validate({
  query: {
    f: Joi.string().regex(/^[\w\-\s._]+$/).required()
  }
}), geoThemesController.filedata_list);

router.route('/datafile/list').post(validate({
  query: {
    f: Joi.string().regex(/^[\w\-\s._]+$/).required()
  }
}), geoThemesController.filedata_list);

router.route('/datafile/meta').get(validate({
  query: {
    f: Joi.string().regex(/^[\w\-\s._]+$/).required()
  }
}), geoThemesController.filedata_meta);

router.route('/datafile/info').get(validate({
  query: {
    f: Joi.string().regex(/^[\w\-\s._]+$/).required()
  }
}), geoThemesController.filedata_info);

router.route('/user_list/list').get(validate({
  query: {
    f_username: Joi.string().regex(/^[\w\-_]+$/).optional(),
    f_fullname: Joi.string().regex(/^[\w\-_]+$/).optional(),
    f_email: Joi.string().regex(/^[\w\-_.@]+$/).optional(),
    f_id: Joi.number().integer().min(1).optional()
  }
}), geoThemesController.user_list);

router.route('/bl_file').get(validate({
  query: {
    dataset_id: Joi.number().integer().min(0).required(),
    row_id: Joi.number().integer().min(0).required(),
    fieldname: Joi.string().regex(/^[\w\-]+$/).required(),
    filehesh: Joi.string().regex(/^[\w\-]+$/).required()
  }
}), geoThemesController.bl_file);

router.route('/ajaxhub').get(validate({}), geoThemesController.ajaxHubGeothemes);
router.route('/ajaxhub').post(validate({}), geoThemesController.ajaxHubGeothemes);

router.route('/create_map_for_field').post(validate({
  body: {
    metaid: Joi.number().integer().min(0).required(),
  }
}), geoThemesController.createMapForField);

router.route('/createmap').post(validate({
  body: {
    filehesh: Joi.string().regex(/^[\w\-]+$/).required(),
    unique: Joi.string().regex(/^[\w\-]+$/).required()
  }
}), geoThemesController.createMapForFile);

router.route('/layers').get(validate({
  query: {
    id: Joi.number().integer().min(0).required()
  }
}), geoThemesController.layers);

/*
@todo Adapt to JSON-only response if needed
router.route('/managetable').get(validate(), geoThemesController.managetablePage);
router.route('/viewdocument').get(validate(), geoThemesController.viewdocument);
router.route('/managejson').get(validate(), geoThemesController.managejsonPage);
*/

export default router;
