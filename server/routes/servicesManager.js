import express from 'express';
import Joi from 'joi';
import validate from 'express-validation';
import servicesManagerController from '../controllers/servicesManager';

const router = express.Router(); // eslint-disable-line new-cap
router.route('/proxy').get(validate({
  query: {
    url: Joi.string().required()
  }
}), servicesManagerController.proxifier);

router.route('/wpsserver').get(validate({}), servicesManagerController.wpsServerInitiate);
router.route('/create').post(validate({
  body: {
    data: Joi.string().required()
  }
}), servicesManagerController.createWpsManager);

router.route('/execute').get(validate({}), servicesManagerController.execute);

router.route('/wrappers').get(validate({
  query: {
    excludeid: Joi.number().min(0).required()
  }
}), servicesManagerController.wrappers);

router.route('/consoleoutput').get(validate({
  query: {
    id: Joi.number().min(0).required()
  }
}), servicesManagerController.consoleOutput);

router.route('/getinfo').get(validate({
  query: {
    id: Joi.number().min(0).required()
  }
}), servicesManagerController.getinfoWpsManager);

export default router;
