import express from 'express';
import jwt from './../utils/jwt.js';
import userRoutes from './user';
import authRoutes from './auth';
import servicesManagerRoutes from './servicesManager';
import geoThemesRoutes from './geoThemes';

const router = express.Router(); // eslint-disable-line new-cap
router.use(jwt.checkAuthToken);
router.use('/users', userRoutes);
router.use('/auth', authRoutes);
router.use('/servicesManager', servicesManagerRoutes);
router.use('/geoThemes', geoThemesRoutes);
router.use('/health-check', (req, res) => {
  res.send({status: 'success'});
});

export default router;
