import fs from 'fs';
import crypto from 'crypto';
import { service_fields } from './service_fields';
import { translit, log } from '../helpers';
import db from '../db';

const client = db.getClient();

/**
 * Managing database via themes abstraction.
 * 
 * Following tables expect to have fixed identifiers in the geo_dataset:
 * 2 - maps
 * 3 - symbols
 * 4 - roles
 * 5 - users
 * 6 - user activation codes
 * 9 - table access rules
 * 10 - dataset tree
 * 20 - sample theme
 * 100 - geo dataset (keeping the data about all themes)
 * 185 - wps methods
 * 186 - method examples
 * 187 - filelink (resolving URL file links to files in local storage system)
 */

var field_types = {
	integer     : 'integer',
	text        : 'text',
	string      : 'varchar(1024)',
	longstring  : 'varchar(4096)',
	number      : 'double precision',
	date        : 'timestamp',
	serial      : 'serial',
	point       : 'geometry',
	line        : 'geometry',
	polygon     : 'geometry',
	bool        : 'boolean',
	region      : 'integer',
	tableaccess : 'integer',
};

function existsColumn(columns, name) {
	name = name.toLowerCase();
	for (var i = 0; i < columns.length; i++) {
		if (columns[i].fieldname.toLowerCase() == name) {
			return i;	
		}
	}

	return -1;
}


var getcorrectobjname = function (name) {
	name=name.toLowerCase();
	var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz';
	var digits='0123456789';
	var res=''
	name=translit(name);
	for(let i=0; i<name.length; i++){
		if (res.length==0 && digits.indexOf(name.charAt(i))!=-1 ) continue;
		if(chars.indexOf(name.charAt(i))!=-1)
			res=res+name.charAt(i);
		if (res.length>15) break;
	}
	return res;	
}

function AddServiceFields(tableJSON) {
	for (let i = 0; i < service_fields.length; i++) {
		if (existsColumn(tableJSON.columns, service_fields[i].name) ==- 1){
			var field = {
				fieldname : service_fields[i].name,
				visible   : false,
				type      : service_fields[i].type,
				default_value: service_fields[i].default_value,
				indexed: service_fields[i].indexed,
			};

			tableJSON.columns[tableJSON.columns.length]=field;
		} //end if
	} //end for

	return tableJSON;
} //end AddServiceFields()

function RemoveServiceFields(tableJSON) {
	for (let i = 0; i < service_fields.length; i++){
		var fieldnumber = existsColumn(tableJSON.columns, service_fields[i].name);
		if (fieldnumber !== -1) {
			tableJSON.columns.splice(fieldnumber,1);
		} //end if
	} //end for

	return tableJSON;
} //end RemoveServiceFields()



function geo_meta_table(main_dataset_id, parameterShouldBeDeletedLately, pg_client) {	
	this.service_tables={};
	this.client=pg_client;	
	this.curent_user_id='';
	this.database_schema='';
  this.main_dataset_id = main_dataset_id;
  
  const _self = this;

  /**
   * Checks if table exists in the database.
   * 
   * @param {*} schema Schema name
   * @param {*} table  Table name
   * 
   * @return {Promise}
   */
  const tableExists = (schema, table) => {
    if (!schema || schema === '') schema = 'public';

    return new Promise((resolve, reject) => {
      _self.client.query(`SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE
        table_schema = '${schema}' AND table_name = '${schema}')`).then((result) => {
        if (result.rowCount === 1) {
          resolve(true);
        } else {
          resolve(false);
        }
      }).catch(reject);
    });
  }

  /**
   * Checking if all tables that are listed in geo_dataset truly exist in the database
   * 
   * @return {Promise}
   */
  const _validateIntegrity = () => {
    // Throw error or silently delete unexisting tables from geo_dataset
    const THROW_ERROR = 0;
    const SILENTLY_DELETE = 1;
    const conflictPolitic = THROW_ERROR;

    log(`Performing integrity check`);
    const validationResult = new Promise((resolve, reject) => {
      _self.client.query('SELECT id, d_schema, d_table FROM geo_dataset WHERE id <> 100 AND is_deleted = FALSE', (error, result) => {
        if (error !== null) {
          log(error);
          throw 'Error occured while querying the geo_dataset';
        }

        let registeredTables = [];
        result.rows.map(item => {
          if (item.d_schema === null) {
            let parsedTablename = item.d_table.split('.');

            if (parsedTablename.length !== 2) throw 'Unable to parse the table name and extract schema';

            registeredTables.push({
              id: item.id,
              name: parsedTablename[1],
              schema: parsedTablename[0]
            });
          } else {
            registeredTables.push({
              id: item.id,
              name: item.d_table,
              schema: item.d_schema
            });
          }
        });

        _self.client.query(`SELECT * FROM information_schema.tables WHERE
          table_schema <> 'information_schema' AND table_catalog = '${process.env.DB_NAME}'`, (error, tables) => {
          if (error !== null) {
            log(error);
            throw 'Error occured while querying the information_schema';
          }

          let existingTables = [];
          if (tables.rows.length === 0) throw `Number of existing tables can not be 0`;
          tables.rows.map(item => {
            existingTables.push({
              name: item.table_name,
              schema: item.table_schema
            });
          });

          let deleteIds = [];
          registeredTables.map(item => {
            let correspondingExistingTableWasFound = false;
            existingTables.map(existingItem => {
              if (item.name === existingItem.name && item.schema === existingItem.schema) {
                correspondingExistingTableWasFound = true;
                return false;
              }
            });

            if (!correspondingExistingTableWasFound) {
              if (conflictPolitic === THROW_ERROR) {
                throw `The corresponding physical table for ${item.schema}.${item.name} does not exist`;
              } else if (conflictPolitic === SILENTLY_DELETE) {
                deleteIds.push(item.id);
              } else {
                throw 'Invalid politic';
              }
            }
          });

          if (deleteIds.length === 0) {
            resolve();
          } else {
            log(`Deleting stale geo_dataset records and access rights with identifiers: ${deleteIds.join(',')}`);
            _self.client.query(`DELETE FROM geo_dataset WHERE id IN (${deleteIds.join(',')})`, (error) => {
              if (error !== null) {
                log(error);
                throw 'Error occured while deleting records from the geo_dataset';
              }

              _self.client.query(`DELETE FROM table_access_rules WHERE table_id IN (${deleteIds.join(',')})`, (error) => {
                if (error !== null) {
                  log(error);
                  throw 'Error occured while deleting access rules';
                }

                resolve();
              });
            });
          }
        });
      });
		});

		return validationResult;
  };


	const scanAndRetrieveFilesFromFolder = (containingFolder, extension) => {
		let result = [];
		let filesList = fs.readdirSync(containingFolder);
		for (let key in filesList) {
			if (filesList[key].endsWith(extension)) {
				let filename = containingFolder + "/" + filesList[key];
				let content = fs.readFileSync(filename, 'utf8');				
				result.push({ filename, content });
			}
		}

		return result;
	}

	/**
	 * Sequential table structure processing. When all tables are created,
	 * processing SQL for these tables.
	 * 
	 * @return {Promise}
	 */
	const processNextMetaTable = (tableStructures, tableData, onImportFinish) => {
		if (tableStructures.length === 0) {
			return processNextSQL(tableData, onImportFinish);
		} else {				
			var meta = tableStructures.pop();
			log(`Creating table using ${meta.filename}`);
			if (meta !== undefined) {
				var dataset_id = -1;
				if ("dataset_id" in meta.content === true && meta.content.dataset_id != undefined) {
					dataset_id = meta.content.dataset_id;
				}

				_self.processTableJson(meta.content, dataset_id, (tableId) => {
					return processNextMetaTable(tableStructures, tableData, onImportFinish);
				});
			} else {
				return processNextMetaTable(tableStructures, tableData, onImportFinish);
			}
		}
	} //end processNextMetaTable();


	/**
	 * Processing SQL for already existing tables.
	 * 
	 * @return {Promise}
	 */
	const processNextSQL = (tableData, onImportFinish) => {
		if (tableData.length === 0) {
			_processDatabaseFunctions().then(() => {
				_validateIntegrity().then(() => {
					onImportFinish();
				}).catch(() => { throw 'Unable to validate integrity'; });
			}).catch(() => { throw 'Unable to process database functions'; })
		} else {
			var item = tableData.pop();
			log(`Creating data using ${item.filename}`);
			var query = _self.client.query(item.content, function(error, sql_res, unofields) {
				if (error === null) {
					processNextSQL(tableData, onImportFinish);
				} else {
					log(error);
					log("Some error may be not wrong");
				}
			});
		}
	} //end processNextSQL()


	/**
	 * Creates required database functions.
	 * 
	 * @return {Promise}
	 */
	const _processDatabaseFunctions = () => {
		const result = new Promise((resolve, reject) => {
			var content = fs.readFileSync(__dirname + '/database/role.sql', 'utf8');
			_self.client.query(content, (error, sql_res, unofields) => {
				if (error != null){
					if (error.code !== "42710") {
						log(error);
						throw 'Unable to process the database role script';
					}
				}
	
				content = fs.readFileSync(__dirname + '/database/dataset_access.sql', 'utf8');
				_self.client.query(content, (error, sql_res, unofields) => {
					if (error != null){
						if (error.code !== "42710") {
							log(error);
							throw 'Unable to process the dataset_access script';
						}
					}

					resolve();
				});
			});
		});

		return result;
	};


	/**
	 * Initializes the database  fro the GeoThemes module to work. It recreates the required for
	 * work tables and checks the integrity of the existing data.
	 * 
	 * @param {Boolean} dropExistingTables Specifies ig exising tables should be descroyed beforehand
	 */
	this.init = (dropExistingTables = false) => {
		const initializationResult = new Promise((resolve, reject) => {
			if (dropExistingTables) {
				let serviceTableNames = [];
				let mainDataSetIndex = -1;
				let tableStructures = scanAndRetrieveFilesFromFolder(__dirname + '/database/tablestructures', 'meta').map((item, index) => {
					item.content = JSON.parse(item.content);
					serviceTableNames.push(item.content.tablename);
					if (parseInt(item.content.dataset_id) === this.main_dataset_id) mainDataSetIndex = index;
	
					return item;
				});
	
				let tableData = scanAndRetrieveFilesFromFolder(__dirname + '/database/tabledata', 'sql');
	
				const proceedWithTablesProcessing = () => {
					var meta = tableStructures[mainDataSetIndex];
					tableStructures.splice(mainDataSetIndex, 1);
					_self.processTableJson(meta.content, meta.dataset_id, (create) => {
						processNextMetaTable(tableStructures, tableData, resolve);
					});
				};

				const defaultScheme = 'public';
				serviceTableNames.filter((value, index, self) => {
					return self.indexOf(value) === index;
			 }).map(item => {
					return ((item.indexOf('.') === -1) ? defaultScheme + '.' + item : item);
				});

				_self.client.query(`DROP TABLE IF EXISTS ${ serviceTableNames.join(', ') }`)
				.then(() => proceedWithTablesProcessing())
				.catch((error) => { throw 'Unable to drop tables'; });
			} else {
				resolve();
			}
		});

		return initializationResult;
	};
	
	/**
	* Removing table
	*/
	this.dropTable=function (dataset_id, callback) {
		var geo_table = this;
		if(!isNaN(parseFloat(dataset_id)) && isFinite(dataset_id)){
			geo_table.is_author(dataset_id, geo_table.curent_user_id, function(error, author){
				if (error === null){
					if (author) {
						geo_table.get_table_json({}, dataset_id, function (meta){
							let tablename = meta.tablename;							
							var sub_sql = 'DROP TABLE '+tablename+'; ';						
							sub_sql += "DELETE FROM public.geo_dataset WHERE id="+dataset_id+";";
							var query = geo_table.client.query(sub_sql, function(error, sql_res2, unofields) {
								callback(error);								
							});
						});
					} else {
            callback('You should be the owner.');
          }
				} else {
					log(error);
					callback(error);
				}
			});
		} else {
			callback('dataset_id is not number.');
		}
	};

	
	/**
	* Clearing table data
	*/
	this.clearTable=function (dataset_id, callback) {
		var geo_table=this;
		if(!isNaN(parseFloat(dataset_id)) && isFinite(dataset_id)){
			geo_table.is_author(dataset_id, geo_table.curent_user_id, function(error, author){
				if (error === null){
					if(author){
						geo_table.get_table_json({}, dataset_id, function (meta){
							let tablename = meta.tablename;
							var sub_sql='DELETE FROM '+tablename+'; ';							
							var query = geo_table.client.query(sub_sql, function(error, sql_res2, unofields) {
								callback(error);								
							});
						});
					}
					else
						callback('You should be the owner.');
				}
				else{
					log(error);
					callback(error);
				}
			} );
		}
		else{
			callback('dataset_id is not number.');
		}
	};	
	
	
	/**
	* AJAX theme update
	*/
	this.processTableJson = (tablejson, dataset_id, callback) => {
		if(tablejson.tablename.indexOf('.')==-1){
			tablejson.tablename = this.database_schema+'.'+tablejson.tablename;
			log(tablejson.tablename);
    }
    
		for (var i = 0; i < tablejson.columns.length; i++) {
			if(tablejson.columns[i].tablename===undefined || tablejson.columns[i].tablename=='')
				tablejson.columns[i].tablename='main';
			if(tablejson.columns[i].sqltablename===undefined || tablejson.columns[i].sqltablename=='')
				tablejson.columns[i].sqltablename=tablejson.tablename;
    }

		if (isNaN(dataset_id))
			dataset_id=-1;		

		var geo_table = this;
		tablejson = RemoveServiceFields(tablejson);

		function processUserList(tableid) {
			if (tableid === undefined || tableid === '' || tableid === null || isNaN(tableid)) {
				callback(tableid);
			} else {
        var cleanupquery = 'DELETE FROM public.table_access_rules WHERE table_id = ' + tableid + ' AND waiting_for_approval = FALSE;';
        var insertrulesquery = '';
        if (tablejson.userlist){
          let userlist = tablejson.userlist;
          for (var i = 0; i < userlist.length; i++){
            insertrulesquery += "INSERT INTO public.table_access_rules (table_id, user_id, access_mode, is_deleted, waiting_for_approval) VALUES ('" + tableid + "', '" + userlist[i].userid + "', '"+userlist[i].accesstype+"', FALSE, FALSE);";
          }
        }

        var overallquery = cleanupquery + insertrulesquery;	
        var query = geo_table.client.query(overallquery, (error, rows, fields) => {
          if (error !== null && error.code !== "42P01") {
            log(error);
            log(overallquery);
            log('deleting table_access_rules');
          }

          callback(tableid);
        });
      }
    }
    
		function register(exist) {
			if (dataset_id == -1) {
				geo_table.register_dataset(tablejson, dataset_id, function(new_dataset_id){
					processUserList(new_dataset_id);
				});
			} else {
				var sub_sql = "SELECT id FROM public.geo_dataset WHERE id=" + dataset_id;
				var query = geo_table.client.query(sub_sql, (error, sql_res, unofields) => {
          if (error === null){
						if (sql_res.rows.length === 0)	{
							geo_table.register_dataset(tablejson, dataset_id, (new_dataset_id) => {
								processUserList(new_dataset_id);
							});
						} else {
							geo_table.update_register_dataset(tablejson, dataset_id, (new_dataset_id) => {
								processUserList(dataset_id);
							});
						}
					} else {
						log(error);
						log(sub_sql);
						callback(-1);
					}
				});
			}
    };
    
		let strtablejson = JSON.stringify(tablejson);
		let tablename    = tablejson.tablename;
		let schema       = tablejson.schema;
		if (tablejson.schema == undefined || tablejson.schema == ''){
			tablejson.schema = this.database_schema;
    }
    
		if (tablename.indexOf('.')!=-1){
			let tparts=tablename.split('.');
			schema=tparts[0];
			tablename=tparts[1];
		}

		var sql="SELECT table_name FROM information_schema.tables WHERE table_schema='"+schema+"' and table_name='"+tablename+"'";		
		var query = this.client.query(sql, (error, sql_res, unofields) => {
			if (error === null){
				if (sql_res.rows.length == 0)	{
					//log("createTable 1 "+sql);
					geo_table.createTable(tablejson, register);
					return;
				} else {
					//log("updateTable 2 "+sql +sql_res.rows.length);
					console.log('author', geo_table.curent_user_id);
					geo_table.is_author(dataset_id, geo_table.curent_user_id, (error, author) => {
						if (error === null){
							if(author){
								geo_table.updateTable(tablejson, register);
							} else {
                callback(-1);
              }
						} else {
							log(error);
							callback(-1);
						}
					});					
				}
			}	else {
				log(error);
				//callback()
			}
		});
	}
	
	this.is_author=function(dataset_id, user_id, callback){
		var sql2="SELECT created_by FROM public.geo_dataset WHERE not is_deleted and id=" + dataset_id;		
		var query = this.client.query(sql2, function(error, sql_res2, unofields) {
			if (error === null && sql_res2.rows.length == 1 && sql_res2.rows[0].created_by==user_id)							
				callback(error, true);
      else if(error === null && sql_res2.rows.length == 0) {
				callback('The table ' + dataset_id + ' is not registered in the geo_dataset.', false);
			}else{
				callback(error, false);
			}
		});
  }
  
	/**
	* Creating table
	*/
	this.createTable = function(json_desc_obj, callback) {
		var geo_table=this;
		let outputobject = AddServiceFields(json_desc_obj);
		var reqobj = new Object();			
		var accessmodevalue;
		if(outputobject.accessmode!==undefined)
			accessmodevalue = outputobject.accessmode.access_mode;

		let strtablejson	 = JSON.stringify(outputobject);		
		var tparts=outputobject.tablename.split('.');
		var scheme=tparts[0];
		var tablename=tparts[1];
		var sqlstr    = 'CREATE TABLE ' + outputobject.tablename + '( ';
		var strfields ='';
		var field_indexes='';
		for (var i = 0; i < outputobject.columns.length; i++){
			if(outputobject.columns[i].tablename!='main' && outputobject.columns[i].tablename!==undefined)
				continue;
			if ((outputobject.columns[i].type !== 'line') && (outputobject.columns[i].type !== 'point') && (outputobject.columns[i].type !== 'polygon')){
				if(strfields != '')	{
					strfields = strfields + ', ';
        }
        
				if(outputobject.columns[i].default_value===undefined || outputobject.columns[i].default_value=='')
					strfields = strfields + '"' + outputobject.columns[i].fieldname + '" ' + field_types[outputobject.columns[i].type];				
				else
          strfields = strfields + '"' + outputobject.columns[i].fieldname + '" ' + field_types[outputobject.columns[i].type] + ' DEFAULT '+ outputobject.columns[i].default_value;
        
				if(outputobject.columns[i].indexed=='true'){
					let index_name='index_'+tablename+'_'+outputobject.columns[i].fieldname;
					if(index_name.length>30)
            index_name=index_name.substring(0,29);
          
					field_indexes+=' CREATE INDEX '+index_name+'  ON ' + outputobject.tablename + '  ("'+outputobject.columns[i].fieldname+'");';
				}
				
				if (outputobject.columns[i].unique && outputobject.columns[i].unique === true) {
					let index_name = `index_unique_${tablename}_${outputobject.columns[i].fieldname}`;
					if (index_name.length > 30) index_name = index_name.substring(0, 29);
					field_indexes += ` CREATE UNIQUE INDEX ${index_name} ON ${outputobject.tablename} ("${outputobject.columns[i].fieldname}");`;
				}

				if (outputobject.columns[i].pk!=undefined && outputobject.columns[i].pk) {
					strfields=strfields + ' CONSTRAINT ' + getcorrectobjname(crypto.randomBytes(10).toString('hex')) + '_constr PRIMARY KEY ';
				}
			}
		}

		sqlstr = sqlstr + strfields + ');';

		// <AddGeometryColumn> query
		var geomstr = '';
		tparts=outputobject.tablename.split('.');
		let schema=tparts[0];
		tablename=tparts[1];
		for (var i = 0; i < outputobject.columns.length; i++){
			if(outputobject.columns[i].tablename!='main')
				continue;			
			switch(outputobject.columns[i].type) {
				case 'point':
				  geomstr = geomstr + 'SELECT AddGeometryColumn(\'' + schema + '\',\'' + tablename + '\',\'' + outputobject.columns[i].fieldname + '\', 4326,\'MULTIPOINT\',2);';
				  break;
				case 'line':
				  geomstr = geomstr + 'SELECT AddGeometryColumn(\'' + schema + '\',\'' + tablename + '\',\'' + outputobject.columns[i].fieldname + '\', 4326,\'MULTILINESTRING\',2);';
				  break;
				case 'polygon':
				  geomstr = geomstr + 'SELECT AddGeometryColumn(\'' + schema + '\',\'' + tablename + '\',\'' + outputobject.columns[i].fieldname + '\', 4326,\'MULTIPOLYGON\',2);';
				  break;
			}
		}

		var tableownerquery = "ALTER TABLE " + schema + "." + tablename + " OWNER TO storage_users;"
		
		// Executing all queries at once
		var createtablequery = sqlstr + geomstr + field_indexes + tableownerquery;// + populateservicefields;
		var cquery = geo_table.client.query(createtablequery, function(error, rows, fields) {	
			if (error) {
				log(error);
				log(createtablequery);
        callback(false);
        throw `Error occured while creating the table ${schema}.${tablename}`;
			}	else{
				callback(true);				
			}
		});		
	}	
	
	this.register_dataset = function(json_desc_obj, dataset_id, callback){
		var strtablejson = JSON.stringify(json_desc_obj);
    var accessmodevalue='';
    let sql = '';
		if(json_desc_obj.accessmode!==undefined)
			accessmodevalue = json_desc_obj.accessmode.access_mode;
		var is_moderated    = json_desc_obj.moderated;
		if(is_moderated===undefined)
			is_moderated='false';
		if(dataset_id!=-1)
			sql = "INSERT INTO geo_dataset(id, name, created_by, \"JSON\", status, d_schema, d_table, access_mode, description) VALUES ("+dataset_id+",'"+json_desc_obj.title+"', '"+this.curent_user_id+"','"+strtablejson+"', 1, NULL,'"+json_desc_obj.tablename+"', 0, '"+json_desc_obj.description+"')";
		else
			sql = "INSERT INTO geo_dataset(name, created_by, \"JSON\", status, d_schema, d_table, access_mode, description) VALUES ('"+json_desc_obj.title+"', '"+this.curent_user_id+"','"+strtablejson+"', 1, NULL,'"+json_desc_obj.tablename+"', 0, '"+json_desc_obj.description+"') RETURNING id;";

		var fquery = this.client.query(sql, function(error, sql_res, fields) {
			if (error === null)	{					
				if(dataset_id==-1)
					callback(sql_res.rows[0].id);
				else
					callback(dataset_id);
				return;
			} else {
				log(error);
				callback(-1);
				return;
			}						
		});	
	};
	
	this.update_register_dataset=function(json_desc_obj, dataset_id, callback){
		//log("this.update_register_dataset");
		var strtablejson = JSON.stringify(json_desc_obj);
		var accessmodevalue='';
		if(json_desc_obj.accessmode!==undefined)
			accessmodevalue = json_desc_obj.accessmode.access_mode;
		var is_moderated    = json_desc_obj.moderated;
		if(is_moderated===undefined)
			is_moderated='false';
		let sql = "UPDATE geo_dataset SET is_deleted=false, name='" + json_desc_obj.title + "', \"JSON\"='" + strtablejson + "', description='"+json_desc_obj.description+"'";
		if(is_moderated!=undefined)
			sql+=", is_moderated = " + is_moderated;
		sql+=" WHERE id=" + dataset_id;		
		//log(sql);
		var fquery = this.client.query(sql, function(error, sql_res, fields) {
			if (error === null)	{					
				callback(true);
				return;
			}
			else{
				log(error);
				callback(false);
				return;
			}						
		});	
	}
	
	/**
	*	Table update
	*/
	this.updateTable=function(json_desc_obj, callback) {
		var geo_table=this;
		
		var newobj = AddServiceFields(json_desc_obj);
		//json_desc_obj=RemoveServiceFields(json_desc_obj);
		var strtablejson = JSON.stringify(json_desc_obj);
		var accessmodevalue;
		if(newobj.accessmode!==undefined)
			accessmodevalue = newobj.accessmode.access_mode;
		var is_moderated    = newobj.moderated;
		if(is_moderated===undefined)
			is_moderated='false';


		// Original version
		this.getTableJSON(newobj.tablename, function (oldobj){			
			// Accumulating queries
			var queryCollector = '';
			let tparts = newobj.tablename.split('.');
			var schema = tparts[0];
			var tablename = tparts[1];
			// Checking every column
			for (var i = 0; i < newobj.columns.length; i++) {
				if(newobj.columns[i].tablename!='main' && newobj.columns[i].tablename!==undefined)
          continue;

				var new_element = true;
				for (var j = 0; j < oldobj.columns.length; j++) {					
					if (newobj.columns[i].fieldname.toLowerCase() == oldobj.columns[j].fieldname.toLowerCase()) {
						// Exact same columns - it is not required to change
						new_element = false;
						break;
					}
        }
        
				if ((new_element) && (newobj.columns[i].fieldname !== 'id')) {					
					//log('field does not exist '+newobj.columns[i].fieldname);
					var geomstr = 'SELECT AddGeometryColumn(\'' + schema + '\', \'' + tablename + '\',\'' + newobj.columns[i].fieldname + '\', 4326,\'HEREGOESEPSG\',2);';
					switch(newobj.columns[i].type) {
						case 'point':
							var geomstr = geomstr.replace(/HEREGOESEPSG/g, 'MULTIPOINT');
							queryCollector = queryCollector + geomstr;
							break;
						case 'line':
							var geomstr = geomstr.replace(/HEREGOESEPSG/g, 'MULTILINESTRING');
							queryCollector = queryCollector + geomstr;
							break;
						case 'polygon':
							var geomstr = geomstr.replace(/HEREGOESEPSG/g, 'MULTIPOLYGON');
							queryCollector = queryCollector + geomstr;
							break;
						case 'calculated':
							break;		
						case 'tableaccess':
							break;		
							
						default:
							if(newobj.columns[i].default_value===undefined || newobj.columns[i].default_value=='')
								queryCollector = queryCollector + 'ALTER TABLE ' + schema + '."' + tablename + '" ADD COLUMN ' + newobj.columns[i].fieldname + ' ' + field_types[newobj.columns[i].type] + ';';						
							else
								queryCollector = queryCollector + 'ALTER TABLE ' + schema + '."' + tablename + '" ADD COLUMN ' + newobj.columns[i].fieldname + ' ' + field_types[newobj.columns[i].type] + ' DEFAULT '+newobj.columns[i].default_value+';';
							break;
					}
				}				
			}			
			//log('queryCollector='+queryCollector);
			if(queryCollector!=''){								
				var cquery = geo_table.client.query(queryCollector, function(error, sql_res, fields) {					
					if (error == null) {									
						callback(true);
					}
					else{
						log('updating - error');
						log(error);
						callback(false);
					}
				});
				
			}
			else
				callback(true);
		});	
	};
	
	this.getTableJSON=function (table, callback) {
		var geo_table=this;
		let tparts=table.split('.');
		var schema=tparts[0];
    var table=tparts[1];
    if (!schema || schema === '' || !table || table === '') {
      throw `Invalid table name was provided: ${table}`;
    } else {
      //log('getTableJSON ' + schema + ' '+ table);
      var tableobject = new Object();
      var params      = new Array();
      tableExists(schema, table).then(exists => {
        if (exists) {
          var query = `SELECT i.data_type, i.column_name, g.type FROM information_schema.columns i left outer join public.geometry_columns g on i.column_name = g.f_geometry_column
            and i.table_schema = g.f_table_schema and i.table_name = g.f_table_name WHERE i.table_name='${table}' and i.table_schema = '${schema}'`;
          var cquery = geo_table.client.query(query, function(error, sql_res, fields) {
            if (error === null) {
              //log('sql_res.rows.length=' + sql_res.rows.length);
              for (var i = 0; i < sql_res.rows.length; i++) {
                var curcol = sql_res.rows[i];
                var tempo = {
                  fieldname: curcol.column_name,
                  title: curcol.column_name
                };
                
                if ((curcol.column_name=='fid' || curcol.column_name=='id' || curcol.column_name=='gid') && (curcol.data_type.match(/integer/i)
                  || curcol.data_type.match(/serial/i))) {
                  tempo = Object.assign(tempo, {
                    type: 'integer',
                    visible: 'false',								
                    pk: 'true',
                    widget: {
                      name: 'hidden'
                    }
                  });

                  params.push(tempo);
                } else if (curcol.data_type.match(/integer/i) || curcol.data_type.match(/serial/i) || curcol.data_type.match(/double/i) ) {
                  tempo = Object.assign(tempo, {
                    type: 'number',	
                    widget: {
                      name: 'number'
                    }
                  });

                  params.push(tempo);
                } else if (curcol.data_type.match(/timestamp/i) || curcol.data_type.match(/date/i)) {
                  tempo = Object.assign(tempo, {
                    type: 'date',
                    widget: {
                      name: 'date'
                    }
                  });

                  params.push(tempo);
                } else if ((curcol.data_type.match(/character/i)) || (curcol.data_type !== 'USER-DEFINED')) {
                  tempo = Object.assign(tempo, {
                    type: 'string',
                    widget: {
                      name: 'edit',
                      properties: {
                        size: 16,
                      }
                    }
                  });
                  
                  params.push(tempo);
                } else if (curcol.data_type == 'USER-DEFINED' && curcol.type!=''){
                  var cursign = '';
                  switch (curcol.type) {
                    case 'POINT':
                      cursign = 'point'; 
                      break;						
                    case 'MULTIPOINT':
                      cursign = 'point'; 
                      break;
                    case 'LINESTRING':
                      cursign = 'line';
                      break;
                    case 'MULTILINESTRING':
                      cursign = 'line';
                      break;
                    case 'POLYGON':
                      cursign = 'polygon';
                      break;
                    case 'MULTIPOLYGON':
                      cursign = 'polygon';
                      break;
                    default:
                      cursign = '';
                      break;
                  } //end switch
                  
                  if (cursign!='') {							
                    var tempo = {
                      fieldname: curcol.column_name,
                      title: curcol.f_geometry_column,
                      type: 'geometry',
                      visible: 'true',							
                      widget: {
                        name: cursign
                      }
                    }

                    params.push(tempo);
                  }
                }
              }
    
              tableobject = {
                tablename: schema+'.'+table,					
                title: schema+'.'+table,
                input: '',
                print: '',
                moderated: false,
                accessmode: {access_mode: 4},
                columns: params				
              }

              callback(tableobject);
              return;
            }	else {
              log('error in getTableJSON');
              log(error);
              log(query);
              callback(null);				
              return;
            }
          });
        } else {
          throw `Requested table does not exist: ${table}`;
        }
      }).catch((error) => {
        log(error);
        throw 'Error occured';
      });
    }
	}
	
	this.get_table_json = (usingmeta, dataset_id, callback) => {
		var geo_table=this;
    var meta;
		var n = parseInt(dataset_id);
		if (isNaN(n)) {
			callback(null);
			return;		
    }

		if (usingmeta[dataset_id] !== undefined) {
			callback(null);
			return;
		}

		function req_load_meta(column_id){
			if (column_id == meta.columns.length || meta.columns[column_id].widget===undefined){
				meta.usingmeta=usingmeta;
				callback(meta);
				return;
			}
      
      log(meta.columns[column_id].widget.name);
			log('-ok');
			if((meta.columns[column_id].widget.name=='classify' || meta.columns[column_id].widget.name=='details') && (meta.columns[column_id].widget.properties.dataset_id % 1 === 0) && usingmeta[meta.columns[column_id].widget.properties.dataset_id]===undefined){
				geo_table.get_table_json(usingmeta,meta.columns[column_id].widget.properties.dataset_id, function (mdobj){
					if(mdobj!=null)
						usingmeta[meta.columns[column_id].widget.properties.dataset_id]=mdobj;
					//meta.columns[column_id].widget.properties.ref_meta=mdobj;
					req_load_meta(column_id+1);
				});
			}
			else
				req_load_meta(column_id+1);
		}
		
		var client = this.client;
		var sql='';
		if (this.curent_user_id !== '') {
			sql = 'SELECT "JSON", wms_link, wms_layer_name, opacity, is_moderated, wms_zoom_max, wms_zoom_min, public.dataset_access('+dataset_id+', \''+this.curent_user_id+'\') as g_access  FROM public.geo_dataset WHERE id = ' + dataset_id + ';';
		} else {
			sql = 'SELECT "JSON", wms_link, wms_layer_name, opacity, is_moderated, wms_zoom_max, wms_zoom_min, public.dataset_access('+dataset_id+', \'\') as g_access  FROM public.geo_dataset WHERE id = ' + dataset_id + ';';		
    }
    
		var query = client.query(sql, function(error, unorows, unofields) {
			if (error === null) {
  			if (unorows.rows.length === 1) {
					meta = JSON.parse(unorows.rows[0].JSON);
					meta.dataset_id=dataset_id;
					if(meta.tablename.indexOf('.')==-1 && meta.schema!=''){
						meta.tablename=meta.schema + '.' + meta.tablename;
					}

					if (meta === null) {
						callback(null);
						return;					
          }

					meta.rights=unorows.rows[0].g_access;
					meta.wms_link=unorows.rows[0].wms_link;
					meta.wms_layer_name=unorows.rows[0].wms_layer_name;
					meta.dataset_id=dataset_id;
					meta.opacity=unorows.rows[0].opacity;
					if (unorows.rows[0].wms_zoom_max==null || unorows.rows[0].wms_zoom_max==undefined)
						meta.maxZoom=18;
					else
            meta.maxZoom=unorows.rows[0].wms_zoom_max;
          
					if (unorows.rows[0].wms_zoom_max==null || unorows.rows[0].wms_zoom_max==undefined)
						meta.minZoom=0;
					else
            meta.minZoom=unorows.rows[0].wms_zoom_min; 

					usingmeta[dataset_id]=meta;
					req_load_meta(0);
				} else {
					log(unorows);
					callback(null);
				} //end if
			} else {
				log(error);
				callback(null);
			}
		});
	}
	
	return this;
}

let instantiatedMetaTable = false;

let exportedObject = {};
Object.defineProperty(exportedObject, 'meta_manager', {
	get: function() {
		if (instantiatedMetaTable === false) {
			console.log('Instantiating the meta manager');
			instantiatedMetaTable = new geo_meta_table(100, {}, client);
		} else {
			console.log('Returning already instantiated singleton of meta manager');
		}

		return instantiatedMetaTable;
	}
});

export default exportedObject;
