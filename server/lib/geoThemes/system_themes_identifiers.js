/**
 * Fixed identifiers of system themes.
 */

const identifiers = {
	MAPS: 2,
	SYMBOLS: 3,
	ROLES: 4,
	USERS: 5,
	USER_ACTIVATION_CODES: 6,
	TABLE_ACCESS_RULES: 9,
	DATASET_TREE: 10,
	SAMPLE_THEME: 20,
	GEO_DATASET: 100,
	WPS_METHODS: 185,
	METHOD_EXAMPLES: 186,
	FILELINK: 187
};

const identifiersWithTableNames = [
	{ id: identifiers.MAPS, name: 'public.maps' },
	{ id: identifiers.SYMBOLS, name: 'public.symbols' },
	{ id: identifiers.ROLES, name: 'public.roles' },
	{ id: identifiers.USERS, name: 'public.users' },
	{ id: identifiers.USER_ACTIVATION_CODES, name: 'public.user_activation_codes' },
	{ id: identifiers.TABLE_ACCESS_RULES, name: 'public.table_access_rules' },
	{ id: identifiers.DATASET_TREE, name: 'public.dataset_tree' },
	{ id: identifiers.SAMPLE_THEME, name: 'public.sample_theme' },
	{ id: identifiers.GEO_DATASET, name: 'public.geo_dataset' },
	{ id: identifiers.WPS_METHODS, name: 'public.wps_methods' },
	{ id: identifiers.METHOD_EXAMPLES, name: 'public.method_examples' },
	{ id: identifiers.FILELINK, name: 'public.filelink' }
];

export default { identifiersWithTableNames, identifiers };
