-- Function: gis_admin.dataset_access(integer, character varying)

-- DROP FUNCTION gis_admin.dataset_access(integer, character varying);

CREATE OR REPLACE FUNCTION public.dataset_access(dataset_id integer, _user_id character varying)
  RETURNS character varying AS
$BODY$
DECLARE
	access_list character varying;
	dataset public.geo_dataset%ROWTYPE;
	dataset_rule public.table_access_rules%ROWTYPE;
BEGIN

	/*	Basic permission constants
	№ character - permission
	0 - view
	1 - edit
	2 - moderation
	3 - publication
	values
	0 - disable
	1 - allowed own rows
	2 - allowed all rows

	Also tables has the access_mode parameter, its values:
	0 - process further
	1 - can be read by everybody
	
	var ableToAddEditViewOwn = 0;
	var ableToViewOthers     = 1;
	var ableToEditOthers     = 2;
	var isOwner              = 3;

	For example:
	1000 - can only view own records
	2100 - can only view and write own record and only view others records

	*/

	SELECT * INTO dataset FROM public.geo_dataset  WHERE id = dataset_id;
	IF NOT FOUND THEN
	    RETURN '000';
	END IF;

	IF _user_id <> '' AND dataset.created_by = _user_id THEN
	    RETURN '242';
	END IF;

	IF _user_id <> '' THEN
		/*	Checking if current user has specific rights for this table	*/
		SELECT * INTO dataset_rule FROM public.table_access_rules WHERE table_id = dataset_id AND user_id = _user_id AND NOT waiting_for_approval;
		IF dataset_rule.access_mode IS NULL THEN
			SELECT * INTO dataset_rule FROM public.table_access_rules WHERE table_id = dataset_id AND user_id = 'registered' AND NOT waiting_for_approval;
			IF dataset_rule.access_mode IS NULL THEN			
				SELECT * INTO dataset_rule FROM public.table_access_rules WHERE table_id = dataset_id AND user_id = 'anonym' AND NOT waiting_for_approval;
				IF dataset_rule.access_mode IS NULL THEN		
					RETURN '000';
				ELSE
					RETURN dataset_rule.access_mode;
				END IF;
			ELSE
				RETURN dataset_rule.access_mode;
			END IF;		
		ELSE
			RETURN dataset_rule.access_mode;
		END IF;
	ELSE
		SELECT * INTO dataset_rule FROM public.table_access_rules WHERE table_id = dataset_id AND user_id = 'anonym' AND NOT waiting_for_approval;
		IF dataset_rule.access_mode IS NULL THEN
			RETURN '000';
		ELSE
			RETURN dataset_rule.access_mode;
		END IF;
	END IF;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.dataset_access(integer, character varying)
  OWNER TO postgres;
