do 
$body$
declare 
  num_roles integer;
begin
   SELECT count(*) into num_roles FROM pg_roles WHERE rolname = 'storage_users';

   IF num_roles = 0 THEN
      CREATE ROLE storage_users NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
      GRANT SELECT ON public.geo_dataset TO storage_users;
   END IF;
end
$body$
;