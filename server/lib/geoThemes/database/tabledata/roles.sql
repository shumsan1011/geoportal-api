DELETE FROM roles WHERE id = 1 OR id = 2 OR id = 3;
INSERT INTO roles (id, name, description, is_default, is_admin) VALUES (1, 'Guest', 'Guest account', true, false);
INSERT INTO roles (id, name, description, is_default, is_admin) VALUES (2, 'Contributor', 'Able to create and manage own content items linked to their own user profile area', false, false);
INSERT INTO roles (id, name, description, is_default, is_admin) VALUES (3, 'Administrator', 'Able to manage the entire site', false, true);
SELECT pg_catalog.setval(pg_get_serial_sequence('public.roles', 'id'), MAX(id)) FROM public.roles;