DELETE FROM mapsymbols WHERE 1 = 1;

INSERT INTO mapsymbols (name, symbol, keywords, id, is_deleted, created_by, edited_by, edited_on, created_on, published) VALUES ('star', '0 .375
.35 .375
.5 0
.65 .375
1 .375
.75 .625
.875 1
.5 .75
.125 1
.25 .625', 'point, polygon', 12, false, '50f7a1d80d58140037000006', '50f7a1d80d58140037000006', '2016-11-07 14:07:16.303026', '2016-11-07 14:06:12.574037+09', false);
INSERT INTO mapsymbols (name, symbol, keywords, id, is_deleted, created_by, edited_by, edited_on, created_on, published) VALUES ('x', '0 0
10 10
-5 -5
0 10
10 0', 'polygon', 11, false, '50f7a1d80d58140037000006', '50f7a1d80d58140037000006', '2016-11-07 14:07:29.712381', '2016-11-07 13:10:52.493198+09', false);
INSERT INTO mapsymbols (name, symbol, keywords, id, is_deleted, created_by, edited_by, edited_on, created_on, published) VALUES ('horline', '0 5
10 5', 'polygon', 10, false, '50f7a1d80d58140037000006', '50f7a1d80d58140037000006', '2016-11-07 14:07:36.143107', '2016-11-07 13:09:33.674099+09', false);
INSERT INTO mapsymbols (name, symbol, keywords, id, is_deleted, created_by, edited_by, edited_on, created_on, published) VALUES ('vertline', '5 0
5 10', 'polygon', 9, false, '50f7a1d80d58140037000006', '50f7a1d80d58140037000006', '2016-11-07 14:07:44.798166', '2016-11-07 13:08:40.858743+09', false);
INSERT INTO mapsymbols (name, symbol, keywords, id, is_deleted, created_by, edited_by, edited_on, created_on, published) VALUES ('slash', '0 10
10 0', 'polygon', 7, false, '50f7a1d80d58140037000006', '50f7a1d80d58140037000006', '2016-11-07 14:07:51.71951', '2016-11-07 13:07:09.533021+09', false);
INSERT INTO mapsymbols (name, symbol, keywords, id, is_deleted, created_by, edited_by, edited_on, created_on, published) VALUES ('backslash', '0 0
10 10', 'polygon', 8, false, '50f7a1d80d58140037000006', '50f7a1d80d58140037000006', '2016-11-07 14:07:58.414023', '2016-11-07 13:07:56.214057+09', false);
INSERT INTO mapsymbols (name, symbol, keywords, id, is_deleted, created_by, edited_by, edited_on, created_on, published) VALUES ('triangle', '0 4
2 0
4 4
0 4', 'point, polygon', 6, false, '50f7a1d80d58140037000006', '50f7a1d80d58140037000006', '2016-11-07 14:08:03.126216', '2016-11-07 13:05:32.174008+09', false);
INSERT INTO mapsymbols (name, symbol, keywords, id, is_deleted, created_by, edited_by, edited_on, created_on, published) VALUES ('cross', '0 0
1 1
-99 -99
0 1
1 0', 'point, polygon', 5, false, '50f7a1d80d58140037000006', '50f7a1d80d58140037000006', '2016-11-07 14:08:06.430044', '2016-11-07 13:04:22.951532+09', false);
INSERT INTO mapsymbols (name, symbol, keywords, id, is_deleted, created_by, edited_by, edited_on, created_on, published) VALUES ('plus', '.5 0
.5 1
-99 -99
0 .5
1 .5', 'point, polygon', 4, false, '50f7a1d80d58140037000006', '50f7a1d80d58140037000006', '2016-11-07 14:08:12.013975', '2016-11-07 13:03:57.819041+09', false);
INSERT INTO mapsymbols (name, symbol, keywords, id, is_deleted, created_by, edited_by, edited_on, created_on, published) VALUES ('diamond', '0 .5
.5 0
1 .5
.5 1
0 .5', 'point, polygon', 3, false, '50f7a1d80d58140037000006', '50f7a1d80d58140037000006', '2016-11-07 14:08:20.818104', '2016-11-07 13:03:25.4225+09', false);
INSERT INTO mapsymbols (name, symbol, keywords, id, is_deleted, created_by, edited_by, edited_on, created_on, published) VALUES ('tent', '0 1
.5 0
1 1
.75 1
.5 .5
.25 1
0 1', 'point, polygon', 2, false, '50f7a1d80d58140037000006', '50f7a1d80d58140037000006', '2016-11-07 14:08:31.904554', '2016-11-05 12:17:46.754353+09', false);
INSERT INTO mapsymbols (name, symbol, keywords, id, is_deleted, created_by, edited_by, edited_on, created_on, published) VALUES ('square', '0 0
0 10
10 10
10 0
0 0', 'point, polygon', 1, false, '50f7a1d80d58140037000006', '50f7a1d80d58140037000006', '2016-11-07 14:08:35.613961', '2016-11-05 12:05:42.189881+09', false);
INSERT INTO mapsymbols (name, symbol, keywords, id, is_deleted, created_by, edited_by, edited_on, created_on, published) VALUES ('v-poly', '0  0
3.5 8
7 0
5.2 0
3.5 4
1.8 0
0 0', 'point, polygon', 13, false, '50f7a1d80d58140037000006', '50f7a1d80d58140037000006', '2016-11-07 14:49:39.098099', '2016-11-07 14:49:30.838117+09', false);
