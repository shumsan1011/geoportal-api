DELETE FROM users WHERE id = 1 OR id = 2;
INSERT INTO users (id, about, email, fullname, password, language, locked, username) VALUES (1, 'Administrator', 'admin@example.com', 'administrator', 'dc0c3e758040cbfa998c52911119d36a', 'en', FALSE, 'admin'); 
INSERT INTO users (id, about, email, fullname, language, locked, username) VALUES (2, 'System', 'system@example.com', 'system', 'en', FALSE, 'system'); 
CREATE SCHEMA IF NOT EXISTS gis_admin;
SELECT pg_catalog.setval(pg_get_serial_sequence('public.users', 'id'), MAX(id)) FROM public.users;