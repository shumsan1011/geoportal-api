INSERT INTO public.table_access_rules (id, table_id, user_id, waiting_for_approval, access_mode, is_deleted) VALUES (5, 5, 'anonym', false, '110', false);
INSERT INTO public.table_access_rules (id, table_id, user_id, waiting_for_approval, access_mode, is_deleted) VALUES (6, 5, 'registered', false, '000', false);
INSERT INTO public.table_access_rules (id, table_id, user_id, waiting_for_approval, access_mode, is_deleted) VALUES (7, 5, '2', false, '222', false);
INSERT INTO public.table_access_rules (id, table_id, user_id, waiting_for_approval, access_mode, is_deleted) VALUES (8, 6, '2', false, '222', false);
INSERT INTO public.table_access_rules (id, table_id, user_id, waiting_for_approval, access_mode, is_deleted) VALUES (9, 100, 'anonym', false, '100', false);
INSERT INTO public.table_access_rules (id, table_id, user_id, waiting_for_approval, access_mode, is_deleted) VALUES (10, 185, 'anonym', false, '100', false);
SELECT pg_catalog.setval(pg_get_serial_sequence('public.table_access_rules', 'id'), MAX(id)) FROM public.table_access_rules;