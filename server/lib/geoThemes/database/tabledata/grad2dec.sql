CREATE OR REPLACE FUNCTION public.grad2dec(lon character varying)
  RETURNS real AS
$BODY$
DECLARE
    lon_a character varying[];
    rlon_a real[];
BEGIN
	
    lon_a=regexp_split_to_array(lon, '�|'''); 
    --return array_length(lon_a, 1);

    --//a=string_to_array(lan, '�''');
    if array_length(lon_a, 1)=0 or lon_a[1]='' then
	return NULL;
    end if;
    rlon_a[1]=lon_a[1];
    
    if array_length(lon_a, 1)<2 or lon_a[2]='' then
	rlon_a[2]=0;
    else 
	rlon_a[2]=lon_a[2];
    end if;
    
    if array_length(lon_a, 1)<3 or lon_a[3]='' then
	rlon_a[3]=0;
    else 
	rlon_a[3]=lon_a[3];
    end if;    

    
    rlon_a[1]=rlon_a[1];
    rlon_a[2]=rlon_a[2]/60;
    rlon_a[3]=rlon_a[3]/3600;
    
    
    RETURN rlon_a[1]+rlon_a[2]+rlon_a[3];
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;