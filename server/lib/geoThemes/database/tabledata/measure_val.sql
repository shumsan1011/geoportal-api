CREATE TABLE IF NOT EXISTS public.measure_val
(
  id serial NOT NULL,
  name character varying(256),
  type integer,
  symbol character varying(16),
  description character varying(1024),
  si integer,
  CONSTRAINT measure_val_pkey PRIMARY KEY (id)
)