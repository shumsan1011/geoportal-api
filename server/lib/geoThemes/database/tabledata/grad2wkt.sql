CREATE OR REPLACE FUNCTION public.grad2wkt(lat character varying, lon character varying)
  RETURNS character varying AS
$BODY$
DECLARE
    rlat real;
    rlon real;
BEGIN
	
   select gis_admin.grad2dec(lat) into rlat;
   select gis_admin.grad2dec(lon) into rlon;
 
   RETURN 'MULTIPOINT('||rlat || ' ' || rlon||')';
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;