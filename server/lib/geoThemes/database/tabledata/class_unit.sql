CREATE TABLE IF NOT EXISTS public.class_unit
(
  id serial NOT NULL,
  name character varying(256),
  number_code character varying(5),
  rus_name1 character varying(50) DEFAULT NULL::character varying,
  eng_name1 character varying(50) DEFAULT NULL::character varying,
  rus_name2 character varying(50) DEFAULT NULL::character varying,
  eng_name2 character varying(50) DEFAULT NULL::character varying,
  measure_val integer,
  class_unit_type_id integer,
  factor numeric,
  visible integer NOT NULL DEFAULT 1,
  comment character varying(255) DEFAULT NULL::character varying,
  CONSTRAINT class_unit_pkey PRIMARY KEY (id)
)