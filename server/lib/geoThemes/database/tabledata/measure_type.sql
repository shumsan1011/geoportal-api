CREATE TABLE IF NOT EXISTS public.measure_type
(
  id serial NOT NULL,
  name character varying(256),
  CONSTRAINT measure_type_pkey PRIMARY KEY (id)
)