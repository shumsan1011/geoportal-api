TRUNCATE TABLE sample_theme;
INSERT INTO sample_theme (id, name, value, is_deleted, created_by, edited_by, published) VALUES (2, 'test_1_name', 'test_1_value', false, '0', NULL, TRUE);
INSERT INTO sample_theme (id, name, geom, value, is_deleted, created_by, edited_by, published) VALUES (3, 'test_2_name', ST_GeomFromText('MULTIPOINT((87.40722 51.79502))', 4326), 'test_2_value', false, '0', NULL, TRUE);
UPDATE public.geo_dataset SET access_mode = 1, created_by = '1' WHERE id = 20;
SELECT pg_catalog.setval(pg_get_serial_sequence('public.sample_theme', 'id'), MAX(id)) FROM public.sample_theme;