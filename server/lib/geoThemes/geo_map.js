import fs from 'fs';
import config from '../../../config/config';
import geo_table from './geo_table';
import { getpath_on_userdir, copyFile } from '../rfm';
import { log, explode, is_number, getcolumn } from '../helpers';

/**
 * Converting Windows UNC style to the Linux one (assuming
 * that target format starts with /s root folder.
 *
 * @param {String} filepath Converted filepath
 *
 * @return {String}
 */
function toUNIXUNC(filepath) {
  filepath = filepath.replace(new RegExp('[a-zA-Z]:(\\\\|\\/)', 'i'), '/s/');
  return filepath;
}

/**
 * Converting locally processed filepath to the
 * filepath at the remote system.
 *
 * @param {String} filepath Converted filepath
 *
 * @return {String}
 */
function toMapserverPath(filepath) {
  if (!filepath) return '';

  var searchedRegExp = config.modules.geothemes.mapfiledir.replace(
    new RegExp('\\\\{1,2}', 'g'),
    '(\\\\{1,2}|\\/{1,2})'
  );
  var modifiedFilepath = filepath.replace(
    new RegExp(searchedRegExp),
    config.modules.geothemes.remoteHostMapFilesPath
  );
  return modifiedFilepath;
}

var font_symbols = [
  {
    name: 'MapInfoSymbols',
    startsymbol: 34,
    endsymbol: 68
  }
];

function geo_map(mapfilename, map_file_dir, user_id, dbclient, meta_manager) {
  /*
	*	Creates map file for table's geometry column
	*/
  var gmap = this;

  var filepath = map_file_dir + '/' + mapfilename + '.map';
  var mapserver_path = config.modules.geothemes.mapserver_path;

  this.createMapForDB = function(
    fieldname,
    mapstyle,
    mdobj,
    selection_array,
    query,
    callback
  ) {
    var sql = '';
    var column = getcolumn(fieldname, mdobj);
    var pfld = getcolumn(column.parentfield, mdobj);

    var metadata = {
      pg_user: config.db.user,
      pg_password: config.db.password,
      pg_db_name: config.db.name,
      pg_host: config.db.host,
      tablename: mdobj.tablename,
      indexcolumn: mdobj.columns[0].fieldname
    };

    var map_options_path = map_file_dir + '/' + mapfilename + '.options';
    var map_symbols_path = map_file_dir + '/' + mapfilename + '.sym';
    var tag_symbols = mapfilename + '.sym';

    var map_options = { mapstyle: '', selection: '', query: '' };
    if (fs.existsSync(map_options_path)) {
      let map_options_str = fs.readFileSync(map_options_path);
      map_options = JSON.parse(map_options_str);
    }

    var data = '';
    var style_default;
    switch (column.type) {
      case 'point':
        data = fs.readFileSync(
          __dirname + '/maptemplates/point_template.map',
          'utf8'
        );
        style_default = fs.readFileSync(
          __dirname + '/maptemplates/point_template.style',
          'utf8'
        );
        break;
      case 'line':
        data = fs.readFileSync(
          __dirname + '/maptemplates/line_template.map',
          'utf8'
        );
        style_default = fs.readFileSync(
          __dirname + '/maptemplates/line_template.style',
          'utf8'
        );
        break;
      case 'polygon':
        data = fs.readFileSync(
          __dirname + '/maptemplates/polygon_template.map',
          'utf8'
        );
        style_default = fs.readFileSync(
          __dirname + '/maptemplates/polygon_template.style',
          'utf8'
        );
        break;
      default:
        if (callback) callback('error');
        return;
    }

    style_default = style_default.replace(/#fieldname/g, column.fieldname);
    if (pfld != null && column.parentfield != 'id' && mapstyle == '') {
      mapstyle = map_options.mapstyle;
    }

    if (mapstyle == '' && map_options.mapstyle != '') {
      mapstyle = map_options.mapstyle;
    } else if (mapstyle == '') {
      mapstyle = style_default;
    }

    var geom_fieldname = '';

    function write_mapfile() {
      map_options.mapstyle = mapstyle;
      if (selection_array == '-' && map_options.selection != '') {
        selection_array = map_options.selection;
      }

      map_options.selection = selection_array;
      //log('write_mapfile');
      //log(mapstyle);

      let str_filter = '';
      data = data.replace(/tag_style/g, mapstyle);
      if (selection_array != '') {
        ids = selection_array.split(',');
        if (ids.length == 0) {
          str_filter = 'main.ID=-1';
        } else {
          str_filter = '';
          for (var i = 0; i < ids.length; i++) {
            if (str_filter != '') str_filter += ' OR ';

            str_filter += 'main.ID=' + ids[i];
          }
        }
      } else {
        str_filter = 'main.id=-1';
      }

      str_filter = ' ' + str_filter;
      data = data.replace(/tag_selection_filter/g, str_filter);

      //data=data.replace(/&quot;/g, '"');
      data = data.replace(/tag_filepath/g, filepath);
      data = data.replace(/tag_fieldname/g, column.fieldname);
      data = data.replace(/tag_user/g, metadata.pg_user);
      data = data.replace(/tag_password/g, metadata.pg_password);
      data = data.replace(/tag_db_name/g, metadata.pg_db_name);
      data = data.replace(/tag_host/g, metadata.pg_host);
      data = data.replace(/tag_table/g, metadata.tablename);
      data = data.replace(/tag_host/g, metadata.pg_host);
      data = data.replace(/tag_port/g, metadata.pg_port);

      data = data.replace(/tag_mapserver_path/g, mapserver_path);
      data = data.replace(/tag_map_file_dir/g, map_file_dir);
      data = data.replace(/tag_symbols/g, tag_symbols);

      sql = sql.replace(/"/g, '\\"');
      data = data.replace(/tag_query/g, sql);

      if (
        column.widget.properties.label != undefined &&
        column.widget.properties.label != ''
      )
        data = data.replace(
          /tag_indexcolumn/g,
          'LABELITEM "' + column.widget.properties.label + '"'
        );
      else data = data.replace(/tag_indexcolumn/g, '');

      data = data.replace(/tag_geom/g, column.fieldname);

      if (data != '') {
        fs.writeFileSync(filepath, data, 'utf-8', function(err) {
          if (err) {
            log(err);
          }
        });
      } else {
        if (callback) callback('error');
      }

      let map_options_str = JSON.stringify(map_options);
      fs.writeFileSync(map_options_path, map_options_str, 'utf-8', function(
        err
      ) {
        if (err) {
          log(err);
        }
      });

      var mod_filepath = filepath.replace(/\\/g, '\\\\');
      if (callback) callback(null, mapserver_path + '?map=' + mod_filepath);

      gmap.update_symbols(map_symbols_path, fieldname, mdobj);
    }

    sql = query;
    map_options.query = sql;
    write_mapfile();
  };

  this.createMapForFile = function(
    sourcepath,
    mapstyle,
    from_color,
    to_color,
    req_date,
    callback
  ) {
    if (callback === undefined) return 'error';
    if (from_color == null) {
      var from_color = {
        r: Math.floor(Math.random() * 120 + 128),
        g: Math.floor(Math.random() * 120 + 128),
        b: Math.floor(Math.random() * 120 + 128)
      };
    }
    if (to_color == null) {
      var to_color = {
        r: Math.floor(Math.random() * 120 + 128),
        g: Math.floor(Math.random() * 120 + 128),
        b: Math.floor(Math.random() * 120 + 128)
      };
    }

    var rngmin = 0;
    var rngmax = 0;
    if (sourcepath[sourcepath.length - 1] == '/')
      sourcepath = sourcepath.substring(0, sourcepath.length - 1);

    var dirpath = toUNIXUNC(
      sourcepath.substring(0, sourcepath.lastIndexOf('/'))
    );

    var filename = sourcepath.substring(
      sourcepath.lastIndexOf('/') + 1,
      sourcepath.length
    );

    var filenameclean = filename.substring(0, filename.lastIndexOf('.'));
    var ext = filename
      .substring(filename.lastIndexOf('.') + 1, filename.length)
      .toUpperCase();
    var min_raster_scale = 0;
    var max_raster_scale = 0;
    var topaste = new Object();
    var ret_style = '';
    if (mapstyle) topaste.tag_style = mapstyle;

    function createMapForMTiff(sourcepath, ext_type) {
      var filename2 = sourcepath.substring(
        sourcepath.lastIndexOf('/') + 1,
        sourcepath.length
      );
      var filenameclean2 = filename2.substring(0, filename2.lastIndexOf('.'));
      var terminal = require('child_process').spawn('gdalinfo', [
        '-hist',
        sourcepath
      ]);
      terminal.stdout.on('data', function(rdata) {
        topaste.tag_imagetype = 'PNG';
        topaste.tag_shapepath = dirpath;
        topaste.tag_mappath = filepath.replace('/', '\\');
        topaste.tag_layer_name = filenameclean;
        topaste.tag_layer_data = filenameclean2;
        var arr = explode('\r\n', '' + rdata);
        for (var i = 0; i < arr.length; i++) {
          // Detecting projection
          if (arr[i].indexOf('PROJCS[') != -1) {
            if (arr[i].indexOf('WGS 84 / UTM zone 48N') > 0) {
              topaste.tag_projection =
                '"proj=utm"\
											"zone=48"\
											"ellps=WGS84"\
											"datum=WGS84"\
											"units=m"\
											"no_defs"';
            }
          } else if (arr[i].indexOf('GEOGCS[') != -1) {
            if (arr[i].indexOf('WGS 84 / UTM zone 48N') > 0) {
              topaste.tag_projection =
                '"proj=utm"\
											"zone=48"\
											"ellps=WGS84"\
											"datum=WGS84"\
											"units=m"\
											"no_defs"';
            }
          }

          if (!topaste.tag_projection) {
            topaste.tag_projection = '"init=epsg:4326"';
          }

          // Detecting lower left
          if (arr[i].indexOf('Lower Left  (') != -1) {
            var tempo = arr[i].replace('Lower Left  ( ', '');
            tempo = tempo.substr(0, tempo.indexOf(')'));
            var tempoarr = explode(',', tempo);
            topaste.tag_extent_minx = parseFloat(tempoarr[0].trim());
            topaste.tag_extent_miny = parseFloat(tempoarr[1].trim());
          }

          // Detecting upper right
          if (arr[i].indexOf('Upper Right (') != -1) {
            var tempo = arr[i].replace('Upper Right (', '');
            tempo = tempo.substr(0, tempo.indexOf(')'));
            var tempoarr = explode(',', tempo);
            topaste.tag_extent_maxx = parseFloat(tempoarr[0].trim());
            topaste.tag_extent_maxy = parseFloat(tempoarr[1].trim());
          }

          if (topaste.tag_extent_minx > topaste.tag_extent_maxx) {
            var temporaryValue = topaste.tag_extent_maxx;
            topaste.tag_extent_maxx = topaste.tag_extent_minx;
            topaste.tag_extent_minx = temporaryValue;
          }

          if (topaste.tag_extent_miny > topaste.tag_extent_maxy) {
            var temporaryValue = topaste.tag_extent_maxy;
            topaste.tag_extent_maxy = topaste.tag_extent_miny;
            topaste.tag_extent_miny = temporaryValue;
          }

          // Detecting size
          if (arr[i].indexOf('Size is ') != -1) {
            var tempo = arr[i].replace('Size is ', '');
            var tempoarr = explode(', ', tempo);
            topaste.tag_size_x = tempoarr[0];
            topaste.tag_size_y = tempoarr[1];
          }
          // Detecting stmax
          if (arr[i].indexOf('    STATISTICS_MAXIMUM') != -1) {
            if (ext_type == 'MTIF') var tempo = rngmax;
            else {
              var tempo = arr[i].replace('    STATISTICS_MAXIMUM=', '');
            }
            topaste.tag_stmax = tempo;
            max_raster_scale = tempo;
            //log('STATISTICS_MAX');
            //log(tempo);
          }
          // Detecting stmin
          if (arr[i].indexOf('    STATISTICS_MINIMUM') != -1) {
            if (ext_type == 'MTIF') {
              var tempo = rngmin;
            } else {
              var tempo = arr[i].replace('    STATISTICS_MINIMUM=', '');
            }
            topaste.tag_stmin = tempo;
            min_raster_scale = tempo;
            //log('STATISTICS_MINIMUM');
            //log(tempo);
          }
          // Detecting type
          if (arr[i].indexOf('Geometry: ') != -1) {
            if (arr[i].indexOf('Polygon') != -1) {
              topaste.tag_layer_type = 'POLYGON';
              topaste.tag_layer_symbol = 'line1';
              topaste.tag_layer_symsize = '5';
            } else if (arr[i].indexOf('Point') != -1) {
              topaste.tag_layer_type = 'POINT';
              topaste.tag_layer_symbol = 'circle';
              topaste.tag_layer_symsize = '10';
            } else if (arr[i].indexOf('Line') != -1) {
              topaste.tag_layer_type = 'LINE';
              topaste.tag_layer_symbol = 'line1';
              topaste.tag_layer_symsize = '2';
            }
          }
        }
        topaste.tag_layer_color_min_r = from_color.r;
        topaste.tag_layer_color_min_g = from_color.g;
        topaste.tag_layer_color_min_b = from_color.b;

        topaste.tag_layer_color_max_r = to_color.r;
        topaste.tag_layer_color_max_g = to_color.g;
        topaste.tag_layer_color_max_b = to_color.b;
      });
      terminal.stdout.on('end', function(rdata) {
        var data = fs.readFileSync(
          rootpath +
            '/modules/community/geothemes/maptemplates/rastr_template.map',
          'utf8'
        );
        data = populateTemplate(data, topaste);
        fileCreationBasedOnData(data);
      });
    }

    function createMapForTiff(sourcepath, ext_type) {
      var filename2 = sourcepath.substring(
        sourcepath.lastIndexOf('/') + 1,
        sourcepath.length
      );
      var filenameclean2 = filename2.substring(0, filename2.lastIndexOf('.'));
      var terminal = require('child_process').execFile('gdalinfo', [
        '-stats',
        '-hist',
        sourcepath
      ]);
      topaste.tag_layer_type = 'tiff';
      terminal.stdout.on('data', function(rdata) {
        topaste.tag_imagetype = 'PNG';
        topaste.tag_shapepath = dirpath;
        topaste.tag_mappath = filepath.replace('/', '\\');
        topaste.tag_layer_name = filenameclean;
        topaste.tag_layer_data = filename2;
        var arr = explode('\r\n', '' + rdata);
        for (var i = 0; i < arr.length; i++) {
          // Detecting projection
          if (arr[i].indexOf('PROJCS[') != -1) {
            if (arr[i].indexOf('WGS 84 / UTM zone 48N') > 0) {
              topaste.tag_projection =
                '"proj=utm"\
											"zone=48"\
											"ellps=WGS84"\
											"datum=WGS84"\
											"units=m"\
											"no_defs"';
            }
          } else if (arr[i].indexOf('GEOGCS[') != -1) {
            if (arr[i].indexOf('WGS 84 / UTM zone 48N') > 0) {
              topaste.tag_projection =
                '"proj=utm"\
											"zone=48"\
											"ellps=WGS84"\
											"datum=WGS84"\
											"units=m"\
											"no_defs"';
            }
          }

          if (!topaste.tag_projection) {
            topaste.tag_projection = '"init=epsg:4326"';
          }

          // Detecting lower left
          if (arr[i].indexOf('Lower Left  (') != -1) {
            var tempo = arr[i].replace('Lower Left  ( ', '');
            tempo = tempo.substr(0, tempo.indexOf(')'));
            var tempoarr = explode(',', tempo);
            topaste.tag_extent_minx = parseFloat(tempoarr[0].trim());
            topaste.tag_extent_miny = parseFloat(tempoarr[1].trim());
          }

          // Detecting upper right
          if (arr[i].indexOf('Upper Right (') != -1) {
            var tempo = arr[i].replace('Upper Right (', '');
            tempo = tempo.substr(0, tempo.indexOf(')'));
            var tempoarr = explode(',', tempo);
            topaste.tag_extent_maxx = parseFloat(tempoarr[0].trim());
            topaste.tag_extent_maxy = parseFloat(tempoarr[1].trim());
          }

          if (topaste.tag_extent_minx > topaste.tag_extent_maxx) {
            var temporaryValue = topaste.tag_extent_maxx;
            topaste.tag_extent_maxx = topaste.tag_extent_minx;
            topaste.tag_extent_minx = temporaryValue;
          }

          if (topaste.tag_extent_miny > topaste.tag_extent_maxy) {
            var temporaryValue = topaste.tag_extent_maxy;
            topaste.tag_extent_maxy = topaste.tag_extent_miny;
            topaste.tag_extent_miny = temporaryValue;
          }

          // Detecting size
          if (arr[i].indexOf('Size is ') != -1) {
            var tempo = arr[i].replace('Size is ', '');
            var tempoarr = explode(', ', tempo);
            topaste.tag_size_x = tempoarr[0];
            topaste.tag_size_y = tempoarr[1];
          }

          if (arr[i].indexOf('STATISTICS_MAXIMUM') != -1) {
            if (ext_type == 'MTIF') {
              var tempo = rngmax;
            } else {
              var tempo = arr[i].replace('STATISTICS_MAXIMUM=', '');
            }

            topaste.tag_stmax = tempo.trim();
            max_raster_scale = tempo;
          }

          // Detecting stmin
          if (arr[i].indexOf('STATISTICS_MINIMUM') != -1) {
            if (ext_type == 'MTIF') {
              var tempo = rngmin;
            } else {
              var tempo = arr[i].replace('STATISTICS_MINIMUM=', '');
            }

            topaste.tag_stmin = tempo.trim();
            min_raster_scale = tempo;
          }

          // Detecting type
          if (arr[i].indexOf('Geometry: ') != -1) {
            if (arr[i].indexOf('Polygon') != -1) {
              topaste.tag_layer_type = 'POLYGON';
              topaste.tag_layer_symbol = 'line1';
              topaste.tag_layer_symsize = '5';
            } else if (arr[i].indexOf('Point') != -1) {
              topaste.tag_layer_type = 'POINT';
              topaste.tag_layer_symbol = 'circle';
              topaste.tag_layer_symsize = '10';
            } else if (arr[i].indexOf('Line') != -1) {
              topaste.tag_layer_type = 'LINE';
              topaste.tag_layer_symbol = 'line1';
              topaste.tag_layer_symsize = '2';
            }
          }
        }
        topaste.tag_layer_color_min_r = from_color.r;
        topaste.tag_layer_color_min_g = from_color.g;
        topaste.tag_layer_color_min_b = from_color.b;

        topaste.tag_layer_color_max_r = to_color.r;
        topaste.tag_layer_color_max_g = to_color.g;
        topaste.tag_layer_color_max_b = to_color.b;
      });

      terminal.stdout.on('end', function(rdata) {
        for (var key in topaste) {
          if (!topaste[key]) {
            log(
              'Invalid tag ' +
                key +
                ' with value ' +
                topaste[key] +
                ' in ' +
                sourcepath
            );
          }
        }

        if (!topaste.tag_stmin) {
          log('Unable to get STATISTICS_MINIMUM for ' + sourcepath);
        }

        if (!topaste.tag_stmax) {
          log('Unable to get STATISTICS_MAXIMUM for ' + sourcepath);
        }

        var data = fs.readFileSync(
          __dirname + '/maptemplates/rastr_template.map',
          'utf8'
        );
        data = populateTemplate(data, topaste);
        fileCreationBasedOnData(data);
      });
    }

    if (ext == 'SHP') {
      // Shapefile case
      var terminal = require('child_process').execFile('ogrinfo', [
        '-so',
        sourcepath,
        filenameclean
      ]);
      terminal.stdout.on('data', function(rdata) {
        log(rdata);
        topaste.tag_shapepath = dirpath;
        topaste.tag_mappath = filepath.replace('/', '\\');
        topaste.tag_layer_name = filenameclean;
        topaste.tag_layer_data = filenameclean;

        var arr = explode('\r\n', '' + rdata);
        for (var i = 0; i < arr.length; i++) {
          // Detecting extent
          if (arr[i].indexOf('Extent: ') != -1) {
            var tempo = arr[i].replace('Extent: ', '');
            tempo = tempo.replace(') - (', ', ');
            tempo = tempo.replace('(', '');
            tempo = tempo.replace(')', '');
            var tempoarr = explode(', ', tempo);
            topaste.tag_extent_minx = tempoarr[0];
            topaste.tag_extent_miny = tempoarr[1];
            topaste.tag_extent_maxx = tempoarr[2];
            topaste.tag_extent_maxy = tempoarr[3];
          }
          // Detecting type
          if (arr[i].indexOf('Geometry: ') != -1) {
            if (arr[i].indexOf('Polygon') != -1) {
              topaste.tag_layer_type = 'POLYGON';
              topaste.tag_layer_symbol = 'line1';
              topaste.tag_layer_symsize = '5';
            } else if (arr[i].indexOf('Point') != -1) {
              topaste.tag_layer_type = 'POINT';
              topaste.tag_layer_symbol = 'circle';
              topaste.tag_layer_symsize = '10';
            } else if (arr[i].indexOf('Line') != -1) {
              topaste.tag_layer_type = 'LINE';
              topaste.tag_layer_symbol = 'line1';
              topaste.tag_layer_symsize = '2';
            }
            // Needs revising
            topaste.tag_layer_color_r = from_color.r;
            topaste.tag_layer_color_g = from_color.g;
            topaste.tag_layer_color_b = from_color.b;
          }
        }
      });
      terminal.stdout.on('end', function(rdata) {
        log('end');
        log(rdata);
        var data = fs.readFileSync(
          rootpath +
            '/modules/community/geothemes/maptemplates/vector_template.map',
          'utf8'
        );
        data = populateTemplate(data, topaste);
        fileCreationBasedOnData(data);
      });
      terminal.on('exit', function(code) {
        if (code != 0) {
          log('Failed: ' + code);
        }
      });
    } else if (ext == 'MTIF') {
      if (req_date != undefined && !is_number(req_date))
        var req_date = Date.parse(req_date);
      var data = fs.readFileSync(sourcepath, 'utf8');
      var dir_separator = '/';
      var dir = sourcepath.slice(0, sourcepath.lastIndexOf(dir_separator) + 1);
      var rngfile =
        sourcepath.slice(0, sourcepath.lastIndexOf('.') + 1) + 'rng';

      var rngtxt = fs.readFileSync(rngfile, 'utf8');
      var row = explode(' ', rngtxt);
      rngmin = row[0];
      rngmax = row[1];

      var outputobj = new Object();
      outputobj.data = new Array();
      var arr = explode('\r\n', '' + data);
      var idealfilepath = filepath;
      for (var i = 0; i < arr.length; i++) {
        var row = explode(' ', arr[i]);
        var c_date = Date.parse(row[0]);
        if (req_date === undefined) {
          createMapForMTiff(dir + row[1], ext);
          outputobj.data.push({ mapfile: filepath, time: c_date });
        } else if (is_number(req_date)) {
          if (req_date == row[0]) {
            createMapForMTiff(dir + row[1], ext);
            outputobj.data.push({ mapfile: filepath, time: c_date });
          }
        } else {
          if (req_date.getTime() > c_date.getTime()) {
            createMapForMTiff(dir + row[1], ext);
            outputobj.data.push({ mapfile: filepath, time: c_date });
          }
        }
      }

      return outputobj;
    } else if (ext == 'TIF' || ext == 'TIFF') {
      createMapForTiff(sourcepath, ext);
    } else {
      return;
    }

    function populateTemplate(data, topaste) {
      for (var key in topaste) {
        data = data.replace(key, topaste[key]);
      }
      return data;
    }

    function fileCreationBasedOnData(data) {
      if (data != '') {
        fs.writeFileSync(filepath, data, 'utf-8', function(err) {
          if (err) {
            log(err);
          }
        });

        callback({
          status: 'success',
          wms_link: mapserver_path + '?map=' + toMapserverPath(filepath),
          type: topaste.tag_layer_type,
          map_style: ret_style
        });

        return 'error';
      }

      callback({ status: 'error' });
    }
  };

  this.update_symbols = function(symbols_path, fieldname, mdobj) {
    var select_query = 'select name, symbol from public.mapsymbols';
    var def_symbols = '';
    var def_symbols_path = map_file_dir + '\\symbols\\def_symbols.sym';
    if (fs.existsSync(def_symbols_path)) {
      def_symbols = fs.readFileSync(def_symbols_path);
    }

    log('symbols_path=' + symbols_path);
    var mainrootdir = config.modules.rfm.mainrootdir;

    var symbol_content = '';
    var column = getcolumn(fieldname, mdobj);
    if (column && column.widget && column.widget.style) {
      var style = JSON.parse(column.widget.style);
      if (style.rules) {
        for (var i = 0; i < style.rules.length; i++) {
          if (!style.rules[i].symbolizers) continue;
          for (var j = 0; j < style.rules[i].symbolizers.length; j++) {
            if (
              style.rules[i].symbolizers[j].image &&
              style.rules[i].symbolizers[j].image != ''
            ) {
              file_path = getpath_on_userdir(
                mainrootdir,
                style.rules[i].symbolizers[j].image
              );
              ext = file_path
                .substr(file_path.lastIndexOf('.') + 1)
                .toLowerCase();
              new_path =
                map_file_dir +
                '/images/' +
                style.rules[i].symbolizers[j].image +
                '.' +
                ext;
              filename =
                'images/' + style.rules[i].symbolizers[j].image + '.' + ext;
              copyFile(file_path, new_path, function(error) {
                if (error) log(error);
              });

              symbol_content +=
                'SYMBOL\n\
  NAME "' +
                style.rules[i].symbolizers[j].image +
                '"\n\
  TYPE pixmap\n\
  IMAGE "' +
                filename +
                '"\n\
END\n';
            }
          }
        }
      }
    }
    for (var i = 0; i < font_symbols.length; i++) {
      for (
        var j = font_symbols[i].startsymbol;
        j <= font_symbols[i].endsymbol;
        j++
      ) {
        symbol_content +=
          'SYMBOL\n\
	NAME "' +
          font_symbols[i].name +
          j +
          '"\n\
	TYPE truetype\n\
	FONT "' +
          font_symbols[i].name +
          '"\n\
	CHARACTER "&#' +
          j +
          ';"\n\
END\n';
      }
    }

    var query = dbclient.query(select_query, function(
      error,
      query_result,
      unofields2
    ) {
      if (error === null) {
        for (var i = 0; i < query_result.rows.length; i++) {
          if (
            !query_result.rows[i].symbol ||
            query_result.rows[i].symbol == '' ||
            query_result.rows[i].name == 'image' ||
            query_result.rows[i].symbol == 'font symbol'
          )
            continue;
          symbol_content +=
            "\
Symbol\n\
Name '" +
            query_result.rows[i].name +
            "'\n\
Type VECTOR\n\
Filled TRUE\n\
Points\n\
" +
            query_result.rows[i].symbol +
            '\n\
END\n\
END\n';
        }

        symbol_content = def_symbols + '\n' + symbol_content;
        if (symbol_content != '\n') {
          fs.writeFileSync(symbols_path, symbol_content, 'utf-8', function(
            err
          ) {
            if (err) {
              log(err);
            }
          });
        }
      } else {
        log(select_query);
        callback(error.message);
      }
    });
  };
  return this;
}

export default {
  geo_map,
  toMapserverPath
};
