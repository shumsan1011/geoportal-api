import fs from 'fs';
import mime from 'mime';
import path from 'path';
import geo_table from './geo_table';
import get_table_json from './get_table_json';
import db from '../db';
import config from '../../../config/config';
import { APIErrors, prepareParams, log, _qv } from '../helpers';
import { meta_manager } from './geo_meta_table';
import { service_fields } from './service_fields';
import { getpath_on_userdir, getpath } from '../rfm';
import { hooks, triggers } from './../../controllers/geoThemes/hooks';

const client = db.getClient();

const clearInputFromServiceFields = (data) => {
  for (var dataKey in data) {
    for (var key in service_fields) {
      if (service_fields[key].name === dataKey) {
        delete data[dataKey];
      }
    }
  }

  return data;
};

/**
 * Defining basic access constants for current user, trying to access dataset
 */
function user_dataset_access(req, dataset_id, successcallback, errorcallback) {
	req.act_list = new Array();
	var l_list = new Array();

  let { user_id } = prepareParams(req);

	var sql = '';
	if (user_id === '') {
    sql = 'SELECT public.dataset_access(' + dataset_id + ', \'\') as g_access';
	} else {
		sql = 'SELECT public.dataset_access(' + dataset_id + ', \'' + user_id + '\') as g_access;';
  }

	var query = client.query(sql, function(error, unorows, unofields) {
		if (error === null)	{
      let g_access = unorows.rows[0].g_access;
      console.log('##### g_access', g_access);
      if (g_access && (g_access.length === 3 || g_access.length === 4)) {
        // According to the dataset_access.sql
        let result = {
          view: parseInt(g_access[0]),
          edit: parseInt(g_access[1]),
          moderation: parseInt(g_access[2]),
          publication: ((g_access.length === 4) ? parseInt(g_access[3]) : 0)
        };

        successcallback(result);
      } else {
        throw new Error('Invalid g_access value');
      }
		} else {
			errorcallback(false);
		}
	});
}


const bldata_list = (req, res) => {
  let { dataset_id, user_id, qparams } = prepareParams(req);

  log('I am here=' + dataset_id);
  let result = new Promise((resolve, reject) => {
    user_dataset_access(req, dataset_id, (userAccessRights) => {
      console.log('userAccessRights', userAccessRights);
      if (userAccessRights.view === 0) {
        reject({
          APIErrorCode: APIErrors.FORBIDDEN
        });
      } else {
        get_table_json(user_id, dataset_id, (mdobj) => {
          if (mdobj === null) {
            reject('empty metadata');
          } else {
            var g_table = new geo_table(user_id, mdobj.dataset_id, meta_manager, mdobj);

            var sLimit='';
            let iDisplayStart = _qv(qparams, 'iDisplayStart');
            let iDisplayLength = _qv(qparams, 'iDisplayLength');
            if (iDisplayStart!='' && iDisplayLength!='')	{
              sLimit = 'LIMIT ' + iDisplayLength + ' OFFSET ' + iDisplayStart;
            } else {
              sLimit = 'LIMIT 10000 OFFSET 0';
            }

            let s_fields = _qv(qparams,'s_fields');
            let includefields=[];
            if(s_fields!='') includefields=s_fields.split(',');

            var sSQL='';
            if (_qv(qparams, 'to_file')=='true') {
              g_table.documentquery(includefields, qparams, '', (error, result) => {
                if (error === null)	{
                  var aaData = '';
                  delimeter='';
                  if (result.aaData.length>0) {
                    delimeter='';
                    for (fieldname in result.aaData[0]){
                      aaData+=delimeter+fieldname;
                      delimeter='\t';
                    }
                    
                    aaData+='\n';
                  }

                  for (var r = 0; r < result.aaData.length; r++) {
                    delimeter='';
                    for (fieldname in result.aaData[r]){
                      v = result.aaData[r][fieldname];
                      if (typeof v ==='object'){
                        str=''
                        for (sname in v) {
                          if(typeof v[sname]!='object')
                            str+=v[sname];
                        }
                        
                        aaData+=delimeter+str;
                      } else {
                        aaData+=delimeter+v;
                      }

                      delimeter='\t';
                    }
                    
                    aaData+='\n';
                  }

                  resolve(aaData);
                } else {
                  log(error);
                  reject(error);
                }
              });
            } else {
              g_table.documentquery(includefields, qparams, sLimit, (error, result) => {
                log('I am here3');
                if (error === null)	{
                  resolve(result);
                } else {
                  log(error);
                  reject(error);
                }
              });
            }
          }
        });
      }
    }, () => {
      reject();
    });
  });

  return result;
};


const bldata_add = (req, res) => {
  let { dataset_id, user_id, qparams } = prepareParams(req);
  let result = new Promise((resolve, reject) => {
    hooks.apply(dataset_id, triggers.BEFORE_CREATE, req, res).then(({ req, res }) => {
      user_dataset_access(req, dataset_id, (alist) => {
        get_table_json(user_id, dataset_id, function (mdobj) {
          log('Access granted');
          if (mdobj == null) {
            reject('empty metadata');
          } else {
            let g_table = new geo_table(user_id, mdobj.dataset_id, meta_manager, mdobj);
            qparams.document = clearInputFromServiceFields(qparams.document);
            g_table.insert(qparams.document, ({ id }) => {
              hooks.apply(dataset_id, triggers.AFTER_CREATE, req, res, id).then(({ req, res, data}) => {
                resolve(data);
              });
            }, reject);
          }
        });
      }, reject);
    });
  });

  return result;
};


const bldata_update = (req, res) => {
	let { dataset_id, user_id, qparams } = prepareParams(req);
	let result = new Promise((resolve, reject) => {
		user_dataset_access(req, dataset_id, (alist) => {
			get_table_json(user_id, dataset_id, (mdobj) => {
				if (mdobj == null) {
					reject({ message: 'empty metadata', res });
				} else {
          var g_table = new geo_table(user_id, mdobj.dataset_id, meta_manager, mdobj);
          qparams.document = clearInputFromServiceFields(qparams.document);
					g_table.update(qparams.document, resolve, (message) => {
            console.log('error message', message);
            reject({ message, res });
          });
				}
			});
		}, reject);
	});

	return result;
};


const bldata_delete = (req, res) => {
  let { dataset_id, user_id, qparams } = prepareParams(req);

  let result = new Promise((resolve, reject) => {
    user_dataset_access(req, dataset_id, (alist) => {
      get_table_json(user_id, dataset_id, (mdobj) => {
        if (mdobj == null) {
          reject('empty metadata');
        } else {
          var g_table = new geo_table(user_id, mdobj.dataset_id, meta_manager, mdobj);
          g_table.delete(qparams.document, resolve, reject);
        }
      });
    }, reject);
  });

  return result;
};


const bldata_file = (req, res) => {
  let { dataset_id, user_id, qparams } = prepareParams(req);

	dataset_id = _qv(qparams, 'dataset_id');
	var row_id = _qv(qparams, 'row_id');
	var fieldname = _qv(qparams, 'fieldname');
	var filehesh = _qv(qparams, 'filehesh');
	qparams['f_' + fieldname] = filehesh.slice(3);
	qparams['f_id'] = row_id;

  get_table_json(req, dataset_id, (mdobj) => {
    log('I am here2', qparams);
    if(mdobj==null){
      res.send({status: "error", data: 'empty metadata'});
      return;
    }

    let g_table = new geo_table(user_id, mdobj.dataset_id, meta_manager, mdobj);
    let from_list=g_table.buildfrom();
    let where_list=g_table.buildwhere(qparams, true, false);

    let sSQL='SELECT main.created_by, main.edited_by, main.' + fieldname + ' FROM ' + from_list + ' WHERE ' + where_list + ' LIMIT 1';
    log(sSQL);
    client.query(sSQL, (error, query_result) => {
      log('I am here3', error, query_result);
      if (error === null && query_result.rows.length === 1)	{
        var mainrootdir = config.modules.rfm.mainrootdir;
        let file_path=getpath_on_userdir(mainrootdir,filehesh);

        log('filehesh='+filehesh);
        log('file_path='+file_path);
        if (fs.existsSync(file_path)) {
          var filename = path.basename(file_path);
          var mimetype = mime.lookup(file_path);

          res.setHeader('Content-disposition', 'attachment; filename=' + filename);
          res.setHeader('Content-type', mimetype);

          var filestream = fs.createReadStream(file_path);
          filestream.on("data", function (chunk) {
            res.write(chunk, "binary");
          });

          filestream.on("end", function () {
              log("Served file " + file_path);
              res.end();
          });

          filestream.on("error", function(err){
            log(err);
            res.write(err + "\n");
            res.end();
          });
        } else {
          error={message:'The file does not exist in the dataset.'};
          res.send(error);
          log(error);
        }
      }else{
        log(error);
        res.send(error);
      }			
    });		
  });
};


export default {
  list: bldata_list,
  add: bldata_add,
  update: bldata_update,
  delete: bldata_delete,
  file: bldata_file
};