import { meta_manager } from './geo_meta_table';

/**
 * Receives the description of the table in a form of JSON.
 *
 * @param {*} req Request object
 * @param {*} id  Identifier of the table
 *
 * @return {Promise} 
 */
const get_table_json = (user_id, id, callback) => {
  if (user_id !== '') {
    meta_manager.curent_user_id = user_id;
  } else {
    meta_manager.curent_user_id = '';
  }

  meta_manager.get_table_json({}, id, (meta) => {
    callback(meta);
  });
}

export default get_table_json;