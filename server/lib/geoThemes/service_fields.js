const service_fields = [
	{name : 'is_deleted', type : 'bool', default_value: 'false'},
	{name : 'symbol', type : 'string'},//for symbol filepath
	{name : 'created_by', type : 'string'},
	{name : 'edited_by', type : 'string'},
	{name : 'edited_on', type : 'date', indexed: 'true', default_value: 'current_timestamp'},
	{name : 'created_on', type : 'string'},
	{name : 'published', type : 'bool', default_value: 'false', indexed: 'true' }
];

export default { service_fields };