import { _qv, log } from '../helpers';

/*
	Basic permission constants
*/
var ableToAddEditViewOwn = 0;
var ableToViewOthers = 1;
var ableToEditOthers = 2;
var isOwner = 3;
var isModerator = 4;

function mysql_real_escape_string(str) {
  return str.replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, function(char) {
    switch (char) {
      case '\0':
        return '\\0';
      case '\x08':
        return '\\b';
      case '\x09':
        return '\\t';
      case '\x1a':
        return '\\z';
      case '\n':
        return '\\n';
      case '\r':
        return '\\r';
      case '"':
      case "'":
      case '\\':
      case '%':
        return '\\' + char; // prepends a backslash to backslash, percent,
      // and double/single quotes
    }
  });
}

// Inheritance functions
function extend(Child, Parent) {
  var F = function() {};
  F.prototype = Parent.prototype;
  Child.prototype = new F();
  Child.prototype.constructor = Child;
  Child.superclass = Parent.prototype;
  mixin(Child.prototype, Parent);
}

function mixin(dst, src) {
  var tobj = {};
  for (var x in src) {
    if (typeof tobj[x] == 'undefined' || tobj[x] != src[x]) {
      dst[x] = src[x];
    }
  }
}

function sql_field(object) {
  this.object = object;
  return this;
}

sql_field.prototype.select_expr = function(fn, settings) {
  if (fn == 'max' || fn == 'min' || fn == 'avg' || fn == 'count')
    return fn + '(' + this.object.tablename + '."' + this.object.sqlname + '")';
  else return this.object.tablename + '."' + this.object.sqlname + '"';
};

sql_field.prototype.where = function(tab_alias, conditions) {
  return '';
};

sql_field.prototype.sql_value = function(value) {
  return value;
};

function sql_date(object) {
  this.fieldname = object.fieldname;
  this.object = object;
  //this.tab_alias=tab_alias;
  this.select_expr = function(fn, settings) {
    if (fn == 'max' || fn == 'min' || fn == 'avg')
      return (
        'to_char(' +
        fn +
        '(' +
        object.tablename +
        '."' +
        object.sqlname +
        "\"), 'YYYY-MM-DD HH24:MI:SS')"
      );
    else if (fn == 'count') {
      return fn + '(' + object.tablename + '."' + object.sqlname + '")';
    } else if (fn.indexOf('groupby') != -1) {
      gr_part = fn.split('_');
      period = gr_part[1];
      return (
        "to_char(date_trunc('" +
        period +
        "'," +
        object.tablename +
        '."' +
        object.sqlname +
        "\"), 'YYYY-MM-DD HH24:MI:SS')"
      );
    } else
      return (
        'to_char(' +
        object.tablename +
        '."' +
        object.sqlname +
        "\", 'YYYY-MM-DD HH24:MI:SS')"
      );
  };
  this.where = function(tab_alias, conditions) {
    if (tab_alias) tab_alias = object.tablename + '.';
    else tab_alias = '';

    var sWhere = '';
    var tempoarr = conditions.split(' - ');
    if (tempoarr[0] != '∞') {
      sWhere =
        tab_alias +
        '"' +
        object.sqlname +
        '" >= to_timestamp(\'' +
        tempoarr[0] +
        "', 'YYYY-MM-DD HH24:MI:SS')";
    }
    if (tempoarr[1] != '∞') {
      if (tempoarr[0] != '∞') {
        sWhere += ' AND ';
      }
      sWhere +=
        tab_alias +
        '"' +
        object.sqlname +
        '" <= to_timestamp(\'' +
        tempoarr[1] +
        "', 'YYYY-MM-DD HH24:MI:SS') ";
    }
    return sWhere;
  };
  this.sql_value = function(value) {
    return "to_timestamp('" + value + "', 'YYYY-MM-DD HH24:MI:SS')";
  };
  return this;
}
extend(sql_date, sql_field);
/*
field.tablename=$("#tablename", fieldcontainer);
field.sqltablename=$("#sqltablename", fieldcontainer);
field.sqlname=$("#sqlname", fieldcontainer);
*/

function sql_string(object) {
  this.fieldname = object.fieldname;
  this.object = object;
  //this.tab_alias=tab_alias;
  this.select_expr = function(fn, settings) {
    if (fn == 'count')
      return fn + '(' + object.tablename + '."' + object.sqlname + '")';
    else return object.tablename + '."' + object.sqlname + '"';
  };
  this.where = function(tab_alias, conditions) {
    if (tab_alias) tab_alias = object.tablename + '.';
    else tab_alias = '';

    let v = conditions.toLowerCase();
    let vsl = v.split(' ');
    let vs = [];
    for (var i = 0; i < vsl.length; i++) {
      v = vsl[i].trim();
      if (v != '') vs.push(v);
    }

    let termWhere = '';
    if (
      object.widget.properties &&
      object.widget.properties.tsvector != '' &&
      object.widget.properties.tsvector != undefined
    ) {
      tsvector = object.widget.properties.tsvector;
      var res_st = '';
      for (var i = 0; i < vs.length; i++) {
        if (vs[i] != '') {
          if (res_st != '') res_st += ' & ';
          res_st += vs[i] + ':*';
        }
      }
      termWhere =
        tab_alias + '"' + tsvector + '" @@ to_tsquery( \'' + res_st + "')";
    } else {
      for (let l = 0; l < vs.length; l++) {
        if (termWhere != '') termWhere += ' AND ';
        termWhere +=
          'lower(' +
          tab_alias +
          '"' +
          object.sqlname +
          '")' +
          " LIKE '%" +
          vs[l] +
          "%'";
      }
    }
    return termWhere;
  };
  this.sql_value = function(value) {
    if (value === undefined) return 'NULL';
    if (typeof value == 'object') return "'" + JSON.stringify(value) + "'";
    else if (typeof value === 'string') {
      value = value.replace(/'/g, '&quot;');
      return "'" + value + "'";
    } else return 'NULL';
  };
  return this;
}
extend(sql_string, sql_field);

function sql_geometry(object, convertGeometries) {
  this.fieldname = object.fieldname;
  this.object = object;
  //this.tab_alias=tab_alias;
  this.select_expr = function(fn, period) {
    if (fn == 'count')
      return fn + '(' + object.tablename + '."' + object.sqlname + '")';
    else if (fn != '' && fn.indexOf('distance') == 0) {
      ar = fn.split('--');
      return (
        'round(cast(ST_Distance(ST_Transform(' +
        object.tablename +
        '."' +
        object.sqlname +
        '"' +
        ", 28418), ST_Transform(ST_GeomFromText('" +
        ar[1] +
        "',4326),28418)) As numeric)/1000,2)"
      );
    } else {
      if (convertGeometries)
        return 'ST_AsText(' + object.tablename + '."' + object.sqlname + '")';
      else return object.tablename + '."' + object.sqlname + '"';
    }
  };

  this.where = function(tab_alias, conditions) {
    if (tab_alias) tab_alias = object.tablename + '.';
    else tab_alias = '';

    if (conditions === false) {
      return tab_alias + '"' + object.sqlname + '" IS NOT NULL';
    } else if (conditions.indexOf('--') != -1) {
      parts = conditions.split('--');
      coordinates = parts[1];
      dist = parts[0];
      proj = '4326';
      if (dist.indexOf(':') != -1) {
        distparts = dist.split(':');
        proj = distparts[0];
        dist = distparts[1];
      }
      if (
        dist == '' ||
        isNaN(parseFloat(dist)) ||
        !isFinite(dist) ||
        proj == ''
      )
        return '';
      if (proj == '4326')
        return (
          "ST_Distance( ST_GeomFromText('" +
          coordinates +
          "',4326)," +
          tab_alias +
          '"' +
          object.sqlname +
          '")<' +
          dist
        );
      else
        return (
          "ST_Distance( ST_Transform(ST_GeomFromText('" +
          coordinates +
          "',4326), " +
          proj +
          '),ST_Transform(' +
          tab_alias +
          '"' +
          object.sqlname +
          '", ' +
          proj +
          '))<' +
          dist
        );
    } else
      return (
        "ST_Contains( ST_GeomFromText( '" +
        conditions +
        "', 4326), " +
        tab_alias +
        '"' +
        object.sqlname +
        '" )'
      );
  };
  this.sql_value = function(value) {
    if (value && value.indexOf) {
      if (value.indexOf('MULTILINESTRING(((') == 0) {
        value = value.replace('MULTILINESTRING(((', 'MULTILINESTRING((');
        value = value.replace(')))', '))');
      }
      return "ST_Multi(ST_GeomFromText('" + value + "',4326))";
    }
    return 'NULL';
  };
  return this;
}
extend(sql_geometry, sql_field);

var number_operators = {
  PropertyIsEqualTo: '=',
  PropertyIsNotEqualTo: '!=',
  PropertyIsLessThan: '<',
  PropertyIsLessThanOrEqualTo: '<=',
  PropertyIsGreaterThan: '>',
  PropertyIsGreaterThanOrEqualTo: '>='
};

function sql_number(object) {
  this.fieldname = object.fieldname;
  this.object = object;
  //	this.tab_alias=tab_alias;
  this.select_expr = function(fn, period) {
    if (fn != '' && fn != 'groupby')
      return fn + '(' + object.tablename + '."' + object.sqlname + '")';
    else return object.tablename + '."' + object.sqlname + '"';
  };

  this.where = function(tab_alias, conditions) {
    if (tab_alias) tab_alias = object.tablename + '.';
    else tab_alias = '';
    var sWhere = '';
    var tempoarr = conditions.split('-');
    if (tempoarr.length == 2 || tempoarr.length == 4) {
      if (tempoarr.length > 0) {
        sWhere =
          tab_alias +
          '"' +
          object.sqlname +
          '"' +
          number_operators[tempoarr[1]] +
          tempoarr[0];
      }
      if (tempoarr.length == 4) {
        sWhere += ' AND ';
        sWhere +=
          tab_alias +
          '"' +
          object.sqlname +
          '" ' +
          number_operators[tempoarr[3]] +
          tempoarr[2];
      }
    } else if (tempoarr.length == 1) {
      sWhere += tab_alias + '"' + object.sqlname + '"=' + tempoarr[0];
    }
    return sWhere;
  };
  this.sql_value = function(value) {
    return value;
  };
  return this;
}
extend(sql_number, sql_field);

function sql_tabaccess(object, user_id) {
  this.fieldname = object.fieldname;
  this.object = object;
  //	this.tab_alias=tab_alias;

  this.select_expr = function(fn, period) {
    if (fn == 'count') return fn + '(' + object.tablename + '."id")';
    else
      return (
        'public.dataset_access(' + object.tablename + ".id, '" + user_id + "')"
      );
  };

  this.where = function(tab_alias, conditions) {
    var sWhere = '';
    return sWhere;
  };
  this.sql_value = function(value) {
    return value;
  };
  return this;
}
extend(sql_tabaccess, sql_field);

/**
 * @param Object  settings                   SQL generation settings
 * @param Boolean settings.convertGeometries Specifies if geometries should be fetched as WKT
 */
function geo_table(user_id, dataset_id, geo_meta_table, meta, settings) {
  log('geo_table');
  if (meta === undefined) {
    log('Error in geo_table creating, meta is null');
    return;
  }

  this.curent_user_id = '';
  this.fields = [];
  this.tabs = [];
  this.details = [];

  if (!settings) settings = {};
  if ('convertGeometries' in settings === false) {
    settings.convertGeometries = true;
  }

  var g_tab = this;
  this.publishing = false;
  this.grouping = false;
  this.document_template = {};

  this.build_template_tree = function(
    m,
    tab_alias,
    includefields,
    level,
    doc_part,
    main_field
  ) {
    if (level > 4) return;
    for (var i = 0; i < m.columns.length; i++) {
      if (level > 0 && m.columns[i].fieldname == 'id') continue;
      if (level == 0 && m.columns[i].fieldname == 'published')
        this.publishing = true;
      if (
        includefields != undefined &&
        includefields.length > 0 &&
        includefields.indexOf(m.columns[i].fieldname) == -1
      )
        continue;
      if (m.columns[i].tablename === undefined)
        m.columns[i].tablename = tab_alias;
      if (m.columns[i].sqltablename === undefined)
        m.columns[i].sqltablename = meta.tablename;
      if (m.columns[i].sqlname === undefined)
        m.columns[i].sqlname = m.columns[i].fieldname;
      if (m.columns[i].widget === undefined)
        m.columns[i].widget = { name: 'edit' };

      switch (m.columns[i].widget.name) {
        case 'date':
          doc_part[m.columns[i].fieldname] = new sql_date(m.columns[i]);
          break;
        case 'point':
          doc_part[m.columns[i].fieldname] = new sql_geometry(
            m.columns[i],
            settings.convertGeometries
          );
          break;
        case 'line':
          doc_part[m.columns[i].fieldname] = new sql_geometry(
            m.columns[i],
            settings.convertGeometries
          );
          break;
        case 'polygon':
          doc_part[m.columns[i].fieldname] = new sql_geometry(
            m.columns[i],
            settings.convertGeometries
          );
          break;
        case 'string':
          doc_part[m.columns[i].fieldname] = new sql_string(m.columns[i]);
          break;
        case 'edit':
          doc_part[m.columns[i].fieldname] = new sql_string(m.columns[i]);
          break;
        case 'textarea':
          doc_part[m.columns[i].fieldname] = new sql_string(m.columns[i]);
          break;
        case 'number':
          doc_part[m.columns[i].fieldname] = new sql_number(m.columns[i]);
          break;
        //case 'tableaccess':
        //	doc_part[m.columns[i].fieldname]=new sql_tabaccess(m.columns[i]);
        //	break;
        case 'details':
          var ref_meta =
            meta.usingmeta[m.columns[i].widget.properties.dataset_id];
          var det_table = new geo_table(
            user_id,
            m.columns[i].widget.properties.dataset_id,
            geo_meta_table,
            ref_meta
          );
          this.details.push({
            tab: det_table,
            ref_meta: ref_meta,
            col_meta: m.columns[i]
          });
          break;
        case 'classify':
          doc_part[m.columns[i].fieldname] = new sql_number(m.columns[i]);
          if (
            m.columns[i].widget.properties.db_field !== undefined &&
            m.columns[i].widget.properties.db_field != ''
          )
            doc_part[m.columns[i].fieldname].displayfields =
              m.columns[i].widget.properties.db_field;

          //ref_meta=m.columns[i].widget.properties.ref_meta;
          ref_meta = meta.usingmeta[m.columns[i].widget.properties.dataset_id];
          if (ref_meta && doc_part[m.columns[i].fieldname].displayfields) {
            tab = ref_meta.tablename;
            let join_tab_alias = m.columns[i].widget.properties.reftablename;
            doc_part[m.columns[i].fieldname].join_tab_alias = join_tab_alias;
            let cond =
              tab_alias +
              '."' +
              m.columns[i].fieldname +
              '" = ' +
              join_tab_alias +
              '.id AND NOT ' +
              join_tab_alias +
              '.is_deleted';
            this.tabs.push({
              table: tab,
              alias: join_tab_alias,
              join_cond: cond
            });
            for (
              var h = 0;
              h < doc_part[m.columns[i].fieldname].displayfields.length;
              h++
            ) {
              let reffieldname =
                doc_part[m.columns[i].fieldname].displayfields[h];
              for (var d = 0; d < ref_meta.columns.length; d++) {
                if (reffieldname == ref_meta.columns[d].fieldname) {
                  var clmn = JSON.parse(JSON.stringify(ref_meta.columns[d]));
                  clmn.tablename = join_tab_alias;
                  doc_part['ref_' + reffieldname] = new sql_string(clmn);
                  doc_part['ref_' + reffieldname].fieldname =
                    'ref_' + reffieldname;
                  this.fields.push(doc_part['ref_' + reffieldname]);
                  break;
                }
              }
            }
          }
          break;
        default:
          switch (m.columns[i].type) {
            case 'integer':
              doc_part[m.columns[i].fieldname] = new sql_number(m.columns[i]);
              break;
            case 'serial':
              doc_part[m.columns[i].fieldname] = new sql_number(m.columns[i]);
              break;
            default:
              doc_part[m.columns[i].fieldname] = new sql_string(m.columns[i]);
              break;
          }
          break;
      }
      if (doc_part[m.columns[i].fieldname]) {
        this.fields.push(doc_part[m.columns[i].fieldname]);
        doc_part[m.columns[i].fieldname].main_field = main_field;
      }
    }
  };

  let tab = meta.tablename;
  let tab_alias = 'main';
  this.tabs.push({ table: tab, alias: tab_alias, join_cond: '' });
  this.build_template_tree(meta, tab_alias, [], 0, this.document_template);

  this.buildselect = function(include, qparams, groupby) {
    let sfields = [];
    this.grouping = false;

    for (var i = 0; i < this.fields.length; i++) {
      //if( include!=undefined && include.length>0 && include.indexOf(this.fields[i].fieldname)==-1)
      //	continue;
      let fn = _qv(qparams, 'g_' + this.fields[i].fieldname);
      if (fn != '') {
        this.grouping = true;
        break;
      }
    }

    var id_filter_value = _qv(qparams, 'f_id');
    for (var i = 0; i < this.fields.length; i++) {
      let fn = _qv(qparams, 'g_' + this.fields[i].fieldname);
      if (this.grouping && fn == '') fn = 'count';
      if (fn.indexOf('groupby') != -1)
        groupby.push(this.fields[i].select_expr(fn));
      if (
        include != undefined &&
        include.length > 0 &&
        include.indexOf(this.fields[i].fieldname) == -1
      )
        continue;
      if (this.fields[i].displayfields != undefined) {
      }
      sfields.push(
        this.fields[i].select_expr(fn) +
          ' AS "' +
          this.fields[i].fieldname +
          '"'
      );

      if (
        this.fields[i].fieldname == 'JSON' &&
        dataset_id == 100 &&
        id_filter_value != ''
      ) {
        sfields.push(
          'public.dataset_access(' +
            id_filter_value +
            ", '" +
            user_id +
            "') AS a_access"
        );
        sfields.push('description AS dataset_description');
        sfields.push('id AS dataset_id');
      }

      //adding calculating fields
      let cf = _qv(qparams, 'calc_' + this.fields[i].fieldname);
      if (cf != '') {
        sfields.push(
          this.fields[i].select_expr(cf) +
            ' AS "calc_' +
            this.fields[i].fieldname +
            '"'
        );
      }
    }

    let s = sfields.join(', ');
    return s;
  };

  this.buildfrom = function() {
    var from = '';
    for (var i = 0; i < this.tabs.length; i++) {
      var tableName = this.tabs[i].table;

      if (this.tabs[i].table.indexOf(' ') !== -1) {
        var splittedTableName = this.tabs[i].table.split('.');
        if (splittedTableName.length === 2) {
          var schema = splittedTableName[0];
          var table = splittedTableName[1];
          tableName = schema + '."' + table + '"';
        }
      }

      if (from == '') from = tableName + ' ' + this.tabs[i].alias;
      else
        from +=
          ' LEFT OUTER JOIN ' +
          tableName +
          ' ' +
          this.tabs[i].alias +
          ' ON ' +
          this.tabs[i].join_cond;
    }

    return from;
  };

  function removeHtmlSymbols(text) {
    return text
      .replace(/&amp;/g, '&')
      .replace(/&lt;/g, '<')
      .replace(/&gt;/g, '>')
      .replace(/&quot;/g, '')
      .replace(/&#039;/g, '')
      .replace(/'/g, '');
  }
  this.buildwhere = function(qparams, use_alias, updating) {
    if (this.tabs.length == 0) return 'false';

    let sWhere = '';
    var f_id = false;
    for (var i = 0; i < this.fields.length; i++) {
      let field_filter_value = _qv(qparams, 'f_' + this.fields[i].fieldname);
      if (field_filter_value == '' && ['point', 'line', 'polygon'].indexOf(this.fields[i].object.type) !== -1 &&
        settings.convertGeometries === false && qparams.fieldname && this.fields[i].fieldname === qparams.fieldname
      ) {
        if (sWhere != '') {
          sWhere += ' AND ';
        }

        sWhere += '(' + this.fields[i].where(use_alias, false) + ') ';
      } else if (field_filter_value == '') {
        continue;
      } else {
        if (this.fields[i].fieldname == 'id') f_id = true;
        field_filter_value = removeHtmlSymbols(field_filter_value);
        if (sWhere != '') sWhere += ' AND ';
        var gtempoarr = field_filter_value.split(';');
        if (gtempoarr.length > 1) sWhere += ' (';
        for (var j = 0; j < gtempoarr.length; j++) {
          gtempoarr[j] = gtempoarr[j].trim();
          if (gtempoarr[j] == '') continue;
          if (j != 0) {
            sWhere += ' OR ';
          }
          sWhere += '(' + this.fields[i].where(use_alias, gtempoarr[j]) + ')';
        }
        if (gtempoarr.length > 1) sWhere += ') ';
      }
    }

    if (
      updating &&
      (meta.rights.charAt(1) == '0' || meta.rights.charAt(1) == '1')
    ) {
      if (sWhere != '') sWhere = '(' + sWhere + ')' + ' AND ';
      if (use_alias) {
        if (
          user_id != undefined &&
          user_id != null &&
          user_id != '' &&
          meta.rights.charAt(1) == '1'
        )
          sWhere += this.tabs[0].alias + ".created_by='" + user_id + "'";
        else if (meta.rights.charAt(1) == '0')
          sWhere += ' ' + this.tabs[0].alias + '.published ';
        else sWhere += ' false ';
      } else {
        if (
          user_id != undefined &&
          user_id != null &&
          user_id != '' &&
          meta.rights.charAt(1) == '1'
        )
          sWhere += " created_by='" + user_id + "'";
        else if (meta.rights.charAt(1) == '0') sWhere += ' published ';
        else sWhere += ' false ';
      }
    }

    if (
      !updating &&
      (meta.rights.charAt(0) == '0' || meta.rights.charAt(0) == '1')
    ) {
      log('user_id=');
      log(user_id);
      log('meta.rights=');
      log(meta.rights);

      var subWhere = '';

      if (dataset_id == 100) {
        if (use_alias) {
          if (
            user_id != undefined &&
            user_id != null &&
            user_id != '' &&
            meta.rights.charAt(0) == '1'
          )
            subWhere +=
              ' (' +
              this.tabs[0].alias +
              ".created_by='" +
              user_id +
              "' OR " +
              this.tabs[0].alias +
              ".published OR SUBSTRING(public.dataset_access(main.id, '" +
              user_id +
              "'), 1,1)<>'0') ";
          else
            subWhere +=
              ' (' +
              this.tabs[0].alias +
              ".published OR SUBSTRING(public.dataset_access(main.id, '" +
              user_id +
              "'), 1,1)<>'0') ";
        } else {
          if (
            user_id != undefined &&
            user_id != null &&
            user_id != '' &&
            meta.rights.charAt(0) == '1'
          )
            subWhere +=
              " (created_by='" +
              user_id +
              "' OR published OR SUBSTRING(public.dataset_access(main.id, '" +
              user_id +
              "'), 1,1)<>'0') ";
          else
            subWhere +=
              " (published OR SUBSTRING(public.dataset_access(main.id, '" +
              user_id +
              "'), 1,1)<>'0') ";
        }
      } else {
        if (use_alias) {
          if (
            user_id != undefined &&
            user_id != null &&
            user_id != '' &&
            meta.rights.charAt(0) == '1'
          )
            subWhere +=
              this.tabs[0].alias +
              ".created_by='" +
              user_id +
              "' OR " +
              this.tabs[0].alias +
              '.published ';
          if (
            (user_id == undefined || user_id == null || user_id == '') &&
            meta.rights.charAt(0) == '1'
          )
            subWhere += this.tabs[0].alias + '.published ';
          else if (meta.rights.charAt(0) == '0')
            subWhere += ' ' + this.tabs[0].alias + '.published ';
        } else {
          if (
            user_id != undefined &&
            user_id != null &&
            user_id != '' &&
            meta.rights.charAt(0) == '1'
          )
            subWhere += " created_by='" + user_id + "' OR " + 'published ';
          if (
            (user_id == undefined || user_id == null || user_id == '') &&
            meta.rights.charAt(0) == '1'
          )
            subWhere += ' published ';
          else if (meta.rights.charAt(0) == '0') subWhere += ' published ';
        }
      }

      if (sWhere != '' && subWhere != '') sWhere += ' AND ';

      sWhere += subWhere;
    }

    if (sWhere != '') {
      if (use_alias) sWhere += ' AND NOT ' + this.tabs[0].alias + '.is_deleted';
      else sWhere += ' AND NOT is_deleted';
    } else {
      if (use_alias) sWhere += ' NOT ' + this.tabs[0].alias + '.is_deleted';
      else sWhere += ' NOT is_deleted';
    }

    return sWhere;
  };

  this.buildquery = function(include, qparams) {
    let groupby = [];
    let select_list = this.buildselect(include, qparams, groupby);
    let from_list = this.buildfrom();
    let where_list = this.buildwhere(qparams, true, false);

    var distinct = '';
    if (_qv(qparams, 'distinct') != '') distinct = ' distinct ';
    let result = 'SELECT ' + distinct + select_list + ' FROM ' + from_list;
    if (where_list != '') result += ' WHERE ' + where_list;
    if (groupby.length > 0) result += ' GROUP BY ' + groupby.join(', ');

    return result;
  };

  this.build_order = function(qparams) {
    let sOrder = '';
    if ('sort' in qparams && qparams.sort !== undefined) {
      var sort = qparams.sort;
      if (typeof sort === 'string') {
        log(sort);
        try {
          sort = JSON.parse(sort);
        } catch (ex) {
          return '';
        }
      }
      if (!Array.isArray(sort)) return '';

      log('sort.length', sort.length);
      for (var i = 0; i < sort.length; i++) {
        log('sort[i].fieldname', sort[i].fieldname);
        if (sort[i].fieldname === undefined || sort[i].fieldname == '')
          continue;

        var dir = '';
        if (sort[i].dir && sort[i].dir === 'ASC') dir = 'ASC';
        else dir = 'DESC';
        if (sOrder != '') sOrder += ', ';
        sOrder += '"' + sort[i].fieldname + '" ' + dir;
      }
      return sOrder;
    }

    for (var i = 0; i < this.fields.length; i++) {
      let col = _qv(qparams, 'iSortCol_' + i);
      let dir = _qv(qparams, 'sSortDir_' + i);
      if (col != '') {
        let fieldname = _qv(qparams, 'mDataProp_' + col);
        if (fieldname == '') continue;
        if (sOrder != '') sOrder += ', ';
        (sOrder += '"' + this.fields[i]), fieldname + '" ' + dir;
      }
    }
    return sOrder;
  };

  this.build_document_tree = function(includefields, document_template, row) {
    var result = {};
    result.id = '';
    for (var fieldname in document_template) {
      if (
        includefields != undefined &&
        includefields.length > 0 &&
        includefields.indexOf(fieldname) == -1
      )
        continue;

      if (document_template[fieldname].displayfields) {
        let res = { id: row[fieldname] };
        for (
          var i = 0;
          i < document_template[fieldname].displayfields.length;
          i++
        ) {
          let nmfld = document_template[fieldname].displayfields[i];
          if ('ref_' + nmfld in row) {
            res[nmfld] = row['ref_' + nmfld];
            continue;
          }

          newld = document_template[fieldname].join_tab_alias + '_' + nmfld;
          if (newld in row) {
            res[nmfld] = row[newld];
            continue;
          }
        }

        result[fieldname] = res;
      } else {
        result[fieldname] = row[fieldname];
      }

      if ('calc_' + fieldname in row) {
        result['calc_' + fieldname] = row['calc_' + fieldname];
      }

      if (fieldname == 'JSON' && dataset_id == 100) {
        let JSON_value = JSON.parse(row[fieldname]);
        JSON_value.dataset_id = row['dataset_id'];
        JSON_value.description = row['dataset_description'];
        JSON_value.rights = row['a_access'];
        result[fieldname] = JSON.stringify(JSON_value);
      }
    }
    return result;
  };

  this.documentquery = (includefields, qparams, sLimit, callback) => {
    var g_table = this;
    var sSQL = this.buildquery(includefields, qparams);
    var distinct = '';
    if (_qv(qparams, 'distinct') != '') distinct = ' distinct ';

    var order_list = this.build_order(qparams);
    if (
      order_list == '' &&
      this.tabs.length > 0 &&
      distinct == '' &&
      !this.grouping
    )
      order_list = this.tabs[0].alias + '.edited_on DESC';

    var cnt = 0;
    log(sSQL + ' ' + sLimit);
    if (callback) {
      function send_data() {
        log('run sql');
        if (order_list != '') sSQL += ' ORDER BY ' + order_list;
        log(sSQL + ' ' + sLimit);
        var subquery = geo_meta_table.client.query(
          sSQL + ' ' + sLimit,
          function(error, query_result, unofields2) {
            log('run sql end');
            if (error === null) {
              let iTotal = cnt;
              let iFilteredTotal = cnt;
              var aaData = new Array();
              for (var r = 0; r < query_result.rows.length; r++) {
                aaData.push(
                  g_table.build_document_tree(
                    includefields,
                    g_table.document_template,
                    query_result.rows[r]
                  )
                );
              }

              if (g_table.details.length == 0) {
                let output = {
                  sEcho: parseInt(_qv(qparams, 'sEcho')),
                  iTotalRecords: '' + iTotal,
                  iTotalDisplayRecords: '' + iFilteredTotal,
                  aaData: aaData
                };

                callback(null, output);
                return;
              }

              function loaddetails(row_index, col_index) {
                log('row_index=' + row_index + ', col_index=' + col_index);
                if (row_index == aaData.length) {
                  let output = {
                    sEcho: parseInt(_qv(qparams, 'sEcho')),
                    iTotalRecords: '' + iTotal,
                    iTotalDisplayRecords: '' + iFilteredTotal,
                    aaData: aaData
                  };

                  callback(null, output);
                  return;
                }

                if (col_index == g_table.details.length) {
                  row_index++;
                  loaddetails(row_index, 0);
                  return;
                }

                var c_obj = aaData[row_index];
                var det_table = g_table.details[col_index].tab;
                var det_meta = g_table.details[col_index].ref_meta;
                var col_meta = g_table.details[col_index].col_meta;
                let det_qparams = {};
                det_qparams['f_' + col_meta.widget.properties.ref_detail] =
                  c_obj.id;
                det_table.documentquery('', det_qparams, '', function(
                  error,
                  outdata
                ) {
                  c_obj[col_meta.fieldname] = outdata.aaData;
                  col_index++;
                  loaddetails(row_index, col_index);
                });
              }

              loaddetails(0, 0);
            } else {
              log(sSQL);
              callback(error, null);
            }
          }
        );
      }

      if (_qv(qparams, 'count_rows') != '') {
        let countsql = 'select count(1) as cnt from ( ' + sSQL + ') s';
        log('run count sql');
        log(countsql);
        var query = geo_meta_table.client.query(countsql, function(
          error,
          cntresult,
          unofields
        ) {
          log('run count sql end');
          log(error);
          if (error === null) {
            cnt = cntresult.rows[0].cnt;
            send_data();
          } else {
            log(countsql);
            //log(error);
            callback(error, null);
          }
        });
      } else {
        log('i am here!');
        send_data();
      }
    }
    return sSQL;
  };

  this.insert = function(document, successCallback, errorCallback) {
    var g_table = this;
    if (meta.rights.charAt(1) == '0') {
      errorCallback('Access denied');
      return;
    }

    var flds = [];
    var vls = [];
    for (var i = 0; i < this.fields.length; i++) {
      if (this.fields[i].fieldname == 'id') continue;
      if (
        this.fields[i].object.tablename === undefined ||
        this.fields[i].object.tablename != 'main'
      )
        continue;

      let sql__value = '';
      let field_value = _qv(document, this.fields[i].fieldname);
      if (field_value != 'NULL' && this.fields[i].fieldname in document) {
        sql__value = this.fields[i].sql_value(
          document[this.fields[i].fieldname]
        );
      } else if (field_value == 'NULL') {
        sql__value = 'NULL';
      } else {
        continue;
      }

      flds.push('"' + this.fields[i].fieldname + '"');
      vls.push(sql__value);
    }

    if (flds.length > 0) {
      let fields = flds.join(', ');
      let values = vls.join(', ');
      var insert_query =
        'INSERT INTO ' +
        this.tabs[0].table +
        '(' +
        fields +
        ', is_deleted, created_on, created_by, edited_on ) VALUES(' +
        values +
        ", false, current_timestamp, '" +
        user_id +
        "', current_timestamp) RETURNING id";

      log(insert_query);
      var query = geo_meta_table.client.query(insert_query, function(
        error,
        query_result,
        unofields2
      ) {
        if (error === null) {
          var new_id = query_result.rows[0].id;
          for (var i = 0; i < g_table.details.length; i++) {
            var det_table = g_table.details[i].tab;
            var col_meta = g_table.details[i].col_meta;
            var fld_values = document[col_meta.fieldname];
            if (Array.isArray(fld_values)) {
              for (var i = 0; i < fld_values.length; i++) {
                sub_doc = fld_values[i];
                sub_doc[col_meta.widget.properties.ref_detail] = new_id;
                det_table.insert(sub_doc, function(res) {
                  /*	if(dataset_id==3){
										g_table.update_symbols();
									}*/

                  log(res);
                });
              }
            }
          }

          successCallback({ id: new_id });
        } else {
          log(insert_query);
          log(error);
          errorCallback(error);
        }
      });
    } else {
      errorCallback('Missing data to insert!');
      return;
    }
  };

  this.update = function(document, successCallback, errorCallback) {
    var g_table = this;
    let flds = [];
    let vls = [];
    var where = '';
    if (meta.rights.charAt(1) == '0') {
      errorCallback('Access denied');
      return;
    }

    let swhere = this.buildwhere(document, false, true);
    if (swhere == '') {
      errorCallback('Missing data identification!');
      return;
    }

    if (meta.rights.charAt(1) == '1') {
      swhere += " AND created_by='" + user_id + "'";
    }

    let id_filter_value = _qv(document, 'f_id');
    if (meta.rights.charAt(1) != '4' && id_filter_value == '') {
      log('group updating is disable');
      errorCallback('group updating is disable');
      return;
    }

    if (
      (id_filter_value == '' || isNaN(id_filter_value)) &&
      _qv(document, 'group_update') == ''
    ) {
      errorCallback('group update error!');
      return;
    }

    for (var i = 0; i < this.fields.length; i++) {
      if (
        this.fields[i].object.tablename === undefined ||
        this.fields[i].object.tablename != 'main'
      )
        continue;

      let field_value = _qv(document, this.fields[i].fieldname);
      if (field_value == '') continue;

      let sql__value = '';
      if (field_value != 'NULL' && this.fields[i].fieldname in document)
        sql__value = this.fields[i].sql_value(
          document[this.fields[i].fieldname]
        );
      else if (field_value == 'NULL') sql__value = 'NULL';
      else continue;

      flds.push('"' + this.fields[i].fieldname + '" = ' + sql__value);
      vls.push(sql__value);
      if (id_filter_value == '') {
        if (swhere != '') swhere += ' AND ';
        swhere += '"' + this.fields[i].fieldname + '" IS NULL ';
        if (this.fields[i] instanceof sql_string)
          swhere += ' or ' + '"' + this.fields[i].fieldname + "\" = ''";
      }
    }

    if (flds.length > 0) {
      flds.push('edited_on = current_timestamp');
      flds.push("edited_by = '" + user_id + "'");

      var tableName = this.tabs[0].table;
      if (tableName.indexOf(' ') !== -1) {
        var splittedTableName = tableName.split('.');
        if (splittedTableName.length === 2) {
          var schema = splittedTableName[0];
          var table = splittedTableName[1];
          tableName = schema + '."' + table + '"';
        }
      }

      let fields = flds.join(', ');
      var update_query =
        'UPDATE ' + tableName + ' SET ' + fields + ' WHERE ' + swhere;
      log(update_query);
      var query = geo_meta_table.client.query(update_query, function(
        error,
        query_result,
        unofields2
      ) {
        if (error === null) {
          /*if(dataset_id==3){
						g_table.update_symbols();
					}*/
          for (var i = 0; i < g_table.details.length; i++) {
            var det_table = g_table.details[i].tab;
            var col_meta = g_table.details[i].col_meta;
            var fld_values = document[col_meta.fieldname];
            if (Array.isArray(fld_values)) {
              for (var i = 0; i < fld_values.length; i++) {
                sub_doc = fld_values[i];
                id_filter_value = _qv(document, 'f_id');
                sub_doc[
                  col_meta.widget.properties.ref_detail
                ] = id_filter_value;
                if (sub_doc.id === undefined || sub_doc.id == 'NULL') {
                  det_table.insert(sub_doc, function(res) {
                    log('insert');
                    log(res);
                  });
                } else {
                  if (sub_doc.operation == 'delete') {
                    sub_doc['f_id'] = sub_doc.id;
                    det_table.delete(sub_doc, function(res) {
                      log('delete');
                      log(res);
                    });
                  } else {
                    sub_doc['f_id'] = sub_doc.id;
                    det_table.update(sub_doc, function(res) {
                      log('update');
                      log(res);
                    });
                  }
                }
              }
            }
          }

          successCallback();
        } else {
          log(update_query);
          errorCallback(error);
        }
      });
    } else {
      errorCallback('Missing data to update!');
    }
  };

  this.delete = function(document, successCallback, errorCallback) {
    var g_table = this;
    if (meta.rights.charAt(1) == '0') {
      errorCallback('Access denied');
      return;
    }

    let id_filter_value = _qv(document, 'f_id');
    if (id_filter_value != '') {
      let swhere = 'id=' + id_filter_value;
      if (meta.rights.charAt(1) == '1') {
        swhere += " AND created_by='" + user_id + "'";
      }

      var update_query =
        'UPDATE ' +
        this.tabs[0].table +
        ' SET is_deleted = true WHERE ' +
        swhere;
      log(update_query);
      var query = geo_meta_table.client.query(update_query, function(
        error,
        query_result,
        unofields2
      ) {
        if (error === null) {
          /*if(dataset_id==3){
						g_table.update_symbols();
					}*/
          for (var i = 0; i < g_table.details.length; i++) {
            var det_table = g_table.details[i].tab;
            var col_meta = g_table.details[i].col_meta;
            var ref_meta = g_table.details[i].ref_meta;
            var delete_query =
              'UPDATE ' +
              ref_meta.tablename +
              ' SET is_deleted = true WHERE ' +
              col_meta.widget.properties.ref_detail +
              ' = ' +
              id_filter_value;
            log(delete_query);
            geo_meta_table.client.query(delete_query, function(
              error,
              query_result,
              unofields2
            ) {
              if (error != null) log(error);
            });
          }

          successCallback();
        } else {
          log(update_query);
          errorCallback(error);
        }
      });
    } else {
      errorCallback('Missing data identification!');
    }
  };
}

export default geo_table;
