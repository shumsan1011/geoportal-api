var clipboard_buffer=''

function addzero(v, cnt){  
	v=v+'';
	while (cnt>v.length){
	  v='0'+v;
	}  
	return v;
}

function addsec(v){  
	v=v+'';
	ps=v.split('.');
	for(i=ps[0].length; i<2; i++){
		v='0'+v;
	}
	/*
	while (2>v.length){
	  v='0'+v;
	} */ 	
	while (5>v.length){
		if(v.length==2)
			v=v+'.';		
		else
			v=v+'0';
	}  
	return v;
}

function Dec2minute_sec(val) {
	if(val<0)
		val=-val;
	var lon_gr = addzero(Math.floor(val),3);
	var lon_mn = addzero(Math.floor((val-lon_gr)*60),2);	
	var lon_sec = addsec(Math.round( (((val-lon_gr)*60-lon_mn)*60)*100 )/100,5);
	
	var value =lon_gr+"°"+lon_mn+"'"+lon_sec+"''";
	return value;
};

function minute_sec2Dec(val){	
	if(val ==undefined || val=='')
		return 0;
	if('ENWS'.indexOf(val.charAt(0))==-1){		
		var tmpval = parseFloat(val);
		if(isNaN(tmpval)){
			val=val.replace(/,/g, '.');
			return val;	
		}
		val='N'+val;
	}	
	var sign=val.toUpperCase().charAt(0);
	sign=sign=='E' || sign=='N';
	val=val.substr(1);
	arr = val.split(/[\D]/);
	var sec=arr[2]+'.'+arr[3];
	var sec = sec/3600;
	var mn = arr[1]/60;
	var g = parseInt(arr[0],10)+mn+sec;	
	if(!sign)
		g=-g;	
	return g;
}

function wkt2decimal(val){
	var re=/MULTIPOINT\((-?[0-9]*\.?[0-9]*)\s(-?[0-9]*\.?[0-9]*)\)/
	var arr = re.exec(val);			
	if (arr==null || arr.length<3)
		return {lat:0, lon:0};
	return {lon:arr[1], lat:arr[2]};
}

function wkt2minute_sec(val){
	var re=/MULTIPOINT\((-?[0-9]*\.?[0-9]*)\s(-?[0-9]*\.?[0-9]*)\)/
	var arr = re.exec(val);			
	if (arr==null || arr.length<3)
		return {lat:0, lon:0};
	lat=Dec2minute_sec(arr[2]);
	lon=Dec2minute_sec(arr[1]);
	
	if(arr[2]<0)
		lat='S'+lat;
	else
		lat='N'+lat;
	if(arr[1]<0)
		lon='W'+lon;
	else
		lon='E'+lon;	
	return {lat:lat, lon:lon};
}



function ToDec(string) {
	in_arr  =  string.split(' ');
	
	var lon_vl = in_arr[1];	
	var lon_sign=lon_vl.toUpperCase().charAt(0);
	if(lon_sign=='E' || lon_sign=='W'){		
		lon_vl=lon_vl.substr(1);
	}
	else
		lon_sign='E';
	
	var lat_vl = in_arr[0];
	var lat_sign=lat_vl.toUpperCase().charAt(0);
	if(lat_sign=='N' || lat_sign=='S'){		
		lat_vl=lat_vl.substr(1);
	}
	else
		lat_sign='N';
		
	arr = lon_vl.split(/[\D]/);
	var sec=arr[2]+'.'+arr[3];
	var lon_sec = sec/3600;
	var lon_mn = arr[1]/60;
	var lon = parseInt(arr[0],10)+lon_mn+lon_sec;	
	if(lon_sign!='E')
		lon=-lon;
	
	arr = lat_vl.split(/[\D]/);
	var sec=arr[2]+'.'+arr[3];
	var lan_sec = sec/3600;
	var lan_mn = arr[1]/60;
	var lat = parseInt(arr[0],10)+lan_mn+lan_sec;
	if(lat_sign!='N')
		lat=-lat;
	
					
	var value = lon + " " + lat;
	return value;
};



function FromDec(string) {
	if(string=='')
		arr={0:'0',1:'0'};	
	else
		arr  =  string.split(' ');
	
	lan_sign='N';
	if(arr[1]+0<0)
		lan_sign='S';

	lon_sign='E';
	if(arr[0]+0<0)
		lon_sign='W';
		
	var lon_gr = addzero(Math.floor(arr[0]),3);
	var lon_mn = addzero(Math.floor((arr[0]-lon_gr)*60),2);	
	var lon_sec = addsec(Math.round( (((arr[0]-lon_gr)*60-lon_mn)*60)*100 )/100,5);
		 
	var lan_gr = addzero(Math.floor(arr[1]),3);
	var lan_mn = addzero(Math.floor((arr[1]-lan_gr)*60),2);
	var lan_sec = addsec(Math.round( (((arr[1]-lan_gr)*60-lan_mn)*60)*100)/100,5);
	
	var value =lan_sign+lan_gr+"°"+lan_mn+"'"+lan_sec+"''"+" "+lon_sign+lon_gr+"°"+lon_mn+"'"+lon_sec+"''";
	return value;
};




exports = module.exports = {
  parseCoordinate: minute_sec2Dec
}  

