import validator from 'validator';
import sanitizer from 'sanitizer';


/**
 * The API should return every error message with corresponding HTTP status code,
 * error identifier (so, the client can specify custom error messages according
 * to predefined set of possible error codes, for example, for localization purposes)
 * and human-readable message in English (so, the client can simple show the error
 * message to user).
 * 
 * This is the highest level of error reporting which actually goes to the client, so
 * the database of file access error are returned also in terms of API errors
 */
const APIErrors = {
  USER_WITH_THIS_EMAIL_NOT_FOUND: {
    id: 1000,
    message: 'User with this email was not found'
  },
  INVALID_ACTIVATION_CODE: {
    id: 1001,
    message: 'Invalid activation code was provided'
  },
  COLUMN_VALUE_ALREADY_EXISTS: {
    id: 1002,
    message: 'Specified column value already exists'
  },
  FORBIDDEN: {
    id: 1003,
    message: 'Forbidden'
  },
};

/*
*	### Server and Client shared script
*/

/**
*	getQueryVariable
*/
function getQueryVariable(variable) {
  var query = window.location.search.substring(1);
  var vars = query.split('&');
  for (var i = 0; i < vars.length; i++) {
      var pair = vars[i].split('=');
      if (decodeURIComponent(pair[0]) == variable) {
          return decodeURIComponent(pair[1]);
      }
  }
return null;
}

/**
*	Pausing browser
*/
function pausecomp(millis) {
  var date = new Date();
  var curDate = null;

  do { curDate = new Date(); } 
    while(curDate-date < millis);
} 

function changeVectorProjectionLine(initepsg, desiredepsg, feature) {
  initepsg = new OpenLayers.Projection(initepsg);
  desiredepsg = new OpenLayers.Projection(desiredepsg);
  var totalvectors = feature.geometry.components.length;
  for (var j = 0; j < totalvectors; j++) {
    feature.geometry.components[j] = feature.geometry.components[j].transform(initepsg, desiredepsg);
  }
  return feature;
}

function changeVectorProjection(initepsg, desiredepsg, feature) {
  initepsg = new OpenLayers.Projection(initepsg);
  desiredepsg = new OpenLayers.Projection(desiredepsg);
  var totalvectors = feature.geometry.components.length;
  var actualvectors = feature.geometry.components;
  for (var i = 0; i < totalvectors; i++) {
    var totalpoints = actualvectors[i].components.length;
    var actualpoints = actualvectors[i].components;
    for (var j = 0; j < totalpoints - 1; j++) {
      feature.geometry.components[i].components[j] = feature.geometry.components[i].components[j].transform(initepsg, desiredepsg);
    }
  }
  return feature;
}

function hexToRGB(hex) {
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ? {
      r: parseInt(result[1], 16),
      g: parseInt(result[2], 16),
      b: parseInt(result[3], 16)
  } : null;
}

function clone(obj) {
  if (null == obj || "object" != typeof obj) return obj;
  var copy = obj.constructor();
  for (var attr in obj) {
      if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
  }
  return copy;
}

function functionApplyTags(funcbody) {
  return str_replace('\'', 'XXQUOTEXX', str_replace('"', '\'', str_replace('\n', 'XXNEWLINEXX', funcbody)));
}

function functionReverseTags(funcbody, foreval) {
  if (foreval) return str_replace('XXQUOTEXX', '\'', str_replace('XXNEWLINEXX', '\n', funcbody));
  return str_replace('XXQUOTEXX', '\'', str_replace('XXNEWLINEXX', '\n', funcbody));
}

function functionGetName(funcbody) {
  var body = functionReverseTags(funcbody);
  var beg = body.indexOf(' ');
  var end = body.indexOf('(');
  return body.substr(beg, (end - beg)).trim();
}


/**
*	JS implementation of PHP str_replace function
*/
var str_replace = function (search, replace, subject, count) {
var i = 0,
  j = 0,
  temp = '',
  repl = '',
  sl = 0,
  fl = 0,
  f = [].concat(search),
  r = [].concat(replace),
  s = subject,
  ra = Object.prototype.toString.call(r) === '[object Array]',
  sa = Object.prototype.toString.call(s) === '[object Array]';
s = [].concat(s);
if (count) {
  this.window[count] = 0;
}

for (i = 0, sl = s.length; i < sl; i++) {
  if (s[i] === '') {
    continue;
  }
  for (j = 0, fl = f.length; j < fl; j++) {
    temp = s[i] + '';
    repl = ra ? (r[j] !== undefined ? r[j] : '') : r[0];
    s[i] = (temp).split(f[j]).join(repl);
    if (count && s[i] !== temp) {
      this.window[count] += (temp.length - s[i].length) / f[j].length;
    }
  }
}
return sa ? s : s[0];
}

/**
* tranlsit - trnforming cyrillic to latin string
*/
var translit = function (str) {
if (str === undefined) {
  str = 'notdef';
};
if (str == undefined) {
  str = 'notdef';
};
var transl = new Array();
  transl['А']='A';     transl['а']='a';
  transl['Б']='B';     transl['б']='b';
  transl['В']='V';     transl['в']='v';
  transl['Г']='G';     transl['г']='g';
  transl['Д']='D';     transl['д']='d';
  transl['Е']='E';     transl['е']='e';
  transl['Ё']='Yo';    transl['ё']='yo';
  transl['Ж']='Zh';    transl['ж']='zh';
  transl['З']='Z';     transl['з']='z';
  transl['И']='I';     transl['и']='i';
  transl['Й']='J';     transl['й']='j';
  transl['К']='K';     transl['к']='k';
  transl['Л']='L';     transl['л']='l';
  transl['М']='M';     transl['м']='m';
  transl['Н']='N';     transl['н']='n';
  transl['О']='O';     transl['о']='o';
  transl['П']='P';     transl['п']='p';
  transl['Р']='R';     transl['р']='r';
  transl['С']='S';     transl['с']='s';
  transl['Т']='T';     transl['т']='t';
  transl['У']='U';     transl['у']='u';
  transl['Ф']='F';     transl['ф']='f';
  transl['Х']='X';     transl['х']='x';
  transl['Ц']='C';     transl['ц']='c';
  transl['Ч']='Ch';    transl['ч']='ch';
  transl['Ш']='Sh';    transl['ш']='sh';
  transl['Щ']='Shh';    transl['щ']='shh';
  transl['Ъ']='';     transl['ъ']='';
  transl['Ы']='Y';    transl['ы']='y';
  transl['Ь']='';    transl['ь']='';
  transl['Э']='E';    transl['э']='e';
  transl['Ю']='Yu';    transl['ю']='yu';
  transl['Я']='Ya';    transl['я']='ya';
transl['_']='_';  transl[' ']='_';

  var result='';
  for(let i = 0; i < str.length; i++) {
    if (transl[str[i]] != undefined) {
      result+=transl[str[i]]; 
    } else {
      result+=str[i];
    }
  }

  return result;
}

/**
* trim - getting rid of spaces
*/
var trim = function(str) {
  return str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
}

/**
* randomString - generating random string
*/
var randomString = function (length, numerical) {
  if (numerical)
  {
    var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz'.split('');
  }
  else
  {
    var chars = '1234567980'.split('');
  }

  if (! length) { length = Math.floor(Math.random() * chars.length); }
  var str = '';
  for (var i = 0; i < length; i++) { str += chars[Math.floor(Math.random() * chars.length)];}
  return str;
}

/**
*	JS implementation of PHP isset() function
*
*	@param obj - input object we are trying to examine
*	@return bool
*/
var isset = function (obj) {
  if (typeof obj != 'undefined') return true;
  else return false;
}


/**
*	It is an util function for obtaining correct object name
*
*	@param name - input name to make correct
*	@return correct name
*/
var getcorrectobjname = function (name) {
  name=name.toLowerCase();
  var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz';
  var digits='0123456789';
  var res=''
  name=translit(name);
  for(var i=0; i<name.length; i++){
    if (res.length==0 && digits.indexOf(name.charAt(i))!=-1 ) continue;
    if(chars.indexOf(name.charAt(i))!=-1)
      res=res+name.charAt(i);
    if (res.length>15) break;
  }
  return res;	
}


/**
*	JS implementation of PHP implode() function
*/
var implode = function (glue, pieces) {
  // http://kevin.vanzonneveld.net
  // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // +   improved by: Waldo Malqui Silva
  // +   improved by: Itsacon (http://www.itsacon.net/)
  // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
  // *     example 1: implode(' ', ['Kevin', 'van', 'Zonneveld']);
  // *     returns 1: 'Kevin van Zonneveld'
  // *     example 2: implode(' ', {first:'Kevin', last: 'van Zonneveld'});
  // *     returns 2: 'Kevin van Zonneveld'
  var i = '',
    retVal = '',
    tGlue = '';
  if (arguments.length === 1) {
    pieces = glue;
    glue = '';
  }
  if (typeof(pieces) === 'object') {
    if (Object.prototype.toString.call(pieces) === '[object Array]') {
      return pieces.join(glue);
    }
    for (i in pieces) {
      retVal += tGlue + pieces[i];
      tGlue = glue;
    }
    return retVal;
  }
  return pieces;
}


/**
*	JS implementation of PHP substr_replace function
*/
var substr_replace = function (str, replace, start, length) {
  if (start < 0) { // start position in str
    start = start + str.length;
  }
  length = length !== undefined ? length : str.length;
  if (length < 0) {
    length = length + str.length - start;
  }
  return str.slice(0, start) + replace.substr(0, length) + replace.slice(length) + str.slice(start + length);
}


var is_number = function (str) {
  return !isNaN(str);
}

// ---------------------- Only Client Scripts -----------------------

/**
* Synchronous AJAX requests
*/
var getRemote = function (url) {
  return $.ajax({
      type: "GET",
      url: url,
      async: false,
  }).responseText;
}

var postRemote = function (url, data) {
  return $.ajax({
      type: "POST",
  data: data,
      url: url,
      async: false,
  }).responseText;
}


var getWidgetsHTML = function () {
var output = '<option value="null" selected>Выберите виджет</option>';
for (var k = 0; k < $.widgets.widgetlist.length; k++) { output += '<option value="' +  $.widgets.widgetlist[k][0] + '">' + $.widgets.widgetlist[k][0] + '</option>';}
return output;
}


function cleanNewLines(text)
{
text = text.replace(/(\r\n|\n|\r)/gm,"");
return text;
}

function getUrlVars() {
  var i,
      tmp     = [],
      tmp2    = [],
      objRes   = {},
  strQuery = window.location.search;
  if (strQuery != '') {
      tmp = (strQuery.substr(1)).split('&');
      for (i = 0; i < tmp.length; i += 1) {
          tmp2 = tmp[i].split('=');
          if (tmp2[0]) {
              objRes[tmp2[0]] = tmp2[1];
          }
      }
  }
  return objRes;
}

function _qv(query, valuename){
	if (query===undefined)
		return '';
	if(valuename in query) {
		return validator.escape('' + query[valuename]);
	} else {
		return '';
	}
} //end _qv()

const log = (...messages) => {
  if (['development', 'test'].indexOf(process.env.NODE_ENV) !== -1) {
    messages.forEach(message => {
      console.log(message);
    });
  }
}

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

/**
* Returns JSON for specific column
*/
function getcolumn(column, obj) {
	for (var i = 0; i < obj.columns.length; i++) {
		if (column == obj.columns[i].fieldname) {
			return obj.columns[i];
		}
  }
  
	log('Column "' + column + '" was not found in metadata object');
	return null;
}

function explode(separator, data) {
    var arr = data.split(separator);
    return arr;
}

/**
 *  Shortcut for detecting if variable is undefined.
 *
 * @param mixed value Analyzed variable
 *
 * @return boolean
 */
function isundefined(value) {
  return typeof value === 'undefined';
} //end isundefined()


/**
*  Shortcut for detecting if variable is JavaScript Object.
*
* @param mixed value Analyzed variable
*
* @return boolean
*/
function isobject(value) {
  return (value !== null && typeof value === 'object');
} //end isobject()


/**
*  Shortcut for detecting if variable is boolean.
*
* @param mixed value Analyzed variable
*
* @return boolean
*/
function isboolean(value) {
  return typeof value === 'boolean';
} //end isboolean()


/**
*  Shortcut for detecting if variable is integer.
*
* @param mixed value Analyzed variable
*
* @return boolean
*/
function isinteger(value) {
  return (value === parseInt(value));
} //end isinteger()


/**
* Checks if object has certain properties.
*
* @param object item       Analyzed object
* @param array  properties Checked properties
*
* @return boolean
*/
function hasproperties(item, properties) {
  var numberofproperties = properties.length;
  for (var i = 0; i < numberofproperties; i++) {
    if (item.hasOwnProperty(properties[i]) === false) {
      return false;
    }
  } //end for
}

const prepareParams = (req) => {
  let dataset_id = req.query.f;
  let user_id = '';
  let user_name = '';
  if (req.user) {
    user_id = req.user.id;
    user_name = req.user.name;
  }

  let qparams = req.query;
	for (var attrname in req.body) {
    qparams[attrname] = req.body[attrname];
  }

  return { dataset_id, user_id, user_name, qparams };
} //end prepareParams()

function XSSRecursiveCheck(obj) {
	for (var k in obj) {
		if (typeof obj[k] === "object")
			XSSRecursiveCheck(obj[k])
		else
			obj[k]= sanitizer.sanitize(obj[k]);
  }

	return obj;
}


export default {
  XSSRecursiveCheck,
  prepareParams,
  hasproperties,
  isinteger,
  isboolean,
  isobject,
  isundefined,
  isNumeric,
  log,
  _qv,
  translit,
  isset,
  explode,
  implode,
  substr_replace,
  str_replace,
  trim,
  is_number,
  getcorrectobjname,
  functionReverseTags,
  functionApplyTags,
  clone,
  hexToRGB,
  functionGetName,
  cleanNewLines,
  getcolumn,
  APIErrors
};