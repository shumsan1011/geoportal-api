import path from 'path';
import fs from 'fs';
import http from 'http';
import config from '../../../config/config';
import { str_replace, randomString, explode } from '../helpers';
import db from '../db'

const DIR_SEPARATOR = '/';

const client = db.getClient();

/**
 * Returns the extension for the provided file.
 *
 * @param string filename  File name
 * @param string extension Desired extension
 *
 * @return string
 */
function changeExtension(filename, extension) {
	var oldextension         = getExtension(filename);
	var filewithnewextension = filename.replace("." + oldextension, "." + extension);
	return filewithnewextension;
} //end changeExtension()


/**
 * Returns the extension for the provided file.
 *
 * @param string filename File name
 *
 * @return string
 */
function getExtension(filename) {
	var lastdot   = filename.lastIndexOf(".");
	var extension = filename.substring((lastdot + 1), filename.length);
	return extension;
} //end getExtension()


/**
 *	Getting file according to the filelink
 *
 *	@param string filepath Absolute path of the file
 *
 *	@return string URL link to the file
 */

function getFileFileLink(req, res, template, block, next) {
	var uniqueval = req.url.split("/")[2];

	var query  = "SELECT filepath, times_accessed FROM public.filelink_data WHERE uniqueval = '" + uniqueval + "';";
	var cquery = client.query(query, function(error, rows, fields) {
		if (error === null) {
			if (rows.rows.length > 0) {
				var filepath = rows.rows[0].filepath;
				var ta       = parseInt(rows.rows[0].times_accessed) + 1;
				var maxta    = parseInt(calipso.config.getModuleConfig('filelink', 'maxfetchqueries'));

				var queriedextension = getExtension(req.url);
				var correctfilepath  = changeExtension(filepath, queriedextension);

				if ((maxta !== 0) && (ta > maxta)) {
					res.format = "json";
					res.end(JSON.stringify({status: 'error', error: 'The link exceeded maximum number of fetch queries'}));
					return;
				}

				if (correctfilepath !== undefined) {
					fs.exists(correctfilepath, function(exists) {
						if (exists) {
							var filestatistics  = fs.statSync(correctfilepath)
							var fileSizeInBytes = filestatistics["size"];
							if (fileSizeInBytes > 0) {
								var readstream = fs.createReadStream(correctfilepath);

								res.writeHead(200);
								readstream.setEncoding("binary");

								readstream.on("data", function (chunk) {
									res.write(chunk, "binary");
								});

								readstream.on("end", function () {
								   res.end();
								   return;
								});

								readstream.on('error', function(err) {
									res.write(err + "\n");
									res.end();
									return;
								});
							}
						} else {
							res.writeHead(404);
							res.write("No data\n");
							res.end();
							return;
						}
					});
				} else {
					res.send({status: 'error', error: 'No such file'});
				}
			} else {
				res.send({status: 'error', error: 'Unable to find specified file, check the link validity or access rights'});
			}
		} else {
			res.send({status: 'error', error: error});
		}
	});
 };


/**
 *	Getting link for specified file and managing the database record
 *
 *	@param string filepath Absolute path of the file
 *
 *	@return string URL link to the file
 */

function getLinkForFile(absfilepath, id) {
	var uniqueval   = randomString(10, true) + id;
	var filenamearr = explode('/', absfilepath);
	var filename    = filenamearr[filenamearr.length - 1];

	var serverUrl = config.server.url;
	if (serverUrl.indexOf(":") === -1) {
		serverUrl += ":" + calipso.config.get('server:port');
	}

	var link   = serverUrl + "/getfile/" + uniqueval + DIR_SEPARATOR + filename;
	var access = '{allow: "*"}';

	var query = "INSERT INTO public.filelink_data (filepath, link, access, times_accessed, method_example, created, uniqueval) VALUES ('" + absfilepath + "', '" + link + "', '" + access + "', '0', NULL, NOW(), '" + uniqueval + "');";
	var cquery = client.query(query, function(error, rows, fields) {
		client.end();
		if (error !== null) {
			log("Error saving generated link in the database");
			log(error);
		}
	});
	return link;
}


/**
 *	Fetching file from the remote server and saving at certain location
 *
 *	@param string filelink URL of the file
 *	@param string filepath Path of the fetched file on local storage
 *
 *	@return bool Shows if the file was fetched and saved properly
 */
 
function fetchFileForLink(filelink, filepath) {
	var file = fs.createWriteStream(filepath);
	var request = http.get(filelink, function(response) {
		response.pipe(file);
		return true;
	}).on('error', function(e) {
		log("Unable to fetch " + filelink + " as " + filepath + ", error:");
		log(e);
	});
	return false;
}


/**
 * Returns root path
 *
 *	@return string
 */
function getRootPath() {
	return config.modules.rfm.mainrootdir;
}


/**
 *	Getting absolute physical path to the file
 *
 *	@param string filepath User path of the file
 *	@param string username Name of authenticated user
 *
 *	@return string Absolute path to the file
 */
function getAbsolutePath(filepath, username) {
  var prefix = config.modules.rfm.mainrootdir;
  if (prefix.substr(prefix.length - 1) !== "/") {
    prefix += "/";
  }

	filepath = prefix.replace(/\\/g, "/") + filepath;
	return filepath;
}


/**
 *	Getting user-friendly filepath from absolute one
 *
 *	@param string absfilepath Absolute path of the file
 *	@param string username    Name of authenticated user
 *
 *	@return string Absolute path to the file
 */
function getUserFilePath(absfilepath, username) {
	var prefix   = config.modules.rfm.mainrootdir;
	var toremove = prefix;
	filepath     = str_replace(toremove, "", absfilepath);
	return filepath;
}


/**
 * Generate random file according to user settings.
 *
 * @return string
 */
function getRandomFileName(extension) {
	var prefix = config.modules.filelink.temporaryfilesfolder;
	if (!prefix) prefix = "S:/temp/shared";

	var filename = prefix + DIR_SEPARATOR + randomString(16, true) + extension;
	return filename;
} //end getRandomFileName()


export default {
  getAbsolutePath,
  getLinkForFile,
  fetchFileForLink,
  getUserFilePath,
  getRootPath,
  getRandomFileName
}