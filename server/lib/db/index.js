const { Pool, Client } = require('pg');
import { log } from '../helpers';

let client = false;

export default {
  getClient: () => {
    if (client) {
      log('Returning cached connection');
      return client;
    } else {
      log('Returning new connection');

      const localClient = new Client({
        user: process.env.DB_USER,
        host: process.env.DB_HOST,
        database: process.env.DB_NAME,
        password: process.env.DB_PASSWORD,
        port: 5432
      });

      client = localClient;
      return client;
    }
  }
}


