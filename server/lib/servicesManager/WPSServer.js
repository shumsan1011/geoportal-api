import fs from 'fs';
import WPSManager from './WPSManager';
import ejs from 'ejs';
import { log } from '../helpers';
import config from '../../../config/config';


/**
 * Class for managing WPS services that present in the system.
 */

function WPSServer(dbclient, userId, userName) {
	var _databaseclient = dbclient;
	var _res            = false;
	var _req            = false;
  var _request        = false;
  var _userId         = userId;
  var _userName       = userName;

	/**
	 * Serving any request that comes to WPS server.
	 *
	 * @param string rawrequest Coming request
	 *
	 * @return string XML document
	 */

	this.serveRequest = function(rawrequest, res, req) {
		_res = res;
		_req = req;

		var request = _requestToArray(rawrequest);
		if (request === false || request.service !== "WPS") {
			throw "Invalid request was provided: " + request;
		}

		if (request.request === "GetCapabilities") {
			_getServices(false, _getCapabilities);
		} else if (request.request === "DescribeProcess") {
			if ("version" in request === false) throw "No version parameter was provided in the request";

			_getServices(request.identifier, _desribeProcess);
		} else if (request.request === "Execute") {
			if ("DataInputs" in request === false) throw "No DataInputs parameter was provided in the request";
			if ("version" in request === false) throw "No version parameter was provided in the request";

			_request = request;
			_getServices(request.identifier, _execute);
		} else {
			throw "Unable to process request: " + request.request;
		}
	} //end serveRequest()


	/**
	 * Queries services from the database.
	 *
	 * @param string   name     Specific service name (identifier)
	 * @param Function callback Has to be fired if service data was successfully fetched
	 *
	 * @return void
	 */

	var _getServices = function(name, callback) {
		var availalbleservcies = new Array();

		if (name === false) {
			var query = "SELECT * FROM wps_methods;";
		} else {
			var query = "SELECT * FROM wps_methods WHERE name = '" + name + "';";
		}

		_databaseclient.query(query, function(error, rows, fields) {
			if (error === null) {
				callback(rows);
			} else {
				log(error);
				throw "Error querying the database";
			}
		});
	} //end _getServices()


	/**
	 * Executes specific service.
	 *
	 * @param Object services Services list
	 *
	 * @return string XML document
	 */

	var _execute = function(services) {
		var service = services.rows[0];

		var successCallback = function(data) {
			var id = data;

			function checkIfOutputIsAvailable() {
				_databaseclient.query("SELECT result FROM method_examples WHERE id = " + id, function(error, rows, fields) {
          var output = rows.rows[0].result;
          if (output && output.length > 0) {
            var o = JSON.parse(output);
            var serviceinfo         = new Object();
            serviceinfo.identifier  = services.rows[0].name;
            serviceinfo.title       = services.rows[0].name;
            serviceinfo.description = services.rows[0].description;

            var outputrenderingdata = new Array();

            var outputparams = JSON.parse(service.output_params);
            for (let paramkey in outputparams) {
              if (outputparams[paramkey].fieldname in o) {
                outputrenderingdata.push({
                  identifier: outputparams[paramkey].fieldname,
                  title: outputparams[paramkey].fieldname,
                  description: outputparams[paramkey].description,
                  value: o[outputparams[paramkey].fieldname]
                });
              }
            }

            var renderedresponse = ejs.render(fs.readFileSync(__dirname + "/wpsdocuments/execute.ejs", "utf8"), {service: serviceinfo, output: outputrenderingdata});
            _res.header('Content-type', 'text/xml');
            _res.end(renderedresponse);
					} else {
						setTimeout(checkIfOutputIsAvailable, 1000);
					}
				});
			} //end checkIfOutputIsAvailable()

			checkIfOutputIsAvailable();
		}

		var failureCallback = function(data) {
			throw "Failed to launch the service";
		}

		var manager = new WPSManager(_req, _databaseclient);
		manager.executeService(successCallback, failureCallback, service.id, _userId, _userName, _request.DataInputs, {}, config.modules.servicesManager);
	} //end _execute()


	/**
	 * Reports the current server state.
	 *
	 * @param Object services Services list
	 *
	 * @return string XML document
	 */

	var _getCapabilities = function(services) {
		var renderingdata = new Array();
		for (let rowid in services.rows) {
      var servicedata = services.rows[rowid];
			if (servicedata.type === "js") {
				renderingdata.push({
					identifier: servicedata.name,
					title: servicedata.name,
					description: servicedata.description
				});
			}
		}

		var renderedresponse = ejs.render(fs.readFileSync(__dirname + "/wpsdocuments/getcapabilities.ejs", "utf8"), {services: renderingdata});
		_res.header('Content-type', 'text/xml');
		_res.end(renderedresponse);
	} //end _getCapabilities()


	/**
	 * Reports information about the specific service.
	 *
	 * @param Object services Services list
	 *
	 * @return string XML document
	 */

	var _desribeProcess = function(services) {
		if (services.rows.length === 1) {
			var service         = new Object();
			service.identifier  = services.rows[0].name;
			service.title       = services.rows[0].name;
			service.description = services.rows[0].description;

			var serviceinputparams  = JSON.parse(services.rows[0].params);
			var serviceoutputparams = JSON.parse(services.rows[0].output_params);
		} else {
			throw "Unable to find specified service";
		}

		var renderedresponse = ejs.render(fs.readFileSync(__dirname + "\\wpsdocuments\\describeprocess.ejs", "utf8"), {
			service: service,
			datainputs: serviceinputparams,
			dataoutputs: serviceoutputparams,
		});

		_res.header('Content-type', 'text/xml');
		_res.end(renderedresponse);
	} //end _desribeProcess()


	/**
	 * Converts raw request to the array.
	 *
	 * @param string request Processed request
	 *
	 * @return mixed Array is function succeedes, false if error occured
	 */

	var _requestToArray = function(request, splitter) {
		if (splitter === undefined) {
			splitter = "&";
		}

		var result = false;
		if (request.length > 0) {
			if (request.charAt(0) === "?") {
				request = request.substr(1, request.length - 1);
			}

			var result          = new Object();
			var splittedrequest = request.split(splitter);
			for (let requestitem in splittedrequest) {
				var splittedrequestitem = splittedrequest[requestitem].split("=");				
				if (splittedrequestitem[0] === "DataInputs") {
					var keylength    = splittedrequestitem[0].length + 1;
					var inputsstring = splittedrequest[requestitem].substr(keylength, splittedrequest[requestitem].length - keylength);
					var inputs       = _requestToArray(inputsstring, ";");

					result[splittedrequestitem[0]] = inputs;
				} else {
					result[splittedrequestitem[0]] = splittedrequestitem[1];
				}
			}
		}

		return result;
	} //end _requestToArray()

} //end class

module.exports = WPSServer;