import fs from 'fs';
import sys from 'sys';
import child_process from 'child_process';
import { isundefined, log } from '../helpers';

/**
 * Coverting existing theme to shapefile.
 */
function theme2shp(databaseconnection) {
	var _databaseconnection = databaseconnection;

	/**
	 * Covert specified theme data to the shapefile.
	 *
	 * @param Object settings Specifies name of the theme or SQL that will be used to fetch data
	 *
	 * @return string
	 */

	this.convert = function(settings) {
		if (isundefined(settings) === true) {
			throw new Error("No settings were specified for conversion");
		}

		if (settings.hasOwnProperty("filename") === true) {
			var settings_filepath = settings.filename;
		} else {
			throw new Error("No shapefile filename specified");
		}

		if (settings.hasOwnProperty("schema") === true) {
			var settings_schema = settings.schema;
		} else {
			var settings_schema = "public";
		}

		if (settings.hasOwnProperty("themename") === true) {
			var settings_themename = settings.themename;
		} else {
			throw new Error("No theme name was specified");
		}

		if (settings.hasOwnProperty("sql") === true) {
			var settings_sql = '"' + settings.sql.replace(/"/g, '\\"') + '"';
		} else {
			var settings_sql = "";
		}

		try {
			var result = child_process.execSync("pgsql2shp");
		} catch (e) {
			var count = (e.stderr.toString().match(/'pgsql2shp' is not recognized/g) || []).length;
			if (count === 1) {
				throw new Error("Failed at locating pgsql2shp executable, add PGSQL bin directory to the PATH environment variable\
				(common location for Windows - C:\\Program Files\\PostgreSQL\\9.*\\bin folder)");
			}
		}

		return _performCoversion({
			filepath  : settings_filepath,
			schema    : settings_schema,
			themename : settings_themename,
			sql       : settings_sql
		});
	} //end convert()

	/**
	 * Pefrorms pgsql2shp invocation.
	 *
	 * @param Object settings Trusted settings set
	 *
	 * @return boolean
	 */
	var _performCoversion = function(settings) {
		var settings_databaseuser         = _databaseconnection.connectionParameters.user;
		var settings_databaseuserpassword = _databaseconnection.connectionParameters.password;
		var settings_databasename         = _databaseconnection.connectionParameters.database;
		var settings_databaseport         = _databaseconnection.connectionParameters.port;
		var settings_databasehost         = _databaseconnection.connectionParameters.host;

		var executonstring = "pgsql2shp -k -f " + settings.filepath + " -u " + settings_databaseuser + " -p " + settings_databaseport
		+ " -P " + settings_databaseuserpassword + " -h " + settings_databasehost + " " + settings_databasename + " ";
		if (settings.sql) {
			executonstring += settings.sql;
		} else {
			executonstring += settings.themename;
		}

		var coverter;
		try {
			coverter = child_process.execSync(executonstring, {encoding: 'utf8', env: {PGCLIENTENCODING: "UTF8"}});
		} catch (err) {
			log("them2shp failed");
			log(err.cmd);
			return false;
		}

		if (coverter.toString().match(/Done/).length === 1) {
			return true;
		} else {
			log("them2shp failed");
			log(coverter.toString());
		}

		return true;
	} //end _performCoversion()

} //end theme2shp()

module.exports = theme2shp;
