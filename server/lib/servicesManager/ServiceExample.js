import { str_replace, log } from '../helpers';

/**
 * ServiceExample class
 */

var ServiceExample = function (client, userId) {
	var _userid = userId;
	var _meid   = null;
	var _client = client;
	var _methoddata;

	/**
	 * Creating method example in the database.
	 *
	 * @param string   serviceid Identitfcator of the parent service
	 * @param string   rawinput  User input parameters
	 * @param Function callback  Callback function that will be performed after the creation
	 *
	 * @return void
	 *
	 * @throws Error
	 */

	this.create = function (serviceid, rawinput, callback) {
		var newserviceexamplequery = 
		"INSERT INTO public.method_examples (mid, is_deleted, status, start_time, console_output, error_output, input_params, owner) \
		VALUES (" + serviceid + ", FALSE, 'METHOD_EXAMPLE_CREATED', NOW(), 'Standard output started', 'Error output started', E'" + rawinput + "',\
		'" + _userid + "') RETURNING id;";

		_client.query(newserviceexamplequery, function(error, rows, fields) {
			if (error === null) {
				_meid = rows.rows[0].id;
				callback(_meid);
			} else {
				log(error);
				throw new Error("Unable to perform creation request");
			}
		});
	} //end create()

	/**
	 * Returns method example identitficator.
	 *
	 * @return string
	 *
	 * @throws Error
	 */

	this.getID = function() {
		if (_meid === null) {
			throw new Error("Unable to return identitficator of uninitialized MethodExample instance");
		} else {
			return _meid;
		}
	} //end getID()

	/**
	 * Updates certain MethodExample instance property.
	 *
	 * @param string   property Name of the property
	 * @param string   value    Property value
	 * @param Function callback Callback function that will be performed after the update
	 *
	 * @return void
	 *
	 * @throw Error
	 */

	this.updateProperty = function (property, value, callback) {
		if (property === "console_output" || property === "error_output") {
			throw new Error("Use corresponding methods in order to append to consoles");
		}

		var appendquery = "UPDATE public.method_examples SET " + property + " = '" + value + "' WHERE id = " + _meid + ";";
		_client.query(appendquery, function(error, rows, fields) {
			if (error === null) {
				if (typeof callback !== "undefined") {
					callback();
				}
			} else {
				log(appendquery);
				log(error);
				throw new Error("Unable to perform update");
			}
		});
	} //end updateProperty()

	/**
	 * Updating STDOUT service console.
	 *
	 * @param string   message  Appended message
	 * @param Function callback Callback function that will be performed after the update
	 *
	 * @return void
	 */

	this.appendToSTDOUT = function (message, callback) {
		message = str_replace("'", "QUOTE", message);

		var appendquery = "UPDATE public.method_examples SET console_output = console_output || E'\\n' || '" + message + "' WHERE id = " + _meid + ";";
		_client.query(appendquery, function(error, rows, fields) {
			if (error === null) {
				if (typeof callback !== "undefined") {
					callback();
				}
			} else {
				log(error);
				throw new Error("Unable to perform STDOUT update");
			}
		});		
	} //end appendToSTDOUT()

	/**
	 * Updating STDERR service console.
	 *
	 * @param string   message  Appended message
	 * @param Function callback Callback function that will be performed after the update
	 *
	 * @return void
	 */

	this.appendToSTDERR = function (message, callback) {
		if (message !== '') {
			message = str_replace("'", "QUOTE", message);

			var appendquery = "UPDATE public.method_examples SET error_output = error_output || E'\\n' || '" + message + "' WHERE id = " + _meid + ";";
			_client.query(appendquery, function(error, rows, fields) {
				if (error === null) {
					if (typeof callback !== "undefined") {
						callback();
					}
				} else {
					log(error);
					throw new Error("Unable to perform STDERR update");
				}
			});
		}
	} //end appendToSTDERR()
} //end class

// Exporting
module.exports = ServiceExample;