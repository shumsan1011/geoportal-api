import ServiceExample from './ServiceExample';
import fs from 'fs';
import filelink from '../filelink';
import config from '../../../config/config';
import { log, str_replace, isundefined, explode, cleanNewLines, functionReverseTags, functionGetName } from '../helpers';
import * as esprima from 'esprima';
import md5 from 'md5';
import axios from 'axios';

/**
 * Class for executing the service.
 */
function WPSServiceExecutor(client, userId, userName, serviceinfo, servicesinfo, executerconfig) {
  var _currentuserid   = userId;
  var _currentusername = userName;

	var _serviceexampleinstanceid = false;

	var _databaseclient  = client;
  var _serviceinfo     = serviceinfo;
  var _servicesinfo    = servicesinfo;
	var _executerconfig  = executerconfig;
  var _positivecallback, _failurecallback, _useroutputparameters;

	/**
	 * Returns identifier of the service example
	 *
	 * @return integer
	 */
	this.getServiceExampleId = function () {
		if (_serviceexampleinstanceid === false) {
			throw "Unable to get the service example identifier";
		} else {
			return _serviceexampleinstanceid;
		}
	} // end getServiceExampleId()
	
	
	var _scenarioCanBeExecuted = function(rootFunctionName, launchedcode) {
		let result = new Promise((resolve, reject) => {
			const constructPath = (data) => {
				return 'http://' + data.host + ':' + data.port + data.path;
			}

			let participatingServices = [];
			if (_serviceinfo.type === 'js') {
				var calledFunctions = [];

				var result = esprima.parseScript(launchedcode);
				var rootFunctionAST = false;

				for (var i = 0; i < result.body.length; i++) {
					if (result.body[i].type === 'FunctionDeclaration' && result.body[i].id && result.body[i].id.name === rootFunctionName) {
						rootFunctionAST = result.body[i];
						break;
					}
				}

				function traverse(o, func) {
					for (var i in o) {
						func.apply(this,[i,o[i]]);  
						if (o[i] !== null && typeof(o[i]) == "object") {
							traverse(o[i], func);
						}
					}
				}

				if (rootFunctionAST) {
					traverse(rootFunctionAST, (key, value) => {
						if (value && value.type && value.type === 'CallExpression') {
							if (value.callee.type === 'Identifier') {
								if (calledFunctions.indexOf(value.callee.name) === -1) {
									calledFunctions.push(value.callee.name);
								}
							}
						}
					});
				}

				for (let key in _servicesinfo) {
					let service = _servicesinfo[key];
					if (service.type === 'wps') {
						if (calledFunctions.indexOf(service.name) !== -1) {
							participatingServices.push(service);
						}
					}
				}
			} else {
				participatingServices.push(_serviceinfo);
			}

			let serversPathsToCheck = [];			
			participatingServices.map(item => {
				let parsedServers = JSON.parse(item.wpsservers);
				for (let subkey in parsedServers) {
					let path = constructPath(parsedServers[subkey]);
					let checkPath = path;
					if (parsedServers[subkey].path[parsedServers[subkey].path.length - 1] !== '?') {
						checkPath += '?';
					}

					checkPath += 'service=WPS&request=GetCapabilities';

					let pathHash = md5(path);
					let pathIsAlreadyStored = false;
					serversPathsToCheck.map(subItem => {
						if (subItem.password === pathHash) {
							pathIsAlreadyStored = true;
							return false;
						}
					});

					if (pathIsAlreadyStored === false) {
						serversPathsToCheck.push({
							hash: pathHash,
							checkPath,
							path
						});
					}
				}
			});

			let successfullChecks = 0;
			let unsuccessfullChecks = 0;
			let failedHostsCheckPaths = [];
			const registerCheck = (successfull, checkPath) => {
				if (successfull) {
					successfullChecks++;
				} else {
					unsuccessfullChecks++;
					failedHostsCheckPaths.push(checkPath);
				}

				if (successfullChecks === serversPathsToCheck.length) {
					resolve();
					return;
				} else if ((successfullChecks + unsuccessfullChecks) === serversPathsToCheck.length) {
					let atLeastOneServiceCanNotBeExecuted = false;
					participatingServices.map(item => {
						let serviceCanBeExeted = true;

						let parsedServers = JSON.parse(item.wpsservers);
						let numberOfWorkingServers = 0;
						parsedServers.map(server => {
							let serverIsWorking = true;

							let serverPath = constructPath(server);
							failedHostsCheckPaths.map(failedPath => {
								if (failedPath.indexOf(serverPath) === 0) {
									serverIsWorking = false;
									return false;
								}
							});

							if (serverIsWorking) numberOfWorkingServers++;
						});

						if (numberOfWorkingServers === 0) {
							atLeastOneServiceCanNotBeExecuted = true;
							return false;
						}
					});

					if (atLeastOneServiceCanNotBeExecuted) {
						reject();
					} else {
						resolve();
					}
				}
			}

			if (serversPathsToCheck.length === 0) {
				resolve();
			} else {
				for (var key in serversPathsToCheck) {
					axios.request({
						url: serversPathsToCheck[key].checkPath,
						timeout: 2000
					}).then(() => {
						registerCheck(true);
					}).catch(error => {
						registerCheck(false, error.config.url);
					});
				}
			}
		});

		return result;
	} //end _scenarioCanBeExecuted()


	/**
	 * Executing the sevice.
	 *
	 * @param function callback            Positive callback
	 * @param Object   userinputparameters User input parameters
	 *
	 * @return void
	 */
	this.execute = function(positivecallback, failurecallback, serviceid, userinputparameters, useroutputparameters) {
		_positivecallback     = positivecallback;
		_failurecallback      = failurecallback;
		_useroutputparameters = useroutputparameters;

		var rawinput               = JSON.stringify(userinputparameters);
    var serviceexampleinstance = new ServiceExample(_databaseclient, _currentuserid);
		serviceexampleinstance.create(serviceid, rawinput, function (serviceexampleid) {
			_serviceexampleinstanceid   = serviceexampleid;
      var rawuserparameters       = str_replace('"', '\'', rawinput);
			var rawfunctionsdefinition  = _getExecutedRawFunctionsDefinition(serviceexampleid);
			var rawvaluestoredefinition = _getValueStoreDefinition(useroutputparameters);

			var specification = "";
			if (_serviceinfo.map_reduce_specification) {
				specification = new Buffer(_serviceinfo.map_reduce_specification.replace(/(\r\n|\n|\r|\s+|\t)/gm, "")).toString('base64');
			}

			var launchedcode = rawfunctionsdefinition.rawfunctionsstring + 
			rawvaluestoredefinition.prolog +
			rawfunctionsdefinition.currentservicefunctionname +
			"(" + rawinput + ", " + rawvaluestoredefinition.mapping + ", '" + specification + "');" +
			rawvaluestoredefinition.epilog;
			launchedcode = launchedcode.replace(/"/g, "'");

			_scenarioCanBeExecuted(rawfunctionsdefinition.currentservicefunctionname, launchedcode).then(() => {
				let userFolder = rawvaluestoredefinition.userFolder;
				
				var url = config.server.url;
				if (url) {
					if (url.substr((url.length - 2), (url.length - 1)) !== "/") {
						url += "/";
					}
				} else {
					throw "Unable to retrieve server url";
				}
				
				var folder = process.cwd().replace("\\", "/");
				if (folder.substr((folder.length - 2), (folder.length - 1)) !== "/") {
					folder += "/public/";
				} else {
					folder += "public/";
				}

				var serviceparameters = [serviceexampleid, launchedcode, folder, url, userFolder];
				_launchExecutable(rawfunctionsdefinition, serviceparameters, serviceexampleinstance);
			}).catch(error => {
				log('Unable to execute scenario due to computational nodes failure');
				failurecallback();
			});
		}, _failurecallback);
	} //end execute()


	/**
	 * Launches binary in order to execute the service.
	 *
	 * @param Object        functionsdata         Service data
	 * @param array         serviceparameters     Parameters for executable
	 * @param ServiceExample serviceexampleinstance Method example instance
	 *
	 * @return void
	 */

	var _launchExecutable = function(functionsdata, serviceparameters, serviceexampleinstance) {
		if (_executerconfig.executablepath === undefined || _executerconfig.executablepath === '') {
			let errorMessage = "ERROR: Executer (" + _executerconfig.executablepath + ") is not defined";
			log(errorMessage);
      _failurecallback(errorMessage);
		} else if (fs.existsSync(_executerconfig.executablepath) === false) {
			let errorMessage = "ERROR: Specified executer (" + _executerconfig.executablepath + ") does not exist";
      log(errorMessage);
      _failurecallback(errorMessage);
    } else if (fs.existsSync(_executerconfig.cwd) === false) {
			let errorMessage = "ERROR: Specified working directory (" + _executerconfig.cwd + ") does not exist";
			log(errorMessage);
      _failurecallback(errorMessage);
		} else {
			var terminal = require('child_process').spawn(_executerconfig.executablepath, serviceparameters, {cwd: _executerconfig.cwd});

      terminal.stdout.on('data', function (data) {
        var arr = explode('\r\n', '' + data);
        for (var i = 0; i < arr.length; i++) {
          if (arr[i] === "") {
            arr.splice(i, 1);
          }
        }

        var valuetype = "";
        var objectwasreceived = true;
        var o = {};
        try {
          eval('o = ' + arr[(arr.length - 1)] + '; valuetype = typeof o;');
        } catch(error) {
          objectwasreceived = false;
        } //end try

        if (objectwasreceived === true && valuetype !== "string") {
          var result = new Object();
          if (typeof functionsdata.outputparamsforcurrentfunction !== 'undefined') {
            var outputParametersDescriptions = JSON.parse(_serviceinfo.output_params);
            for (var property in o) {
              if (o.hasOwnProperty(property)) {
                var propertyWasProcessed = false;
                for (var i = 0; i < outputParametersDescriptions.length; i++) {
                  if (outputParametersDescriptions[i].fieldname.toLowerCase() === property.toLowerCase()) {
                    if (outputParametersDescriptions[i].widget.name === "file_save") {
                      if (property in _useroutputparameters) {
                        result[property] = filelink.getUserFilePath(_useroutputparameters[property], _currentusername);
                        propertyWasProcessed = true;
                      }
                    }
                  }
                }
                
                if (propertyWasProcessed === false) {
                  result[property] = o[property];
                }
              } //end if
            } //end for
          } //end if

          serviceexampleinstance.updateProperty('end_time', 'NOW()');
          serviceexampleinstance.updateProperty('status', 'METHOD_EXAMPLE_SUCCEEDED');
          serviceexampleinstance.updateProperty('result', JSON.stringify(result));
        } else {
          for (var i = 0; i < arr.length; i++) {
            serviceexampleinstance.appendToSTDOUT(arr[i]);
          }
        } //end if
      });

      terminal.stderr.on('data', function (data) {
        var arr = explode('\r\n', '' + data);
        if (arr[0] !== '') {
          for (var i = 0; i < arr.length; i++) {
            serviceexampleinstance.appendToSTDERR(arr[i]);
          }

          serviceexampleinstance.updateProperty('end_time', 'NOW()');
          serviceexampleinstance.updateProperty('status', 'METHOD_EXAMPLE_FAILED');
        } //end if
      });
      
      terminal.on('close', function (code) {
        log('child process exited with code ' + code);
        if (code !== 0) {
          serviceexampleinstance.appendToSTDERR("Error occurred, application exited with code " + code);
          serviceexampleinstance.updateProperty('end_time', 'NOW()');
          serviceexampleinstance.updateProperty('status', 'METHOD_EXAMPLE_FAILED');
        }
      });

      _positivecallback(serviceexampleinstance.getID());
    }
	} //end _launchExecutable()


	/**
	 * Returns ready-to-use set of strings with definitions for ValueStore objects;
	 *
	 * @return Object
	 */

	var _getValueStoreDefinition = function(useroutputparameters) {
		var rawoutputparametersstring = "";
		var outputparameters          = JSON.parse(_serviceinfo.output_params);
		var numberofoutputparameters  = outputparameters.length;
		for (var i = 0; i < numberofoutputparameters; i++) {
			var parameter = outputparameters[i];

			rawoutputparametersstring += " var outputparameter" + parameter.fieldname + "Store" + i + " = new ValueStore();";
		}

		var mappingparametersstring = "{";
		var outputwidgets           = "{";
		for (var i = 0; i < numberofoutputparameters; i++) {
			var parameter = outputparameters[i];

			mappingparametersstring += parameter.fieldname + ": outputparameter" + parameter.fieldname + "Store" + i;
			outputwidgets += parameter.fieldname + ": '" + parameter.widget.name + "'";

			if (i !== (numberofoutputparameters - 1)) {
				mappingparametersstring += ",";
				outputwidgets           += ",";
			}
		}

		mappingparametersstring += "}";
		outputwidgets += "}";

		var rawoutputwaitforparametersstring = "var rawoutput = {";
		for (var i = 0; i < numberofoutputparameters; i++) {
			var parameter = outputparameters[i];

			rawoutputwaitforparametersstring += parameter.fieldname + ": outputparameter" + parameter.fieldname + "Store" + i + ".get()";
			if (i !== (numberofoutputparameters - 1)) {
				rawoutputwaitforparametersstring += ",";
			}
		}

		var saveExecutionResultsPath = filelink.getAbsolutePath("", _currentusername);
		if (saveExecutionResultsPath.slice(-1) !== "/") saveExecutionResultsPath += "/";

		let userFolder = saveExecutionResultsPath + _currentusername;

		rawoutputwaitforparametersstring += "}; var output = processOutput(rawoutput, " + outputwidgets + ", '"
		+ userFolder + "', " + JSON.stringify(useroutputparameters).replace(/"/g, "'") + "); output";

		return {
			prolog:     rawoutputparametersstring,
			mapping:    mappingparametersstring,
			epilog:     rawoutputwaitforparametersstring,
			userFolder: userFolder + "/"
		};
	} //end _getValueStoreDefinition()


	/**
	 * Returns ready-to-use set of strings for registered service functions.
	 *
	 * @return Object
	 */

	var _getExecutedRawFunctionsDefinition = function() {
		var calledServiceBody = false;
		for (var i = 0; i < _servicesinfo.length; i++) {
			if (_servicesinfo[i].id == _serviceinfo.id) {
				calledServiceBody = cleanNewLines(functionReverseTags(_servicesinfo[i].js_body, true));
			}
		}

		var currentservicefunctionname = "";
		var rawfunctionsstring         = "";

		var inputparamsforcurrentfunction;
		var outputparamsforcurrentfunction;

		for (var i = 0; i < _servicesinfo.length; i++) {
			var currentFunctionName = functionGetName(_servicesinfo[i].js_body);
			if (calledServiceBody.indexOf(currentFunctionName) !== -1) {
				var inputparams = JSON.parse(_servicesinfo[i].params);

				var inputparamsshortened = "{";
				for (var j = 0; j < inputparams.length; j++) {
					inputparamsshortened += inputparams[j].fieldname + ": '" + inputparams[j].widget.name + "'";
					if (j < (inputparams.length - 1)) {
						inputparamsshortened += ",";
					}
				} //end for

				inputparamsshortened += "}";

				if ((_servicesinfo[i].output_params != null) && (_servicesinfo[i].output_params !== 'undefined')) {
					var outputparams = JSON.parse(_servicesinfo[i].output_params);

					var outputparamsshortened = "{";
					for (var j = 0; j < outputparams.length; j++) {
						outputparamsshortened += outputparams[j].fieldname + ": '" + outputparams[j].type + "'";
						if (j < (outputparams.length - 1)) {
							outputparamsshortened += ",";
						} //end if							
					} //end for

					outputparamsshortened += "}";
				} else {
					outputparamsshortened = "{}";
				} //end if

				if (_servicesinfo[i].id == _serviceinfo.id) {
					eval('inputparamsforcurrentfunction = ' + inputparamsshortened);
					eval('outputparamsforcurrentfunction = ' + outputparamsshortened);
				} //end if

				rawfunctionsstring += cleanNewLines(functionReverseTags(_servicesinfo[i].js_body, true)) + '; ';

				if (_servicesinfo[i].id == _serviceinfo.id) {
					var currentservicefunctionname = functionGetName(_servicesinfo[i].js_body);
				} //end if
			} //end if
		} //end for

		if (isundefined(currentservicefunctionname) === true) {
			let errorMessage = "Unable to get current service function name";
			_failurecallback(errorMessage);
			throw new Error(errorMessage);
		}

		return {
			rawfunctionsstring,
			currentservicefunctionname,
			inputparamsforcurrentfunction,
			outputparamsforcurrentfunction
		};
	} //end _getExecutedRawFunctionsDefinition()
} //end class


module.exports = WPSServiceExecutor;