import path from 'path';
import filelink from '../filelink';
import theme2shp from './theme2shp';
import geo_table from '../geoThemes/geo_table';
import { meta_manager } from '../geoThemes/geo_meta_table';
import db from '../db';

const client = db.getClient();

/**
 * Class for transforming input parameters for service regarding its widget settings.
 */

function WPSInputParametersProcessor(serviceId, userId, userName) {
	var _serviceId = serviceId;
	var _userId    = userId;
	var _userName  = userName;

	/**
	 * Processing input parameters according to widget politics.
	 *
	 * @param Object parameterdescriptions
	 * @param Object parametervalues
	 *
	 * @return Object
	 */

	this.process = function(parameterdescriptions, parametervalues, processingCallback) {
		var numberofparameters = parameterdescriptions.length;
		var processedFieldnames = new Array();
		var settleResult = function (fieldname, value) {
			if (value === false) {
				for (var key in parametervalues) {
					if (key === fieldname) {
						delete parametervalues[key];
						break;
					}
				}
			} else {
				parametervalues[fieldname] = value;
			}
			
			for (var i = 0; i < processedFieldnames.length; i++) {
				if (processedFieldnames[i] === fieldname) {
					processedFieldnames.splice(i, 1);
					break;
				}
			}

			if (processedFieldnames.length === 0) {
				processingCallback(parametervalues);
			}
		}

		for (var i = 0; i < numberofparameters; i++) {
			processedFieldnames.push(parameterdescriptions[i].fieldname);
		}

		for (var i = 0; i < numberofparameters; i++) {
			if (parametervalues[parameterdescriptions[i].fieldname]) {
				switch (parameterdescriptions[i].widget.name) {
					case "file_save":
						_processFileSave(parameterdescriptions[i].fieldname, parametervalues[parameterdescriptions[i].fieldname], _userName, settleResult);
						break;
					case "file":
						if (parameterdescriptions[i].fieldname in parametervalues) {
							_processFile(parameterdescriptions[i].fieldname, parametervalues[parameterdescriptions[i].fieldname], _serviceId, settleResult);
						} else {
							throw new Error("Unable to get corresponding input value for " . parameterdescriptions[i].fieldname);
						}
						break;
					case "theme_select":
						_processThemeSelect(parameterdescriptions[i].fieldname, parametervalues[parameterdescriptions[i].fieldname], settleResult);
						break;
					case "rectangle":
						_processExtent(parameterdescriptions[i].fieldname, parametervalues[parameterdescriptions[i].fieldname], settleResult);
						break;
					default:
						settleResult(parameterdescriptions[i].fieldname, parametervalues[parameterdescriptions[i].fieldname]);
						break;
				} //end switch
			} else {
				settleResult(parameterdescriptions[i].fieldname, false);
			} //end if
		} //end for
	} //end process()


	var _processFileSave = function(fieldname, value, currentusername, callback) {
		callback(fieldname, filelink.getAbsolutePath(value, currentusername));
	} //end _processFileSave()


	/**
	 * Processing file widget.
	 *
	 * @param string value Parameter value
	 *
	 * @return string
	 */

	var _processFile = function(fieldname, value, serviceid, callback) {
		var absolutepath = filelink.getAbsolutePath(value, _userName);
		var link         = filelink.getLinkForFile(absolutepath, serviceid);

		callback(fieldname, link);
	} //end _processFile()


	/**
	 * Processing the theme select widget.
	 *
	 * @param string value Parameter value
	 *
	 * @return string
	 */

	var _processThemeSelect = function (fieldname, value, callback) {
    /*
    @todo 
    */
    //throw 'Stuck at multiple geo_meta_managers';

		var tableID;
		var filterValues;
		if (value.indexOf(":") !== -1) {
			var splitValue = value.split(":");
			tableID = parseInt(splitValue[0]);
			filterValues = JSON.parse(new Buffer(splitValue[1], 'base64').toString());
			for (var key in filterValues) {
				filterValues["f_" + key] = filterValues[key];
			}
		} else {
			tableID = parseInt(value);
			filterValues = {value: tableID};
		}

    meta_manager.curent_user_id = _userId;
		meta_manager.get_table_json({}, tableID, function (meta){
			var requestedGeoTable = new geo_table(_userId, tableID, {client: client}, meta, {convertGeometries: false});
			var query = requestedGeoTable.buildquery(undefined, filterValues);

      /*
      @todo Use correct database credentials
      */
			//if ("database_login" in _req === false) {
			if (false) {
				throw new Error('Unable to get database connection credentials from the "req" object');
			} //end if

			var filetosaveslash = filelink.getRandomFileName(".shp");
			filetosave          = filetosaveslash.replace(new RegExp("/",'g'), '\\');
			var localtheme2shp = new theme2shp(testclient);
			localtheme2shp.convert({
        filename: filetosave,
        /*
        @todo Use correct database credentials
        */
				//schema: _req.database_schema,
				schema: '',
				themename: meta.tablename,
				sql: query
			});

			var link = filelink.getLinkForFile(filetosaveslash, "t2s");
			callback(fieldname, link);
		});
	} //end _processThemeSelect()


	/**
	 * Processing extent widget.
	 *
	 * @param string value Parameter value
	 *
	 * @return string
	 */

	var _processExtent = function(fieldname, value, callback) {
		var reg = /([0-9]*\.?[0-9]*)\s([0-9]*\.?[0-9]*)\,\s?([0-9]*\.?[0-9]*)\s([0-9]*\.?[0-9]*)\,\s?([0-9]*\.?[0-9]*)\s([0-9]*\.?[0-9]*)/;
		var myArray = reg.exec(value);

		if (myArray && myArray.length === 7) {
			x1 = myArray[1];
			y1 = myArray[2];
			x2 = myArray[5];
			y2 = myArray[6];
			value = x1 + ',' + y1 + ',' + x2 + ',' + y2 + ',urn:ogc:def:crs:EPSG:6.6:4326';

			callback(fieldname, value);
		} else {
			callback(fieldname, "");
		}
	} //end _processExtent()


} //end class


module.exports = WPSInputParametersProcessor;