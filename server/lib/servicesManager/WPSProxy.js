/**
 * Proxy for avoiding Same-Origin-Policy restrictions
 */

import path from 'path';
import queryString from 'querystring';
import url from 'url';
import http from 'http';


/**
* Index page
*/
function request(req, res, options, next) {
	var theUrl = url.parse(req.url);
  var tempo  = queryString.parse(theUrl.query);

  /*
  @todo User-spevific database client
  calipso.modules.postgres.fn.init_gis_user(req, () => {
    ...
  }
  */
	
  if (tempo.type == 'geonames') {
    var options = {
      host   : tempo.host,
      path   : '/' + tempo.path + '&username=' + tempo.username,
      method : 'GET'
    };
  } else if (tempo.type == 'regular') {
    var options = tempo.url;		
  } else {
    var options = {
      host   : tempo.host,
      port   : tempo.port,
      path   : tempo.wpspath + tempo.request,
      method : 'GET'
    };
  } //end if

  var username = req.session.user.username;

  // Transforming filepaths
  if (tempo.internalid != undefined && tempo.internalid != '0') {
    var query  = 'SELECT params FROM wps_methods WHERE id = ' + tempo.internalid;
    var cquery = client.query(query, function(error, rows, fields) {
      if (error === null) {		
        var obj = JSON.parse(rows.rows[0].params);
        for (var i = 0; i < obj.length; i++) {
          if ((obj[i].widget.name == 'file') || (obj[i].widget.name == 'file_save')) {
            options.path = replaceFilepath(obj[i].fieldname, options.path, username);
          } else if (obj[i].widget.name == 'theme_select' ) {
            options.path = replaceThemeName(req, obj[i].fieldname, options.path, username);
          } else if (obj[i].widget.name == 'rectangle' ) {
            options.path = replaceEXTENT(req, obj[i].fieldname, options.path, username);
          }
        }
        
        var intrequest = http.request(options, function(result) {
          result.setEncoding('utf8');
        });

        intrequest.on('response', function (response) {
          var body = '';
          
          response.on('data', function (chunk) {
            body += chunk;
          });

          response.on('end', function () {
            res.format = "xml";
            res.end(body,"UTF-8");
            return;
          });
        });

        intrequest.on('error', function(e) {
          log('Problem with request: ' + e.message);
        });

        intrequest.end();
      } else {
        log('Problem with request: ' + e.message);
      }
    });
  } else {
    var intrequest = http.request(options, function(result) {
      result.setEncoding('utf8');
    });

    intrequest.on('response', function (response) {
      var body = '';
      response.on('data', function (chunk) {
        body += chunk;
      });

      response.on('end', function () {
        res.format = "xml";
        res.end(body,"UTF-8");
        return;
      });
    });

    intrequest.on('error', function(e) {
      log('Problem with request: ' + e.message);
    });

    intrequest.end();
  }
} //end indexProxy()

module.exports = request;