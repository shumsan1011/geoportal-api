import fs from 'fs';
import sys from 'sys';
import mime from 'mime';
import { log } from '../helpers';

const dir_separator = '/';

// Temporary mock
const calipso = {
  modules: {
    postgres: {
      fn: {
        init_gis_user: () => { throw 'Mock method'; },
        getUserFolder: () => { throw 'Mock method'; }
      }
    }
  },
  config: {
    getModuleConfig: () => { throw 'Mock method'; }
  }
};


var ftps = require("./filezilla");

var passgen = require('password-generator');
var Query = require("mongoose").Query;

/**
* Small block that will appear on every page
*/
function fm2(req, res, template, block, next) {
  console.log('I am here begin fm2');
  console.log(req.query);
  res.format = "json";
  var nodes = new Array();
  if(req.session.user===undefined || req.session.user==null){
    res.end(JSON.stringify({"error" : "You should log in."}),"UTF-8"); 
    console.log('I am here begin fm2 end');
    return;
  }

  calipso.modules.postgres.fn.init_gis_user(req, function(){
    try{
      addfile = (req.query.mode === undefined || req.query.mode=='1')
      adddir = (req.query.mode === undefined || req.query.mode=='2')
      //var roots=get_rootdirs(req);
      var root_dir=req.root_dirs[0];
      
      fhesh=req.query.id;
      if (fhesh=='1'){
        var cpath=root_dir;
        var chash=gethash(root_dir + dir_separator +req,req.userfolder);
        var f={"attr":{"id":chash,"rel":"folder"},"data":req.userfolder,"state":"closed"};
        nodes[nodes.length]=f;
      }
      else{
        var cpath=getpath(req,fhesh);
        console.log('readdir');
        var files = fs.readdirSync(cpath);
        console.log('readdir end');
        for(var i in files) {
          if(files[i].charAt(0)=='.') continue;
            var cname=cpath+dir_separator+files[i];
          var chash=gethash(req,cname);
          var stats=fs.statSync(cname);
          if (stats.isDirectory() && adddir){
            var f={"attr":{"id":chash,"rel":"folder"},"data":files[i],"state":"closed"};
            nodes[nodes.length]=f;
          }		  	
          if (stats.isFile() && addfile){
            var f={"attr":{"id":chash,"rel":"default"},"data":files[i],"state":""};
            nodes[nodes.length]=f;
          }		  	
        }
      }
    }
    catch (e) {
      console.log("I am here");
      console.log(req.query);
      console.log(e);
      res.end(JSON.stringify({"error" : e}),"UTF-8"); 
      console.log('I am here begin fm2 end');
      return;		
    }

    res.end(JSON.stringify(nodes),"UTF-8"); 
    console.log('I am here begin fm2 end');
    return;
  });
};

/**
* Small block that will appear on every page
*/
function prfm(req, res, template, block, next) {
console.log('I am here begin prfm');
if(req.session.user===undefined || req.session.user==null){
  res.end(JSON.stringify({"error" : "You should log in."}),"UTF-8"); 
  return;
}
calipso.modules.postgres.fn.init_gis_user(req, function(){
  //var roots=get_rootdirs(req);
  var root_dir=req.root_dirs[0];
  res.format = "json";
  var files = new Array();
  var dfiles = new Array();
  var dirs = new Array();

  switch (req.body.action) {
     case 'read':{
      console.log(req.body.param0);
      console.log('readdir 2');
      var files = fs.readdirSync(root_dir+dir_separator+req.body.param0);
      console.log('readdir end ');			  
    for(var i in files) {
      if(files[i].charAt(0)=='.') continue;
//		  console.log(files[i]);
      var stats=fs.statSync(root_dir+dir_separator+req.body.param0+dir_separator+files[i]);
      if (stats.isDirectory()){
        var dir={n:files[i], p:req.body.param0+dir_separator+files[i],m:files[i]};
        dirs[dirs.length]=dir;
      }		  	
      if (stats.isFile()){
        var f={n:files[i], p:req.body.param0+dir_separator+files[i],m:files[i]};
        dfiles[dfiles.length]=f;
      }		  	
      
    }
//	      calipso.log("read!");
      break;
    }
     case 'delete':{
      calipso.log("delete!");
      break;
    }
     default:{
      calipso.log("default!");
      break;
     }
  }
  res.end(JSON.stringify({ status: true, current: req.body.param0, bcrumb: [ "webfm" ], "dirs": dirs, "files": dfiles}),"UTF-8"); 
});
return;
};

/**
* file manager form
*/
function rfm_form(req, res, template, block, next) {


// Get some data (e.g. this could be a call off to Mongo based on the params
var item = {
    variable: "Hello World33333",
    params: '1'
};

// Now render the output
calipso.theme.renderItem(req, res, template, block, {
  item: item
},next);

};


/**
* page for configuring fileZilla
*/
function ftps_form(req, res, template, block, next) {
// Get some data (e.g. this could be a call off to Mongo based on the params
var username=req.session.user.username;
var pswd=passgen(10, false);
var ftpsconfigs = calipso.config.getModuleConfig("rfm", "ftpsconfigs");
var item={
    user: username,
    pswd: pswd,
    };

var usercheck=ftps.fzUsers(username, 1, 'users', ftpsconfigs, '')
if (usercheck['ans']==0){ //User doesn't exist!
  item['u_ex']=0;
  }
else{
  item['u_ex']=1;
  }

// Now render the output
calipso.theme.renderItem(req, res, template, block, {
  item: item
},next);

};

//Add new user to FileZilla
function addftpsuser(req, res, template, block, next) {
res.format = "json";
calipso.modules.postgres.fn.init_gis_user(req, function(){
  var user_dir=req.root_dirs;	
  //console.log(user_dir[0]);
  var ftpsconfigs = calipso.config.getModuleConfig("rfm", "ftpsconfigs");
  var ans=ftps.fzUsers(req.session.user.username, 2, 'users', ftpsconfigs, req.query['pswd'], user_dir[0] )
  //console.log(ans);
  res.end(JSON.stringify(ans),"UTF-8"); 
});
}

//Delete ftps user
function delftpsuser(req, res, template, block, next) {
  res.format = "json";
  var ftpsconfigs = calipso.config.getModuleConfig("rfm", "ftpsconfigs");
  var ans=ftps.fzUsers(req.session.user.username, 3, 'users', ftpsconfigs);
  res.end(JSON.stringify(ans),"UTF-8");
}
//Change ftps user pswd
function pwsdchangeftpsuser(req, res, template, block, next) {
  res.format = "json";
  var ftpsconfigs = calipso.config.getModuleConfig("rfm", "ftpsconfigs");
  var ans=ftps.fzUsers(req.session.user.username, 4, 'users', ftpsconfigs, req.query['pswd']);
  res.end(JSON.stringify(ans),"UTF-8"); 
}

function nsep(cpath){
  var prev='';
  var res='';
  for (var i = 0; i < cpath.length; i++) {
    if(!((cpath.charAt(i)=='/' || cpath.charAt(i)=='\\') && (prev=='/' || prev=='\\' ) )){
      prev=cpath.charAt(i);
      res=res+cpath.charAt(i);
    }
  }
  return res;
}

function get_fd_desc(req, cpath){
  var roots=req.root_dirs;
  var root_id=-1;
  var hash=gethash(req,cpath);	
  var name=cpath.slice(cpath.lastIndexOf(dir_separator)+1);
  //	calipso.log("name="+name);
  for(var i in roots){
    if(roots[i]==cpath){
      root_id=i;
      break;
    }
  }
  if (root_id!=-1){
    var phash=hash;
  }
  else{
    var ppath=cpath.slice(0,-name.length-1);
    var phash=gethash(req, ppath);
  }
  rcpath=fs.realpathSync(cpath);
  var stats=fs.statSync(rcpath);
  if (stats.isDirectory()){
    var f={"mime":"directory","ts":1359191956,"read":1,"write":1,"size":0,"hash":hash, "name":name,"date":"Today 18:19","dirs":1};
    if (hash!=phash)
      f.phash=phash;

  } else if (stats.isFile()){
    var f={"mime":"file","ts":1359191956,"read":1,"write":1,"size":0,"hash":hash,"phash":phash,"volumeid":"l1_","name":name,"date":"Today 18:19","locked":0,"dirs":0};
  }

  else return null;

  return f;
};

function get_rootdirs(req){
  var mainrootdir = calipso.config.getModuleConfig("rfm", "mainrootdir");
  var UserFolder = calipso.modules.postgres.fn.getUserFolder(req);
  var rootdir = mainrootdir+dir_separator+UserFolder;
  if(!dirExistsSync(rootdir)){
    fs.mkdirSync(rootdir, '0755');
  }

  var root_dirs=[mainrootdir];
  // to do?????
  return root_dirs;
}

function gethash(req,cpath){
cpath=cpath+'';
var root_id=0;
var root_dirs=req.root_dirs;
//	for(var i in root_dirs) {
//		cdir=root_dirs[i];
  pos=cpath.indexOf(req.common_root);
  if (pos==0){
//			root_id=i;
    cpath=cpath.slice(req.common_root.length);
    cpath=cpath.replace(/^\\+/g,"");
//			break;
  }
//	}

if (cpath == '') {
  cpath = dir_separator;
}

var b = new Buffer(cpath);
var fhash = b.toString('base64');
fhash=fhash.replace(/\+/g,"-");
fhash=fhash.replace(/\//g,"_");
fhash=fhash.replace(/=/g,".");
fhash=fhash.replace(/\.+$/g,"");

root_id=parseInt(root_id)+1;
return 'l'+root_id+'_'+fhash;
}

function getpath(req,hash){
  var root_dirs= (req.common_root) ? req.common_root : '';
  if (hash=== undefined || hash=='' || hash=="l1_XA") {		
    return root_dirs+dir_separator+req.userfolder;
  }
  
  let dir=hash.slice(3);
  dir=dir.replace(/-/g,"+");
  dir=dir.replace(/_/g,"/");
  dir=dir.replace(/\./g,"=");
  var b = new Buffer(dir, 'base64');
  let subpath=b.toString();
  while (subpath.length>0 && subpath.charAt(0) == dir_separator) {
    subpath = subpath.slice(1);
  }
  
  if (subpath.indexOf(req.userfolder) != 0) {
    var result = root_dirs+dir_separator+req.userfolder;
    return '';
  }

  let res=root_dirs+dir_separator+subpath;
  res=nsep(res);
  return res;
}

function getpath_on_userdir(user_dir,hesh){	
  if (hesh=== undefined || hesh=='' || hesh=="l1_XA") {		
    return '';
  }
  var dir=hesh.slice(3);
  dir=dir.replace(/-/g,"+");
  dir=dir.replace(/_/g,"/");
  dir=dir.replace(/\./g,"=");	

  var b = new Buffer(dir, 'base64');
  var res=user_dir+dir_separator+b.toString();
  res=nsep(res);

  //res=fs.realpathSync(res);
  return res;
}

function hasAccess(req,cpath, mode){
  var root_dirs=req.root_dirs;
  var root_id=-1;
  for(var i in root_dirs) {
    cdir=root_dirs[i];
    pos=cpath.indexOf(cdir);
    if (pos==0){
      root_id=i;
      cpath=cpath.slice(cdir.length);
      cpath=cpath.replace(/^\\+/g,"");
      break;
    }
  }

  return true;
}

function GetTree(req,dirpath, depth, maxdepth, addroot, addfile ){
var ResArray=new Array();
calipso.log("dirpath="+dirpath);
var stats=fs.statSync(dirpath);
if (!stats.isDirectory()){
  calipso.log("is not dir");
  return ResArray;
}

if(dirpath.charAt(dirpath.length-1)==dir_separator)
  dirpath=dirpath.slice(0,-1);
var name=dirpath.slice(dirpath.lastIndexOf(dir_separator)+1);
var chash=gethash(req,dirpath);
var root_id=-1;
var root_dirs=req.root_dirs;
for(var i in root_dirs){
  if(root_dirs[i]==dirpath){
    root_id=i;
    break;
  }
}
if (root_id!=-1){
  var phash=chash;
}
else{
  var parentpath=dirpath.slice(0,-name.length-1);
  var phash=gethash(req, parentpath);
}

if(addroot==1){
  var f=get_fd_desc(req,dirpath);
  ResArray[ResArray.length]=f;
}
if (depth<=maxdepth){
  rdirpath=fs.realpathSync(dirpath);
      console.log('readdir 3');

  var readresults = fs.readdirSync(rdirpath);
      console.log('readdir end 3');		
  for(var i in readresults) {	
    if(readresults[i].charAt(0)=='.') continue;
    var cname=dirpath+dir_separator+readresults[i];
    rcname=fs.realpathSync(cname);
    var stats=fs.statSync(rcname);
    if (addfile!=1 && stats.isDirectory()){
      var res=GetTree(req,cname, depth+1,maxdepth,1, addfile);
      var ResArray = ResArray.concat(res);
    }		  		
    if (addfile==1 && stats.isFile()){
      var f=get_fd_desc(req,cname);
      ResArray[ResArray.length]=f;	  	
    }		
  }
}
return ResArray;
}

function getf(files, hash){
for(var i in files){
  if(files[i].hash==hash){
    return files[i];
  }
}
return 0;
}

/**

*/
function fm(req, res, template, block, next) {
if(req.session.user===undefined || req.session.user==null){
  res.end(JSON.stringify({"error" : "You should log in."}),"UTF-8"); 
  return;
}
console.log('I am here begin');
calipso.modules.postgres.fn.init_gis_user(req, function(){
  var root_dirs=req.root_dirs;
  if(req.query.cmd!='file')
    res.format = "json";
  var files = new Array();
  var result = new Object();
  try {
    switch (req.query.cmd) {
      case 'open':{
//				calipso.log("open!");
        if(req.query.init=='1'){
          cpath=root_dirs[0]+dir_separator;
          fhesh=gethash(req, cpath);
          result.options={"path":"files","url":"\\/1\\/php\\/..\\/files\\/","tmbUrl":"\\/1\\/php\\/..\\/files\\/.tmb\\/","disabled":[],"separator":"\\","copyOverwrite":1,"archivers":{"create":["application\\/zip"],"extract":["application\\/zip"]}};
          result.api ="2.0";
          result.uplMaxSize ="2M";
        }
        else{
          fhesh=req.query.target;
          cpath=getpath(req,fhesh);
        }
        if(cpath==''){
          res.end(JSON.stringify({"error" : "Path does not exist."}),"UTF-8"); 		
          return;
        }
        if (req.query.tree=='1')
          files=GetTree(req,cpath,0,0,1,1);
        else
          files=GetTree(req,cpath,0,1,1,1);
        result.files=files;
        result.cwd=getf(files,fhesh);
        break;
      }
       case 'tree':{			
        fhesh=req.query.target;
        var cpath=getpath(req,fhesh);
        if(cpath==''){
          res.end(JSON.stringify({"error" : "Path does not exist."}),"UTF-8"); 		
          return;
        }					
        result.tree=GetTree(req,cpath,0,0,1,0);
      break;
      }
       case 'mkdir':{			
        fhesh=req.query.target;
        var cpath=getpath(req,fhesh);
        if(cpath==''){
          res.end(JSON.stringify({"error" : "Dir does not exist."}),"UTF-8"); 		
          return;
        }					
        cpath=cpath+dir_separator+req.query.name;
        fs.mkdirSync(cpath, '0755');
        result.added=GetTree(req,cpath,0,0,1,0);
      break;
      }
       case 'rename':{			
        fhesh=req.query.target;
        var newname=req.query.name;
        var cpath=getpath(req,fhesh);
        if(cpath=='' || newname==''){
          res.end(JSON.stringify({"error" : "Path does not exist."}),"UTF-8"); 		
          return;
        }					
        var oldname=cpath.slice(cpath.lastIndexOf(dir_separator)+1);
        var parentpath=cpath.slice(0,-oldname.length-1);
        var newpath=parentpath+dir_separator+newname;
        cpath=fs.realpathSync(cpath);
        fs.renameSync(cpath, newpath);
        result.added=GetTree(req,newpath,0,0,1,0);
        result.removed=[fhesh];
      break;
      }
       case 'parents':{			
        fhesh=req.query.target;
        var cpath=getpath(req,fhesh);
        if(cpath==''){
          res.end(JSON.stringify({"error" : "Path does not exist."}),"UTF-8"); 		
          return;
        }					
        var name=cpath.slice(cpath.lastIndexOf(dir_separator)+1);
        var parentpath=cpath.slice(0,-name.length-1);
        result.tree=GetTree(req,parentpath,0,0,1,0);
      break;
      }
       case 'ping':{//to do			
        result.removed=new Array();			
      break;
      }
       case 'duplicate':{//to do			
        result.removed=new Array();			
      break;
      }
       case 'rm':{		
        var targets=req.query.targets;
        result.removed=new Array();
        for(var i in targets){
          fhesh=targets[i];
          var cpath=getpath(req,fhesh);
          resdirs=fm_rec_delete(req,cpath);
          result.removed=result.removed.concat(resdirs);
        }
      break;
      }
       case 'ls':{			
        var targets=req.query.targets;
        var cpath=getpath(req,fhesh);
        if(cpath==''){
          res.end(JSON.stringify({"error" : "Path does not exist."}),"UTF-8"); 		
          return;
        }					
        result.tree=GetTree(req,cpath,0,0,0,1);
      break;
      }
       case 'file':{		
        var target=req.query.target;
        var cpath=getpath(req,target);					
        if(cpath!='' && fs.existsSync(cpath)){
          var filename = path.basename(cpath);
          var mimetype = mime.lookup(cpath);
          res.setHeader('Content-disposition', 'attachment; filename=' + filename);
          res.setHeader('Content-type', mimetype);						
          var filestream = fs.createReadStream(cpath);						
          filestream.pipe(res);
          return;
        }
        res.end(JSON.stringify({"error" : "File does not exist."}),"UTF-8"); 		
        return;
      break;
      }
       case 'read':{		
        var target=req.query.target;
        var cpath=getpath(req,target);
        if(cpath==''){
          res.end(JSON.stringify({"error" : "Path does not exist."}),"UTF-8"); 		
          return;
        }					
        result.content=fs.readFileSync(cpath);
      break;
      }
       case 'edit':{// to debug
        var target=req.query.target;
        var cpath=getpath(req,target);
        if(cpath==''){
          res.end(JSON.stringify({"error" : "Path does not exist."}),"UTF-8"); 		
          return;
        }					
        fs.writeFileSync(cpath,req.query.content);
        result.file=get_fd_desc(req,cpath);
      break;
      }
       case 'paste':{
        var targets=req.query.targets;
        var dst=req.query.dst;
        var dstpath=getpath(req,dst);
        if(dstpath==''){
          res.end(JSON.stringify({"error" : "Path does not exist."}),"UTF-8"); 		
          return;
        }
        result.removed=new Array();
        for(var i in targets){
          fhesh=targets[i];
          var cpath=getpath(req,fhesh);
          if(cpath==''){
            res.end(JSON.stringify({"error" : "Path does not exist."}),"UTF-8"); 		
            return;
          }						
          result.removed=new Array();
          result.added=new Array();
          result=fm_rec_copy(req, cpath,dstpath,req.query.cut=='1', result);
        }
      break;
      }
       default:{
        calipso.log("default!");
        break;
       }
    }
  } catch (e) {			
    console.log(e);
    res.end(JSON.stringify({"error" : e}),"UTF-8"); 
    return;		
  }	
  console.log('I am here end');
  res.end(JSON.stringify(result),"UTF-8"); 
});

};

function dirExistsSync(d) {
  try {
    return fs.statSync(d).isDirectory()
  } catch (er) { return false } 
}

function fm_rec_copy(req,cpath, dst, cut, result){
  var files = new Array();
  var stats=fs.statSync(cpath);
  var fname=cpath.slice(cpath.lastIndexOf(dir_separator)+1);
  
  if(stats.isFile()){
    var dst_name=dst+dir_separator+fname;
    if(cut){
      var fhash=gethash(req,cpath);
      result.removed[result.removed.length]=fhash;
  //			fs.unlinkSync(cpath);
      fs.renameSync(cpath, dst_name);
    } else{
      fs.createReadStream(cpath).pipe(fs.createWriteStream(dst_name));		
    }

    result.added[result.added.length]=get_fd_desc(req,dst_name);
  } else {
    var dst_child_path=dst+dir_separator+fname;
    if(!dirExistsSync(dst_child_path)){
      fs.mkdirSync(dst_child_path, '0755');
      result.added[result.added.length]=get_fd_desc(req,dst_child_path);			
    }
    
    console.log('readdir 4');
    var readresults = fs.readdirSync(cpath);
    console.log('readdir end 4');		
    for(var i in readresults) {
      var cname=cpath+dir_separator+readresults[i];	
      result=fm_rec_copy(cname,dst_child_path,cut,result);
    }

    if(cut){
      var fhash=gethash(req,cpath);
      result.removed[result.removed.length]=fhash;			
      fs.rmdirSync(cpath);
    }
  }
  return result;
}

function fm_rec_delete(req,cpath){
  var files = new Array();
  var stats=fs.statSync(cpath);
  files[files.length]=gethash(req,cpath);

  if(stats.isFile()){
    fs.unlinkSync(cpath);
  } else {
    console.log('readdir 5');
    var readresults = fs.readdirSync(cpath);
    console.log('readdir end 5');		
    for(var i in readresults) {
      var cname=cpath+dir_separator+readresults[i];			
      var stats=fs.statSync(cname);

      if (stats.isDirectory()){
        var res=fm_rec_delete(cname);
        var files = files.concat(res);
      }
      		  		
      if (stats.isFile()){
        fs.unlinkSync(cname);
        var fhash=gethash(req,cname);
        files[files.length]=fhash;				
      }
    }

    fs.rmdirSync(cpath);
  }

  return files;
}

function fm_upload(req, res, template, block, next) {
  res.format = "json";
  if(req.session.user===undefined || req.session.user==null){
    res.end(JSON.stringify({"error" : "You should log in."}),"UTF-8"); 
    return;
  }

  calipso.modules.postgres.fn.init_gis_user(req, function(){
    try{
      if (req.body.cmd=='upload') {
        fhesh=req.body.target;
        cpath=getpath(req,fhesh);
        if(dirExistsSync(cpath)){
          console.log(req.files);
          //for(var i in req.files.upload) {						
            // || req.files.upload[i].filename=='undefined')
            
            console.log(cpath+dir_separator+req.files.files.name);
            if (fs.existsSync(req.files.files.path) && req.files.files.name!=undefined) {
              // Do something
              fs.createReadStream(req.files.files.path).pipe(fs.createWriteStream(cpath+dir_separator+req.files.files.name));										
            }
          //}
          var result = new Object();
          result.added=GetTree(req,cpath,0,0,1,0);
          res.end(JSON.stringify(result),"UTF-8"); 
        }
        else
        {
          res.end(JSON.stringify({status:'error'}),"UTF-8"); 
        }
        return;
      }
    } catch (e) {
      console.log(e);
      res.end(JSON.stringify({"error" : e}),"UTF-8"); 
      return;		
    }
  });

};

export default {
  getpath,
  getpath_on_userdir
}