var fs = require('fs'); //File system
var crypto = require('crypto');
var sax = require('xmldom').DOMParser;  //XML parser
var execFile = require('child_process').execFile;
//var fileName="fzt.xml";

//������ �����
function readFile(fileName){
	str=fs.readFileSync(fileName, 'utf8');
	return str;
	}

//������ �����
function saveFile(fileName, data){
	res=-1;
	outp=fs.createWriteStream(fileName, { flags: 'w' });
	outp.write(data);
	outp.end();
	return res;
}
	
//�������� ������������� ������������
function checkUser(doc, userName, nodeName){
	nodeName = typeof nodeName !== 'undefined' ? nodeName : 'User';
	var users = doc.getElementsByTagName(nodeName);
	var reccnt=users.length;
	for (var i=0; i<reccnt; i++){
		var u=users[i];
		if (u.getAttribute('Name')==userName){
			return 1;
			}
		}
	return 0;
	}

//������������ ����
function clonishNode(oldNode, newName){
	try{
		var newNode=oldNode.ownerDocument.createElement(newName);
		var cnt1=oldNode.attributes.length;
	
		for (var i=1; i<cnt1; i++){
			var cn=oldNode.attributes[i].cloneNode(true);
			newNode.appendChild(cn);
			}
		var cnt2 = oldNode.childNodes.length;
		for (var i=0; i<cnt2; i++){
			newNode.appendChild(oldNode.childNodes[i].cloneNode(true));
			}
		return newNode;

	}
	catch (err){
		console.log("clonish err "+ err);
	}
}
function delUser(doc, userName, fileName){
	var res=-1;
	try{
		var users=doc.getElementsByTagName('User');
		var c=users.length;
		for(var i=0; i<c; i++){
			if (users[i].getAttribute("Name")==userName){
				users[i].parentNode.removeChild(users[i]);
				f=fs.writeFile(fileName, doc, function (err) {
					if (err) throw err;
					console.log('It\'s saved!');
				});				
				res=0;
				break;
			}
		}
	}
	catch(err){
		console.log("del err");
	}
	finally {
		return res;
	}
}
/*
userName - ��� ������������
ot - ��� �������� ( 1 -�������� ������������� ������������	
			2 -���������� ������������
			3 - �������� ������������
			4 - �������� ������ �����������
			)
group - ������ �������������
fileName - ������������ ����� �������� FileZilla
pswd - ������ ������������
userFolder - ����� ������������
*/
exports.fzUsers = function (userName, ot, group, fileName, pswd, userFolder){
	if (userName ==""){
		x={"op":-1, "ok":1, "user":'', "ans":"user is unknown"};
		return x;
	}
	var str=fs.readFileSync(fileName, 'utf8');
	var newNode;
	pswd = typeof pswd !== 'undefined' ? pswd : '';
	userFolder = typeof userFolder !== 'undefined' ? userFolder : '';
	try{

		//����� �������� �������� � ������
		var doc = new sax().parseFromString(str); //�������� XML ��������
		doc.formatOutput = 1;
		doc.preserveWhiteSpace = 1;
		ue=checkUser(doc, userName);
		switch(ot){
			case 1:
				x= {"op": 1, "ok":0, "user":userName, "ans":ue };
				//console.log(res_callback);
				//res_callback(x); 
				break;
			case 2:
				x={"op": 2, "ok":1, "user":userName, "ans":"error"};
				if (ue==0){ //������������ ���
					var gr=doc.getElementsByTagName('Group');
					var users=doc.getElementsByTagName('Users').item(0);
					var grl=gr.length;
					for(var i=0; i<grl; i++){
						if  (gr[i].getAttribute("Name")==group){ //������ ������������
							//��������� �����
							var newUsr = gr[i].cloneNode(true);
							newUsr=clonishNode(newUsr, "User");
							newUsr.setAttribute('Name', userName);
							//���������� ����� ������
							newNode = doc.createElement("Option");
							if (pswd==''){
								x={"op":2, "ok":1, "user":userName, "ans":"password is required"};
								break;
								}
							var md5 = crypto.createHash('md5').update(pswd).digest("hex");
							newNode.setAttribute('Name', "Pass");
							newNode.textContent=md5; //���� ������
							newUsr.appendChild(newNode); //Password data
							newNode = doc.createElement("Option");
							newNode.textContent=group;
							newNode.setAttribute('Name', "Group");
							newUsr.appendChild(newNode); //Group data
							newNode = doc.createElement("Option");
							newNode.textContent=1;
							newNode.setAttribute('Name', "Enabled");
							newUsr.appendChild(newNode); //Enabled data	
							//������� ��� ������ ������������
							var folder=newUsr.getElementsByTagName("Permission").item(0);
							//var homeFolder=folder.getAttribute("Dir");
							if (userFolder==''){
								x={"op":2, "ok":1, "user":userName, "ans":"unknown user dir"};
								break;
							}
							//folder.setAttribute("Dir", homeFolder +"\\" +userFolder);
							folder.setAttribute("Dir", userFolder);
							users.appendChild(newUsr); //New user data								
							f=fs.writeFile(fileName, doc, function (err) {
								if (err) throw err;
								console.log('It\'s saved!');
							});
							//console.log("doc.toString()");
							x={"op":2, "ok":0, "user":userName, "ans":"user added"};
							
						}
						
					}
				}
				else
					x={"op":2, "ok":1, "user":userName, "ans":"user already exists"};
				break;
			case 3:
				x={"op":3, "ok":1, "user":userName, "ans":"some error"};
				if (ue==1){ //������������ ���������� - �������
					var res=delUser(doc, userName, fileName);
					if (res==0)
						x={"op":3, "ok":0, "user":userName, "ans":"user deleted"};
					else
						x={"op":3, "ok":1, "user":userName, "ans":"delete error"};
				}
				else
					x={"op":3, "ok":1, "user":userName, "ans":"user doesn't exists"};
				break;
			case 4:
				x={"op":4, "ok":1, "user":userName, "ans":"err"};
				var newDoc = changepswd(doc, userName, pswd);
				f=fs.writeFile(fileName, newDoc, function (err) {
					if (err) throw err;
				});
				x={"op":4, "ok":0, "user":userName, "ans":"password has been changed"};
		}
		if (x['ok']==0){
			//Path to FileZilla Server
			var f="C:\\Program Files (x86)\\FileZilla Server\\FileZilla Server.exe",
			fzkey=["/reload-config"];
			
			execFile(f, fzkey, null, function (err, stdout, stderr) {
			 if (stderr!="")
				 console.log("execFileSTDERR:", JSON.stringify(stderr))
			});
		}
			
		return x; 
		
		
		

	}
	catch (err){
		console.log("some problems " +err);
		x={"op":-1, "ok":2, "user":userName, "ans":"some problems: " +err};
		return x;
	}		
}

//Function to change user password
function changepswd(doc, user, newpswd){
	var user_node=doc.getElementsByTagName("User");
	var ul=user_node.length;
	for(var i=0; i<ul; i++){
		if (user_node[i].getAttribute("Name")==user){
			usrNode=user_node[i]; 
			var opt=usrNode.getElementsByTagName("Option");
			var ol=opt.length;
			for (var j=0; j<ol; j++) {
				if (opt[j].getAttribute("Name")=="Pass"){
					var md5 = crypto.createHash('md5').update(newpswd).digest("hex");
					opt[j].textContent=md5; //���� ������
					break;
					
				}
			}
			break;
		}
		
	}
	return doc;
	
}
