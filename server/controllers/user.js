import crypto from 'crypto';
import config from '../../config/config';
import { identifiers } from '../lib/geoThemes/system_themes_identifiers';
import { APIErrors } from '../lib/helpers'
import { hashmd5 } from './../utils/crypto';
import bldata from './../lib/geoThemes/bldata';
import nodemailer from 'nodemailer';

/**
 * Activating user account using the secret code.
 */
const activate = (req, res) => {
  const code = req.query.code.trim();

  req.user = { id: config.systemAccountId };
  req.query = { f: identifiers.USER_ACTIVATION_CODES };
  req.body = {
    s_fields: ['code', 'id', 'user_id'],
    f_code: code
  };

  bldata.list(req, res).then((result) => {
    if (result.aaData.length === 1) {
      const userId = result.aaData.pop().user_id;
      req.query = { f: identifiers.USERS };
      req.body.document = {
        f_id: userId,
        locked: "false"
      };

      bldata.update(req, res).then((result) => {
        res.send({ status: 'success' });
      }).catch((err) => {
        res.boom.badRequest(err);
      });
    } else {
      res.boom.badRequest('Invalid activation code');
    }
  }).catch((err) => {
    res.boom.badImplementation(err);
  });
}


/**
 * Resetting user password.
 * 
 * @param {*} req 
 * @param {*} res 
 */
const resetPassword = (req, res) => {
  const email = req.body.email;
  req.user = { id: config.systemAccountId };
  req.query = { f: identifiers.USERS };
  req.body = { f_email: email };
  bldata.list(req, res).then((result) => {
    if (result.aaData.length === 1) {
      const newPassword = crypto.randomBytes(12).toString('hex');
      const hashedPassword = hashmd5(newPassword, config.passwordSalt);

      const user = result.aaData.pop();
      req.body.document = {
        f_id: user.id,
        password: hashedPassword
      };

      bldata.update(req, res).then((result) => {
        let transporter = nodemailer.createTransport(config.mail.smtp);
        let mailOptions = {
          from: `"Geoportal" <${config.mail.from}>`,
          to: user.email,
          subject: `New password for ${user.username} account at Geoportal`,
          html: `<h1>Hello, ${user.fullname}</h1><p>You have recently requested the password reset, new password for <strong>${user.username}</strong> account: <span style="font-family: 'Lucida Console', Monaco, monospace">${newPassword}</span></p><p></p><p>Please reply to this message if you have any questions</p>`
        };

        transporter.sendMail(mailOptions, (error, info) => {
          if (error === null) {
            transporter.close();
            res.send({ status: 'success' })
          } else {
            res.boom.badImplementation();
          }
        });
      }).catch((err) => {
        res.boom.badImplementation();
      });
    } else {
      res.apiError(APIErrors.USER_WITH_THIS_EMAIL_NOT_FOUND);
    }
  });
};

export default { activate, resetPassword };
