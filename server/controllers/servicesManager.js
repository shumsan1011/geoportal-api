import request from 'request';
import url from 'url';
import WPSServer from '../lib/servicesManager/WPSServer';
import WPSManager from '../lib/servicesManager/WPSManager';
import WPSManagerBootstrap from '../lib/servicesManager/WPSManagerBootstrap';
import wpsrequest from '../lib/servicesManager/WPSProxy';
import db from '../lib/db';
import config from '../../config/config';
import { prepareParams, XSSRecursiveCheck } from '../lib/helpers';

let client = db.getClient();


/**
 * Proxies the request to the remote host to overcome the CORS policy.
 *
 * @param {*} req Request object
 * @param {*} res Response object
 */
function proxifier(req, res) {
  req.pipe(request(req.query.url)).pipe(res);
}


/**
 * Bridges the internal services and scenarios and external callers via WPS server interface.
 *
 * @param {*} req Request object
 * @param {*} res Response object
 */
function wpsServerInitiate(req, res) {
  let { user_id, user_name } = prepareParams(req);
  if (user_id === '') user_id = -1;

	var server     = new WPSServer(client, user_id, user_name);
	var parameters = req.url.split("?");

	if (parameters.length === 2) {
		server.serveRequest(parameters[1], res, req);
	} else {
		res.format = "json";
		res.end("{status: error, message: 'Invalid request was provided, no parameters in GET request were provided'}");
		log("Invalid request was provided, no parameters in GET request were provided");
	}
}


/**
 * Registers services and scenarios in the catalog.
 *
 * @param {*} req Request object
 * @param {*} res Response object
 */
function createWpsManager(req, res) {
  let { user_id } = prepareParams(req);

  var manager = new WPSManager(req, client);
  var obj = XSSRecursiveCheck(JSON.parse(req.body.data));

  var status = false;
  if (obj.status === "true") {
    status = true;
  }

  var extraparams = new Object();
  if (obj.type === "js") {
    extraparams.funcbody = obj.funcbody;
  } else if (obj.type === "wps") {
    extraparams.wpsservers = obj.servers;
  } else {
    throw new Error("Wrong type value: " + obj.type);
  }

  if ("previd" in obj) {
    var serviceid = parseInt(obj.previd);
    manager.updateService(() => {
      res.send({status: 'success', id: serviceid});
    }, obj.name, obj.description, obj.map_reduce_specification, obj.type, obj.params, obj.output_params, obj.method, status, extraparams, serviceid);
  } else {
    var serviceid = parseInt(obj.previd);
    manager.registerService(receivedid => {
      res.send({
        status: 'success',
        id: receivedid
      });
    }, obj.name, obj.description, obj.map_reduce_specification, obj.type, obj.params, obj.output_params, user_id, obj.method, status, extraparams);
  }
}


/**
 * Executes the service or scenario.
 *
 * @param {*} req Request object
 * @param {*} res Response object
 */
function execute(req, res) {
  let { user_id, user_name } = prepareParams(req);
  let _url = url.parse(req.url, true);

  /*
  Database client specification
  calipso.modules.postgres.fn.init_gis_user(req, function() {
    ..
  }
  */

  var manager = new WPSManager(req, client);

  var id           = parseInt(_url.query['id']);
  var inputparams  = _url.query['inputparams'];
  var outputparams = _url.query['outputparams'];

  var userinputparameters, useroutputparameters;
  try {
    userinputparameters  = XSSRecursiveCheck(JSON.parse(inputparams));
    useroutputparameters = XSSRecursiveCheck(JSON.parse(outputparams));
  } catch(e) {
    throw new Error("Error while parsing input JSON parameter");
  }

  manager.executeService(methodexampleid => {
    res.send({status: 'executing', data: methodexampleid});
  }, (message) => {
    res.send({status: 'error', error: "Application crushed", message});
  }, id, user_id, user_name, userinputparameters, useroutputparameters, config.modules.servicesManager);
}


/**
 * Returns wrappers for existing services and scenarios.
 *
 * @param {*} req Request object
 * @param {*} res Response object
 */
function wrappers(req, res) {
  let { user_name } = prepareParams(req);
  let _url = url.parse(req.url, true);

  /*
  Database client specification
  calipso.modules.postgres.fn.init_gis_user(req, function() {
    ..
  }
  */

  var manager = new WPSManager(req, client);
  var excludeid = parseInt(_url.query['excludeid']);
  manager.getWrappers(function(rows) {
    res.send({status: 'success', data: rows});
  }, excludeid);
}


/**
 * Returns console output for instance of service or scenario.
 *
 * @param {*} req Request object
 * @param {*} res Response object
 */
function consoleOutput(req, res) {
  let { user_name } = prepareParams(req);
  let _url = url.parse(req.url, true);

  /*
  Database client specification
  calipso.modules.postgres.fn.init_gis_user(req, function() {
    ..
  }
  */

  var manager = new WPSManager(req, client);
  var id = parseInt(_url.query['id']);
  manager.getConsoleOutput((data) => {
    res.send({status: 'success', data: data});
  }, id);
}


/**
 * Returns description of the service or scenario.
 *
 * @param {*} req Request object
 * @param {*} res Response object
 */
function getinfoWpsManager(req, res) {
	var _url = url.parse(req.url, true);
	var id  = parseInt(_url.query['id']);

  var query = "SELECT * FROM wps_methods WHERE id = " + id + ";";
  var cquery = client.query(query, function(error, rows, fields) {
    var obj = rows.rows[0];
    if (error === null && rows.rows.length === 1) {
      obj.params        = JSON.parse(obj.params);
      obj.output_params = JSON.parse(obj.output_params);
      obj.wpsservers    = JSON.parse(obj.wpsservers);
      res.send({status: 'success', data: obj});
    } else {
      res.send({status: 'error'});
    }
  });
}

export default {
  proxifier,
  wpsServerInitiate,
  createWpsManager,
  execute,
  wrappers,
  consoleOutput,
  getinfoWpsManager
};
