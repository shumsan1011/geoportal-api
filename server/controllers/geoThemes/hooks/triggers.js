const allowedTriggers = {
	BEFORE_CREATE: 0,
	AFTER_CREATE: 1,
	BEFORE_UPDATE: 2,
	AFTER_UPDATE: 3,
	BEFORE_DELETE: 4,
	AFTER_DELETE: 5
};

export default allowedTriggers;