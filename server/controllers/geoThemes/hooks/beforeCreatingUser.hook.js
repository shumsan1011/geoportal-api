import allowedTriggers from './triggers';
import { hashmd5 } from './../../../utils/crypto';
import config from '../../../../config/config';

/**
 * Hashing the password with salt
 */
const hookDescription = {
  tableId: 5,
  trgger: allowedTriggers.BEFORE_CREATE,
  hook: (req, res) => {
    let result = new Promise((resolve, reject) => {
      req.body.document.password = hashmd5(req.body.document.password, config.passwordSalt);
      resolve({ req, res });
    });

    return result;
  }
};

export default hookDescription;