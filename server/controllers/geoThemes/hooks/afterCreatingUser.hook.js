import uuid from 'uuid';
import nodemailer from 'nodemailer';
import allowedTriggers from './triggers';
import { identifiers } from '../../../lib/geoThemes/system_themes_identifiers';
import bldata from './../../../lib/geoThemes/bldata';
import config from '../../../../config/config';
import { log } from '../../../lib/helpers';


/**
 * Setting "locked" of freshly created user to true, creating
 * the activation code and sending an actication email
 */
const hookDescription = {
  tableId: identifiers.USERS,
  trgger: allowedTriggers.AFTER_CREATE,
  hook: (req, res, userId) => {
    let result = new Promise((resolve, reject) => {
      const userEmail = req.body.document.email;
      const userFullName = req.body.document.fullname;
      const userName = req.body.document.username;

      // Setting "locked" to true
      req.user = { id: config.systemAccountId };
      req.body.document = {
        f_id: userId,
        locked: "true"
      };

      bldata.update(req, res).then(() => {
        // Creating the activation code record
        const code = uuid.v1();
        req.query.f = identifiers.USER_ACTIVATION_CODES;
        req.body.document = { user_id: '' + userId, code };

        // @todo Delete all previous activation codes for this user

        bldata.add(req, res).then(() => {
          let transporter = nodemailer.createTransport(config.mail.smtp);
          let mailOptions = {
            from: `"Geoportal" <${config.mail.from}>`,
            to: userEmail,
            subject: `Activate ${userName} account at Geoportal`,
            html: `<h1>Hello, ${userFullName}</h1><p>You have recently registered at Geoportal as <strong>${userName}</strong> using this email address.</p><p>Please, follow the link below to activate your account <a href="${config.primaryClientURL}/activate-account/${code}" title="Activate your account at Geoportal">${config.primaryClientURL}/activate-account/${code}</a></p><p></p><p>Please reply to this message if you have any questions</p>`
          };

          transporter.sendMail(mailOptions, (error, info) => {
            if (error === null) {
              log('Email was sent');
              transporter.close();
              resolve({ req, res, data: userId });
            } else {
              reject(error);
            }
          });
        }).catch(error => {
          reject(error);
        });
      }).catch(error => {
        reject(error);
      });
    });

    return result;
  }
};

export default hookDescription;