import { identifiersWithTableNames } from '../../../lib/geoThemes/system_themes_identifiers';
import allowedTriggers from './triggers';

import beforeCreatingUser from './beforeCreatingUser.hook';
import afterCreatingUser from './afterCreatingUser.hook';

const hooks = [beforeCreatingUser, afterCreatingUser];

/**
 * Appying hooks to regular data manipulations
 */
class HookManager {
	hooks = [];

	constructor(){}
 
	register(datasetId, trigger, hook) {
		console.log('Registering hook', datasetId, trigger);

		let theDatasetBelongsToSystemOnes = false;
		identifiersWithTableNames.map(item => {
			if (item.id === datasetId) {
				theDatasetBelongsToSystemOnes = true;
				return false;
			}
		});

		if (!theDatasetBelongsToSystemOnes) throw 'Hooks can be created only for system datasets';
		let triggerExists = false;
		for (let key in allowedTriggers) {
			if (allowedTriggers[key] === trigger) {
				triggerExists = true;
				break;
			}
		}

		if (!triggerExists) throw 'Invalid trigger was provided';

		this.hooks.push({ datasetId, trigger, hook });
	}

	/**
	 * Applying eligible hooks for the specific action
	 * 
	 * @param {*} datasetId Internal system identifier of the table
	 * @param {*} trigger Hook trigger
	 * @param {*} req Request object
	 * @param {*} res Response object
	 * @param {*} data Record data (optional)
	 */
	apply(datasetId, trigger, req, res, data) {
		if (!Number.isInteger(datasetId)) {
			throw 'Dataset identifier should be an integer';
		}
		
		let eligibleHooks = [];
		this.hooks.map(item => {
			if (item.datasetId === datasetId && item.trigger === trigger) {
				eligibleHooks.push(item);
			}
		});

		/**
		 * @todo Process more than one eligible hook
		 */

		if (eligibleHooks.length > 0) {
			console.log('Using first encountered eligible hook');
			return eligibleHooks.pop().hook(req, res, data);
		} else {
			console.log('No eligible hooks');
			let result = new Promise((resolve, reject) => {
				resolve({ req, res, data });
			});
	
			return result;
		}
	}
}

let hookManager = new HookManager();
hooks.map(item => {
	hookManager.register(item.tableId, item.trgger, item.hook);
});

export default {
	hooks: hookManager,
	triggers: allowedTriggers
};