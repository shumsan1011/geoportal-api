import request from 'request';
import fs from 'fs';
import url from 'url';
import config from '../../../config/config';
import { meta_manager } from '../../lib/geoThemes/geo_meta_table';
import geo_table from '../../lib/geoThemes/geo_table';
import get_table_json from '../../lib/geoThemes/get_table_json';
import { getpath_on_userdir, getpath } from '../../lib/rfm';
import bldata from '../../lib/geoThemes/bldata';
import {
  APIErrors,
  prepareParams,
  _qv,
  log,
  isNumeric,
  is_number,
  getcolumn,
  hexToRGB,
  explode
} from '../../lib/helpers';
import db from '../../lib/db';
import dbErrorCodes from '../../lib/db/errorCodes';

import m_date from '../../lib/geoThemes/date';
import m_geom from '../../lib/geoThemes/geom';
import { geo_map, toMapserverPath } from '../../lib/geoThemes/geo_map';

const supportedExtensions = ['tab', 'shp'];
const client = db.getClient();

/**
 * Error callback, processes regular error messages and translates the
 * PostgreSQL errors to application-specific format
 *
 * @param {Object} error Error object
 * @param {Object} res Response object
 */
const errorCallback = (error, res) => {
  if (error && error.code) {
    let errorId = false;
    for (let index in dbErrorCodes) {
      if (error.code === dbErrorCodes[index].literal) {
        errorId = dbErrorCodes[index].symbol;
      }
    }

    let errorDescription = { dbErrorId: errorId };
    if (errorId === 'unique_violation') {
      let regex = /\((.*?)\)/;
      let matched = regex.exec(error.detail);
      errorDescription.field = matched[1];
    }

    res.apiError(APIErrors.COLUMN_VALUE_ALREADY_EXISTS, errorDescription);
  } else if (error && error.APIErrorCode) {
    switch (error.APIErrorCode) {
      case APIErrors.FORBIDDEN:
        res.boom.forbidden();
        break;
      default:
        res.boom.badRequest();
        break;
    }
  } else {
    res.boom.badRequest(error);
  }
};

/**
 * Listing data from the specific table.
 *
 * @param {*} req Request object
 * @param {*} res Response object
 */
function bldata_list(req, res) {
  let { qparams } = prepareParams(req);
  bldata
    .list(req, req)
    .then(data => {
      if (_qv(qparams, 'to_file') == 'true') {
        res.setHeader('Content-disposition', 'attachment; filename=table.txt');
        res.setHeader('Content-type', 'text/plain');
        res.send(data, 'UTF-8');
      } else {
        res.send(data);
      }
    })
    .catch(message => {
      errorCallback(message, res);
    });
}

/**
 * Adding items to the data table.
 *
 * @param {*} req Request object
 * @param {*} res Response object
 */
function bldata_add(req, res) {
  // Temporary fix as it is a bad thing to send JSON with encoded JSON in it
  if (req.body && req.body.document && typeof req.body.document === 'string') {
    req.body.document = JSON.parse(req.body.document);
  }

  bldata
    .add(req, req)
    .then(data => {
      res.send({
        status: 'success',
        id: data
      });
    })
    .catch(message => {
      errorCallback(message, res);
    });
}

/**
 * Updating items in the data table.
 *
 * @param {*} req Request object
 * @param {*} res Response object
 */
function bldata_update(req, res) {
  // Temporary fix as it is a bad thing to send JSON with encoded JSON in it
  if (req.body && req.body.document && typeof req.body.document === 'string') {
    req.body.document = JSON.parse(req.body.document);
  }

  bldata
    .update(req, res)
    .then(() => {
      res.send({ status: 'success' });
    })
    .catch(({ message, res }) => {
      errorCallback(message, res);
    });
}

/**
 * Deleting items from the data table.
 *
 * @param {*} req Request object
 * @param {*} res Response object
 */
function bldata_delete(req, res) {
  bldata
    .delete(req, req)
    .then(() => {
      res.send({ status: 'success' });
    })
    .catch(message => {
      errorCallback(message, res);
    });
}

/**
 * Dataset to file conversion
 *
 * @param {*} req Request object
 * @param {*} res Response object
 */
function bl_file(req, res) {
  bldata.file(req, res);
}

/**
 * Clears specified table.
 *
 * @param {*} req Request object
 * @param {*} res Response object
 */
function bldata_clearTable(req, res) {
  let { dataset_id, user_id } = prepareParams(req);

  if (user_id === '') {
    res.boom.forbidden();
  } else {
    meta_manager.curent_user_id = user_id;
    meta_manager.clearTable(dataset_id, error => {
      if (error) {
        res.boom.badRequest(error);
      } else {
        res.send({
          status: 'success',
          message: 'The table is empty'
        });
      }
    });
  }
}

/**
 * Updating theme.
 *
 * @param {*} req Request object
 * @param {*} res Response object
 */
function processTableJson(req, res) {
  if (req.body.tablejson === undefined) {
    res.boom.badRequest('Table JSON is not defined');
  } else {
    let { user_id } = prepareParams(req);

    var strtablejson = req.body.tablejson;
    var tablejson = JSON.parse(strtablejson);

    var dataset_id = -1;
    if (tablejson.dataset_id != undefined) {
      dataset_id = tablejson.dataset_id;
    }

    if (user_id === '') {
      res.boom.forbidden();
    } else {
      meta_manager.curent_user_id = user_id;
      meta_manager.processTableJson(tablejson, dataset_id, new_dataset_id => {
        if (new_dataset_id != -1) {
          res.send({
            data: new_dataset_id,
            status: 'success'
          });
        } else {
          res.boom.badRequest();
        }
      });
    }
  }
}

/**
 * Getting list of tables.
 *
 * @param {*} req Request object
 * @param {*} res Response object
 */
function gettablesGeothemes(req, res) {
  var additionalwhereclause = '';
  var sql = `select table_schema, table_name from information_schema.tables where table_schema<>\'pg_catalog\' and table_schema<>\'information_schema\' and
    has_table_privilege(quote_ident(table_schema) || \'.\' || quote_ident(table_name), \'SELECT\') ${additionalwhereclause} ORDER BY table_schema, table_name;`;
  var query = client.query(sql, function(error, unorows, unofields) {
    if (error === null) {
      var output = new Array();
      for (var i = 0; i < unorows.rows.length; i++) {
        output.push({
          name: unorows.rows[i].table_schema + '.' + unorows.rows[i].table_name
        });
      }

      res.send(output);
    } else {
      res.send(error);
    }
  });
} //end gettablesGeothemes()

/**
 * Drops table.
 *
 * @param {*} req Request object
 * @param {*} res Response object
 */
function droptable(req, res) {
  let { user_id, qparams } = prepareParams(req);
  var dataset_id = _qv(qparams, 'dataset_id');
  if (user_id === '') {
    res.boom.forbidden();
  } else {
    meta_manager.dropTable(dataset_id, function(error) {
      if (error) {
        res.boom.badRequest();
      } else {
        res.send({
          status: 'success',
          data: 'The table was deleted'
        });
      }
    });
  }
}

/**
 * Data import.
 *
 * @param {*} req Request object
 * @param {*} res Response object
 */
function bldata_import(req, res) {
  let { dataset_id, user_id, qparams } = prepareParams(req);
  const dataset_import_id = req.query.import_f;
  if (user_id === '') {
    res.boom.forbidden();
  } else {
    var first_is_head = 'first_is_head' in qparams;

    var delimeter = _qv(qparams, 'delimeter');
    if (delimeter == '' || delimeter == 't') delimeter = '\t';

    var encoding = _qv(qparams, 'encoding');
    if (encoding == '') encoding = 'CP1251';

    var mainrootdir = config.modules.rfm.mainrootdir;
    var file_path = getpath_on_userdir(mainrootdir, dataset_import_id);

    if (is_number(dataset_id)) {
      user_dataset_access(
        req,
        dataset_id,
        alist => {
          get_table_json(req, dataset_id, mdobj => {
            if (mdobj == null) {
              res.boom.badRequest('Empty metadata');
            }

            log('mdobj.dataset_id=' + mdobj.dataset_id);

            var g_table = new geo_table(
              user_id,
              mdobj.dataset_id,
              meta_manager,
              mdobj
            );
            for (var i = 0; i < mdobj.columns.length; i++) {
              if (!(mdobj.columns[i].fieldname in qparams)) continue;

              if (mdobj.columns[i].widget.name == 'classify') {
                //mdobj.columns[i].g_table = new geo_table(user_id, mdobj.columns[i].widget.properties.dataset_id, meta_manager, mdobj.columns[i].widget.properties.ref_meta);
                mdobj.columns[i].g_table = new geo_table(
                  user_id,
                  mdobj.columns[i].widget.properties.dataset_id,
                  meta_manager,
                  mdobj.usingmeta[mdobj.columns[i].widget.properties.dataset_id]
                );
              }
            }

            function load_obj(loadobj, last) {
              var newobj = {};
              if (loadobj != null) {
                function handling_params(paramnum, obj, newobj) {
                  if (paramnum == mdobj.columns.length) {
                    log('INSERT LOAD');
                    log(newobj);
                    g_table.insert(newobj, function(result) {
                      return;
                    });

                    return;
                  }

                  if (mdobj.columns[paramnum].fieldname in qparams) {
                    var method = qparams[mdobj.columns[paramnum].fieldname];
                    switch (method.name) {
                      case 'copy':
                        if (
                          method.prm !== undefined &&
                          method.prm.length == 1 &&
                          method.prm[0].value != undefined &&
                          method.prm[0].value != ''
                        ) {
                          cpy_fld = method.prm[0].value;

                          if (
                            cpy_fld in obj &&
                            obj[cpy_fld] !== undefined &&
                            obj[cpy_fld] != ''
                          ) {
                            if (
                              mdobj.columns[paramnum].widget.name == 'classify'
                            ) {
                              subqparams = {};
                              var props =
                                mdobj.columns[paramnum].widget.properties;

                              if (
                                props != undefined &&
                                props.db_field != null &&
                                props.db_field != undefined
                              ) {
                                subqparams['f_' + props.db_field[0]] =
                                  obj[cpy_fld];
                                sub_sql = mdobj.columns[
                                  paramnum
                                ].g_table.buildquery(['id'], subqparams);
                                var subquery = client.query(sub_sql, function(
                                  error,
                                  query_result,
                                  unofields2
                                ) {
                                  log('run sql end');

                                  if (error === null) {
                                    if (query_result.rows.length == 1) {
                                      newobj[
                                        mdobj.columns[paramnum].fieldname
                                      ] =
                                        query_result.rows[0]['id'];
                                    } else
                                      log(
                                        'ambiguous decision for value=' +
                                          obj[cpy_fld]
                                      );

                                    handling_params(paramnum + 1, obj, newobj);
                                    return;
                                  } else {
                                    log(sub_sql);
                                    log(error);
                                    return;
                                  }
                                });

                                return;
                              }
                            } else if (
                              mdobj.columns[paramnum].widget.name == 'number'
                            ) {
                              v = obj[cpy_fld];
                              log('v=');
                              log(v);
                              log('name=' + mdobj.columns[paramnum].fieldname);

                              if (v !== undefined && typeof v === 'string') {
                                v = v.replace(/,/g, '.');
                                if (!isNaN(parseFloat(v)) && isFinite(v))
                                  newobj[mdobj.columns[paramnum].fieldname] = v;
                              } else if (
                                v !== undefined &&
                                typeof v === 'number'
                              ) {
                                newobj[mdobj.columns[paramnum].fieldname] = v;
                              }
                            } else {
                              newobj[mdobj.columns[paramnum].fieldname] =
                                obj[cpy_fld];
                            }
                          }
                        }

                        break;
                      case 'copy_pnt':
                        if (
                          method.prm !== undefined &&
                          method.prm.length == 2 &&
                          method.prm[0].value != undefined &&
                          method.prm[1].value != undefined &&
                          method.prm[0].value != '' &&
                          method.prm[1].value != ''
                        ) {
                          cpy_fld = method.prm[0].value;
                          cpy_fld2 = method.prm[1].value;
                          var lat = '',
                            lon = '';
                          if (
                            cpy_fld in obj &&
                            obj[cpy_fld] !== undefined &&
                            obj[cpy_fld] != ''
                          ) {
                            lat = m_geom.parseCoordinate(obj[cpy_fld]);
                          }
                          if (
                            cpy_fld2 in obj &&
                            obj[cpy_fld2] !== undefined &&
                            obj[cpy_fld2] != ''
                          ) {
                            lon = m_geom.parseCoordinate(obj[cpy_fld2]);
                          }
                          if (lat != '' && lon != '') {
                            newobj[mdobj.columns[paramnum].fieldname] =
                              'MULTIPOINT(' + lat + ' ' + lon + ')';
                          }
                        }

                        break;
                      case 'copy_date':
                        if (
                          method.prm !== undefined &&
                          method.prm.length == 2 &&
                          method.prm[0].value != undefined &&
                          method.prm[1].value != undefined &&
                          method.prm[0].value != '' &&
                          method.prm[1].value != ''
                        ) {
                          cpy_fld = method.prm[0].value;
                          frm = method.prm[1].value;
                          d = m_date.getDateFromFormat(obj[cpy_fld], frm);
                          if (d != 0) {
                            dv = new Date(d);
                            newobj[
                              mdobj.columns[paramnum].fieldname
                            ] = m_date.formatDate(dv, 'yyyy-MM-dd HH:mm:ss');
                          }
                        }

                        break;
                      case 'const':
                        if (
                          method.prm !== undefined &&
                          method.prm.length == 1 &&
                          method.prm[0].value != undefined &&
                          method.prm[0].value != ''
                        ) {
                          v = method.prm[0].value;
                          if (v != '') {
                            newobj[mdobj.columns[paramnum].fieldname] = v;
                          }
                        }

                        break;
                    }
                  }

                  handling_params(paramnum + 1, obj, newobj);
                }

                handling_params(0, loadobj, newobj);
              } else if (!last) {
                res.end(
                  JSON.stringify({
                    status: 'error',
                    data: 'The file is missing.'
                  }),
                  'UTF-8'
                );
              }

              if (last) {
                output = {
                  status: 'ok',
                  data: 'ok'
                };

                res.end(JSON.stringify(output), 'UTF-8');
                return false;
              }

              return true;
            }

            ext = file_path
              .substr(file_path.lastIndexOf('.') + 1)
              .toLowerCase();
            if (ext == 'csv' || ext == 'txt') {
              filedata_load(
                file_path,
                encoding,
                first_is_head,
                delimeter,
                load_obj
              );
            } else if (supportedExtensions.indexOf(ext) != -1) {
              new_file_path =
                file_path.substr(0, file_path.lastIndexOf('.')) + '.json';
              jsondata_load(new_file_path, encoding, load_obj);
            }
          });
        },
        message => {
          if (typeof message === 'undefined') message = 'No access';
          res.end(JSON.stringify({ status: 'error', data: message }), 'UTF-8');
        }
      );
    } else {
      res.end(
        JSON.stringify({
          status: 'error',
          data: 'The paramenter f is missing.'
        }),
        'UTF-8'
      );
    }
  }
}

/**
 * Generates the meta-description for the specific table.
 *
 * @param {*} req Request object
 * @param {*} res Response object
 */
const genJSON = (req, res) => {
  var table = req.query.tablename;

  let { user_id } = prepareParams(req);
  meta_manager.curent_user_id = user_id;
  meta_manager.getTableJSON(table, tableobject => {
    res.send(tableobject);
  });
};

function filedata_load(
  file_path,
  encoding,
  first_is_head,
  delimeter,
  callback
) {
  var columns = [];
  var translator;
  if (fs.existsSync(file_path)) {
    if (encoding != 'UTF-8') {
      var Iconv = require('iconv').Iconv;
      translator = new Iconv(encoding, 'UTF-8');
    }

    var line_cnt = 0;
    function func(data, last) {
      if (data != '') {
        if (line_cnt == 0) {
          if (first_is_head) {
            let tmp = smartsplit(data, delimeter);
            for (var i = 0; i < tmp.length; i++) {
              let v = tmp[i];
              if (
                v !== undefined &&
                v.length > 1 &&
                ((v.charAt(0) == '"' && v.charAt(v.length - 1) == '"') ||
                  (v.charAt(0) == "'" && v.charAt(v.length - 1) == "'"))
              )
                v = v.substring(1, v.length - 1);
              columns.push(v);
            }
          } else {
            let tmp = smartsplit(data, delimeter);
            for (var i = 1; i <= tmp.length; i++) {
              columns.push('column' + i);
            }
          }
          line_cnt++;
          return true;
        }
        line_cnt++;

        let values = smartsplit(data, delimeter);
        var row = {};
        for (var j = 0; j < values.length; j++) {
          if (columns.length == j) columns.push('column' + (j + 1));
          let v = values[j];
          if (
            v !== undefined &&
            v.length > 1 &&
            ((v.charAt(0) == '"' && v.charAt(v.length - 1) == '"') ||
              (v.charAt(0) == "'" && v.charAt(v.length - 1) == "'"))
          )
            v = v.substring(1, v.length - 1);
          row[columns[j]] = v;
        }

        return callback(row, last);
      }

      if (last) {
        return callback(null, last);
      }
      return true;
    }
    var input = fs.createReadStream(file_path);
    readLines(input, func, translator, delimeter);
  } else {
    throw `File ${file_path} does not exists`;
    callback(null, true);
  }
}

function indexOfEnd(remaining, delimeter) {
  var quote_start = false;
  var quote = '';
  var i = 0;
  var prevsign = '';
  while (i < remaining.length) {
    var sign = remaining.charAt(i);
    var nextsign = '';
    if (i + 1 < remaining.length) nextsign = remaining.charAt(i + 1);
    if ((sign == "'" || sign == '"') && nextsign == sign) {
      i = i + 2;
      continue;
    }
    if (
      !quote_start &&
      (sign == "'" || sign == '"') &&
      (prevsign == delimeter || i == 0)
    ) {
      quote_start = true;
      quote = sign;
    } else if (
      quote_start &&
      sign == quote &&
      (nextsign == delimeter || nextsign.charCodeAt(0) == 13)
    ) {
      quote_start = false;
    } else if (sign == '\n' && !quote_start) return i;
    prevsign = sign;
    i++;
  }
  return -1;
}

function smartsplit(data, delimeter) {
  var res = [];
  var quote_start = false;
  var quote = '';
  var prevsign = '';
  var value = '';
  var i = 0;
  while (i < data.length) {
    var sign = data.charAt(i);
    var nextsign = '';
    if (i + 1 < data.length) nextsign = data.charAt(i + 1);
    if ((sign == "'" || sign == '"') && nextsign == sign) {
      i = i + 2;
      value += sign + nextsign;
      continue;
    }
    if (
      !quote_start &&
      (sign == "'" || sign == '"') &&
      (prevsign == delimeter || i == 0)
    ) {
      quote_start = true;
      quote = sign;
    } else if (quote_start && sign == quote && nextsign == delimeter) {
      quote_start = false;
    } else if (sign == delimeter && !quote_start) {
      res.push(value);
      value = '';
      i++;
      continue;
    }
    prevsign = sign;
    i++;
    value += sign;
  }
  if (value != '') res.push(value);
  return res;
}

function readLines(input, func, translator, delimeter) {
  var remaining = '';
  var read_head = false;

  input.on('data', function(data) {
    if (translator) {
      try {
        var data1 = translator.convert(data).toString();
        data = data1;
      } catch (ex) {
        log(ex);
      }
    }
    remaining += data;
    //var index = remaining.indexOf('\n');
    var index = indexOfEnd(remaining, delimeter);
    while (index > -1) {
      var line = remaining.substring(0, index - 1);
      remaining = remaining.substring(index + 1);
      if (!func(line, false)) {
        input.close();
        remaining = '';
        return;
      }
      //index = remaining.indexOf('\n');
      index = indexOfEnd(remaining, delimeter);
    }
  });

  input.on('end', function() {
    //if (remaining.length > 0) {
    func(remaining, true);
    //}
  });
}

/**
 * @todo
 *
 * @param {*} req Request object
 * @param {*} res Response object
 */
const filedata_list = (req, res) => {
  let { user_id, qparams } = prepareParams(req);
  var filehesh = qparams.f;

  var first_is_head = 'first_is_head' in qparams;
  var iDisplayStart = _qv(qparams, 'iDisplayStart');
  if (iDisplayStart == '') iDisplayStart = 0;
  iDisplayStart = parseInt(iDisplayStart);
  var iDisplayLength = _qv(qparams, 'iDisplayLength');
  if (iDisplayLength == '') iDisplayLength = 1000000;
  iDisplayLength = parseInt(iDisplayLength);
  if (first_is_head) iDisplayStart = iDisplayStart + 1;
  var delimeter = _qv(qparams, 'delimeter');
  if (delimeter == '' || delimeter == 't') delimeter = '\t';
  var encoding = _qv(qparams, 'encoding');
  if (encoding == '') encoding = 'CP1251';

  var mainrootdir = config.modules.rfm.mainrootdir;
  var file_path = getpath_on_userdir(mainrootdir, filehesh);

  log('I am here=' + filehesh);
  var col_cnt = 0;
  var columns = [];
  var aaData = new Array();
  var content_str = '';
  var line_cnt = 0;

  //file = file.substr(0, file.lastIndexOf(".")) + ".htm";
  if (file_path.lastIndexOf('.') == -1) {
    res.end(
      JSON.stringify({ status: 'error', data: 'The file is missing.' }),
      'UTF-8'
    );
    return;
  }

  function send_result(obj, last) {
    if (obj != null) {
      line_cnt++;
      if (
        line_cnt >= iDisplayStart &&
        line_cnt < iDisplayStart + iDisplayLength
      ) {
        aaData.push(obj);
      }
      if (line_cnt >= iDisplayStart + iDisplayLength - 1) last = true;
    } else if (!last) {
      res.end(
        JSON.stringify({ status: 'error', data: 'The file is missing.' }),
        'UTF-8'
      );
    }
    if (last) {
      let iTotal = line_cnt;
      let iFilteredTotal = iTotal;
      let output = {
        sEcho: parseInt(_qv(qparams, 'sEcho')),
        iTotalRecords: '' + iTotal,
        iTotalDisplayRecords: '' + iFilteredTotal,
        aaData: aaData
      };
      res.send(output);
      return false;
    }
    return true;
  }

  let ext = file_path.substr(file_path.lastIndexOf('.') + 1).toLowerCase();
  if (ext == 'csv' || ext == 'txt') {
    filedata_load(file_path, encoding, first_is_head, delimeter, send_result);
  } else if (supportedExtensions.indexOf(ext) != -1) {
    var new_file_path =
      file_path.substr(0, file_path.lastIndexOf('.')) + '.json';
    var terminal = require('child_process').execFile(
      'ogr2ogr',
      ['-f', 'GeoJSON', new_file_path, file_path],
      function(error, stdout, stderr) {
        if (error) {
          log(error);
        }
        log(stdout);
      }
    );
    terminal.stdout.on('end', function(rdata) {
      log('end');
      log(rdata);
      jsondata_load(new_file_path, encoding, send_result);
    });
    terminal.on('exit', function(code) {
      if (code != 0) {
        log('Failed: ' + code);
      }
    });
  } else {
    res.end(
      JSON.stringify({ status: 'error', data: 'The file is missing.' }),
      'UTF-8'
    );
    return;
  }
};

/**
 * @todo
 *
 * @param {*} req Request object
 * @param {*} res Response object
 */
const filedata_meta = (req, res) => {
  let { dataset_id, user_id, qparams } = prepareParams(req);
  var filehesh = dataset_id;

  var mainrootdir = config.modules.rfm.mainrootdir;
  var file_path = getpath_on_userdir(mainrootdir, filehesh);

  //console.log('I am here='+filehesh);
  if (file_path.lastIndexOf('.') == -1) {
    res.send({ status: 'error', data: 'The file is missing.' });
    return;
  }

  let ext = file_path.substr(file_path.lastIndexOf('.') + 1).toLowerCase();
  var params = [file_path];
  if (ext === 'tif' || ext === 'tiff') {
    var stdout_str = '';
    var terminal = require('child_process').spawn('gdalinfo', params);

    terminal.stdout.on('data', function(rdata) {
      if (rdata) stdout_str += rdata.toString();
    });

    terminal.stdout.on('end', function(rdata) {
      if (rdata) {
        stdout_str += rdata.toString();
      }

      let xSize = false;
      let ySize = false;
      let numberOfBands = 0;
      stdout_str.split('\r\n').map(item => {
        if (item.indexOf('Size is') === 0) {
          let size = item.replace('Size is', '').split(',');
          xSize = parseInt(size[0]);
          ySize = parseInt(size[1]);
        }

        if (item.indexOf('Band') === 0) {
          numberOfBands++;
        }
      });

      if (xSize === false || ySize === false || numberOfBands === 0) {
        throw `Unable to extract parameters of the ${file_path}`;
      }

      res.send({
        status: 'ok',
        data: {
          column_count: xSize,
          row_count: ySize,
          band_count: numberOfBands
        }
      });
    });
  } else {
    res.send({ status: 'error', data: 'The file format is not supported.' });
    return;
  }
};

function readJSON(input, func, translator) {
  var remaining = '';
  var state = 0;
  var beg_str = '"features": [';
  var scb_cnt = 0;
  var obj_beg = -1;
  function extract_object() {
    var i = 0;
    while (i < remaining.length) {
      let s = remaining.charAt(i);
      if (s == '{') {
        scb_cnt++;
        if (obj_beg == -1) obj_beg = i;
      } else if (s == '}') scb_cnt--;

      if (scb_cnt == 0 && obj_beg != -1) {
        let str_obj = remaining.substring(obj_beg, i + 1);
        remaining = remaining.substring(i + 1);
        obj_beg = -1;
        i = 0;
        if (!func(str_obj, false)) input.close();
      }
      i++;
    }
  }

  input.on('data', function(data) {
    if (translator) {
      try {
        let data1 = translator.convert(data).toString();
        data = data1;
      } catch (ex) {
        log(ex);
      }
    }
    remaining += data;
    switch (state) {
      case 0: // looking object start
        var index = remaining.indexOf(beg_str);
        if (index == -1) return;
        remaining = remaining.substring(index + beg_str.length + 1);
        state = 0;
        break;
      case 1: //object extracting
        extract_object();
        if (remaining != '' && remaining.charAt(0) == ']') {
          func('', true);
          state = 2;
          return;
        }
        break;
      case 2:
        break;
    }
  });

  input.on('end', function() {
    extract_object();
    //if (remaining.length > 0) {
    func('', true);
    //}
  });
}

function jsondata_load(file_path, encoding, callback) {
  var columns = [];
  var translator;

  if (fs.existsSync(file_path)) {
    if (encoding != 'UTF-8') {
      var Iconv = require('iconv').Iconv;
      translator = new Iconv(encoding, 'UTF-8');
    }

    function whatDoesThisFunctinDo(data, last) {
      if (data != '') {
        //log('data=');
        //log(data);
        let row = JSON.parse(data);
        var cobj = {};
        for (var fieldname in row.properties) {
          cobj[fieldname] = row.properties[fieldname];
        }
        //log(GeoJSON2WKT(row.geometry));
        cobj.geom = GeoJSON2WKT(row.geometry);

        return callback(cobj, last);
      }
      if (last) {
        return callback(null, last);
      }
      return true;
    }
    var input = fs.createReadStream(file_path);
    readJSON(input, whatDoesThisFunctinDo, translator);
  } else {
    callback(null, true);
  }
}

function GeoJSON2WKT(GeoJSON) {
  if (GeoJSON == null) return null;
  switch (GeoJSON.type) {
    case 'Point':
      return (
        'MULTIPOINT((' +
        GeoJSON.coordinates[0] +
        ' ' +
        GeoJSON.coordinates[1] +
        '))'
      );
    case 'MultiPoint':
      return 'MULTIPOINT(' + getwktpointlist(GeoJSON.coordinates) + ')';
    case 'LineString':
      return 'MULTILINESTRING(' + getwktpointlist(GeoJSON.coordinates) + ')';
    case 'MultiLineString':
      return (
        'MULTILINESTRING(' + getwktpointlist_of_list(GeoJSON.coordinates) + ')'
      );
    case 'Polygon':
      return (
        'MULTIPOLYGON(' + getwktpointlist_of_list(GeoJSON.coordinates) + ')'
      );
    case 'MultiPolygon':
      var res = 'MULTIPOLYGON(';
      GeoJSON.coordinates.forEach(function(polygon, i) {
        if (i < GeoJSON.coordinates.length - 1)
          res += getwktpointlist_of_list(polygon) + ', ';
        else res += getwktpointlist_of_list(polygon) + ')';
      });
      return res;
  }
  return '';
}

/**
 * @todo
 *
 * @param {*} req Request object
 * @param {*} res Response object
 */
const filedata_info = (req, res) => {
  let { dataset_id, user_id, qparams } = prepareParams(req);
  var filehesh = dataset_id;

  var first_is_head = 'first_is_head' in qparams;
  var delimeter = _qv(qparams, 'delimeter');
  if (delimeter == '' || delimeter == 't') delimeter = '\t';

  var encoding = _qv(qparams, 'encoding');
  if (encoding == '') encoding = 'CP1251';

  var mainrootdir = config.modules.rfm.mainrootdir;
  let file_path = getpath_on_userdir(mainrootdir, filehesh);
  var short_file_path = getpath_on_userdir('', filehesh);
  log('I am here=' + filehesh);

  var content_str = '';
  if (file_path.lastIndexOf('.') == -1) {
    res.send({ status: 'error', data: 'The file is missing.' });
    return;
  }

  let replyWasSend = false;
  function send_result(obj, last) {
    if (obj != null) {
      let mcolumns = [];
      for (var fieldname in obj) {
        let field = {
          fieldname: fieldname,
          title: fieldname,
          description: fieldname,
          visible: true,
          type: 'string',
          widget: { name: 'edit', properties: { size: '20' } }
        };
        mcolumns.push(field);
      }

      let datafile = {
        dataset_id: filehesh,
        tablename: short_file_path,
        title: short_file_path,
        columns: mcolumns
      };
      if (!replyWasSend) {
        res.send(datafile);
        replyWasSend = true;
      }
    } else {
      if (!replyWasSend) {
        res.send({ status: 'error', data: 'The file is missing.' });
      }
    }

    return false;
  }

  let ext = file_path.substr(file_path.lastIndexOf('.') + 1).toLowerCase();
  if (ext == 'csv' || ext == 'txt') {
    filedata_load(file_path, encoding, first_is_head, delimeter, send_result);
  } else if (supportedExtensions.indexOf(ext) != -1) {
    var new_file_path =
      file_path.substr(0, file_path.lastIndexOf('.')) + '.json';
    var terminal = require('child_process').execFile(
      'ogr2ogr',
      ['-f', 'GeoJSON', new_file_path, file_path],
      function(error, stdout, stderr) {
        if (error) {
          log(error);
        }

        log(stdout);
      }
    );
    terminal.stdout.on('end', function(rdata) {
      log('end');
      log(rdata);
      jsondata_load(new_file_path, encoding, send_result);
    });
    terminal.on('exit', function(code) {
      if (code != 0) {
        log('Failed: ' + code);
      }
    });
  } else {
    res.send({ status: 'error', data: 'The file format is not supported.' });
    return;
  }
};

/**
 * Generates list of users
 *
 * @param {*} req Request object
 * @param {*} res Response object
 */
function user_list(req, res) {
  let { dataset_id, user_id, qparams } = prepareParams(req);

  var iDisplayStart = _qv(qparams, 'iDisplayStart');
  if (iDisplayStart == '') iDisplayStart = 0;
  iDisplayStart = parseInt(iDisplayStart);

  var iDisplayLength = _qv(qparams, 'iDisplayLength');
  if (iDisplayLength == '') iDisplayLength = 1000000;
  iDisplayLength = parseInt(iDisplayLength);

  var col_cnt = 0;
  var columns = [];
  var aaData = new Array();
  res.format = 'json';
  var content_str = '';
  var line_cnt = 0;

  var sort = [];
  var s = qparams.sort;
  if (s !== undefined) {
    for (var i = 0; i < s.length; i++) {
      f = [s[i].fieldname, 'desc'];
      if (s[i].dir || s[i].dir == 'true') f[1] = 'asc';
      sort.push(f);
    }
  }

  var options = {
    sort: sort
  };

  let whereClauses = [];
  ['f_username', 'f_fullname', 'f_email', 'f_id'].map(item => {
    let field_filter_value = _qv(qparams, item);
    if (field_filter_value !== '') {
      whereClauses.push(
        `${item.replace('f_', '')} LIKE '%${field_filter_value}%'`
      );
    }
  });

  let sql = 'SELECT id, email, fullname, username FROM "users" ';
  if (whereClauses.length > 0) {
    sql += ` WHERE ${whereClauses.join(' OR ')}`;
  }

  client
    .query(sql)
    .then(result => {
      let aaData = [];
      let ii = 0;
      result.rows.map(u => {
        ii++;
        if (ii >= iDisplayStart && ii < iDisplayStart + iDisplayLength) {
          if (user_id === '') {
            var m = { fullname: u.fullname };
          } else {
            var m = {
              username: u.username,
              email: u.email,
              fullname: u.fullname,
              id: u._id
            };
          }

          aaData.push(m);
        }
      });

      let output = {
        sEcho: parseInt(_qv(qparams, 'sEcho')),
        iTotalRecords: '' + result.rows.length,
        iTotalDisplayRecords: '' + aaData.length,
        aaData: aaData
      };

      res.send(output);
    })
    .catch(err => {
      log(err);
      res.send({ status: 'error', data: err });
    });
}

/**
 * Small services that should reside in separate routes
 *
 * @todo Split in separate routes
 *
 * @param {*} req Request object
 * @param {*} res Response object
 */
function ajaxHubGeothemes(req, res) {
  let _url = url.parse(req.url, true);
  var qparams = req.query;
  var action = _qv(qparams, 'action');

  function codeSampleWithMongoDBIteraction(id, waiting_for_approval) {
    if (waiting_for_approval !== 'TRUE' && waiting_for_approval !== 'FALSE') {
      log('Error - use TRUE or FALSE as second parameter');
      res.send({ status: 'error' });
      return;
    }

    var sqlstr =
      'SELECT id, user_id, access_mode FROM public.table_access_rules WHERE (table_id = ' +
      id +
      ' AND is_deleted = FALSE AND waiting_for_approval = ' +
      waiting_for_approval +
      ');';
    log(sqlstr);
    var cquery = client.query(sqlstr, function(error, result, fields) {
      if (error === null) {
        var user = calipso.db.model('User');
        function recursiveUserAliasing(usersobject, arraytosend) {
          var userslength = usersobject.length;
          if (userslength === 0 || !('user_id' in usersobject[0])) {
            res.send({ status: 'success', data: arraytosend });
          } else {
            if (
              usersobject[0].user_id == 'anonym' ||
              usersobject[0].user_id == 'registered'
            ) {
              arraytosend.push({
                userid: usersobject[0].user_id,
                username: usersobject[0].user_id,
                accesstype: usersobject[0].access_mode,
                rid: usersobject[0].id
              });

              usersobject.splice(0, 1);
              recursiveUserAliasing(usersobject, arraytosend);
            } else {
              user.findOne({ _id: usersobject[0].user_id }, function(err, u) {
                if (u != undefined) {
                  arraytosend.push({
                    userid: u._id,
                    username: u.username,
                    accesstype: usersobject[0].access_mode,
                    rid: usersobject[0].id
                  });

                  usersobject.splice(0, 1);
                  recursiveUserAliasing(usersobject, arraytosend);
                } else {
                  res.send({ status: 'success', data: arraytosend });
                }
              });
            }
          }
        }

        var arraytosend = new Array();
        recursiveUserAliasing(result.rows, arraytosend);
      } else {
        log(error);
        log(sqlstr);
        res.send({ status: 'error' });
      }
    });
  } //end codeSampleWithMongoDBIteraction()

  switch (action) {
    case 'getaccesmodeuserslistfortable':
      var namestartswith = _url.query['name_startsWith'];
      var user = calipso.db.model('User');
      var usernameregexp = new RegExp(namestartswith + '*');

      var combinedresponse = Array();
      user.find({ username: usernameregexp }, function(err, u) {
        if (err === null) {
          var totalusersfound = u.length;
          for (var i = 0; i < totalusersfound; i++) {
            combinedresponse.push({
              id: u[i]._id,
              username: u[i].username,
              fullname: u[i].fullname
            });
          }

          res.send({ status: 'success', data: combinedresponse });
        } else {
          res.send({ status: 'error' });
        }
      });
      break;
    case 'newaccessquery':
      var tableid = _url.query['id'];
      var userid = req.session.user.id;
      var accesstype = 1;

      var checkquery =
        'SELECT id FROM public.table_access_rules WHERE table_id = ' +
        tableid +
        " AND user_id = '" +
        userid +
        "' AND is_deleted = FALSE;";
      var cquery = client.query(checkquery, function(error, rows, fields) {
        if (error === null) {
          if (rows.rows.length > 0) {
            res.send({ status: 'alreadyexists' });
          } else {
            var sqlstr =
              'INSERT INTO public.table_access_rules (table_id, user_id, access_mode, is_deleted, waiting_for_approval) VALUES (' +
              tableid +
              ", '" +
              userid +
              "', " +
              accesstype +
              ', FALSE, TRUE);';
            var cquery = client.query(sqlstr, function(error, rows, fields) {
              if (error === null) {
                res.send({ status: 'success' });
              } else {
                log(error);
                log(sqlstr);
                res.send({ status: 'error' });
              } //end if
            });
          } //end if
        } else {
          log(error);
          log(sqlstr);
          res.send({ status: 'error' });
        } //end if
      });

      break;
    case 'getaccesmodemanageaccessquery':
      var queryaction = _url.query['queryaction'];
      var id = _url.query['id'];
      var accesstype = _url.query['accesstype'];

      if (queryaction === 'true') {
        queryaction = 'TRUE';
        var sqlstr =
          "UPDATE public.table_access_rules SET waiting_for_approval = FALSE, is_deleted = FALSE, access_mode = '" +
          accesstype +
          "' WHERE id = " +
          id +
          ';';
        log(sqlstr);
      } else if (queryaction === 'false') {
        queryaction = 'FALSE';
        var sqlstr =
          'DELETE FROM public.table_access_rules WHERE id = ' + id + ';';
      } else {
        log('Undefined queryaction');
        res.send({ status: 'error' });
      }

      var cquery = client.query(sqlstr, function(error, rows, fields) {
        if (error === null) {
          res.send({ status: 'success' });
        } else {
          log(error);
          log(sqlstr);
          res.send({ status: 'error' });
        }
      });

      break;
    case 'getcurrentaccessqueries':
      var currentuser = '';
      if (
        'session' in req &&
        req.session != undefined &&
        'user' in req.session &&
        req.session.user != undefined &&
        req.session.user != null
      )
        currentuser = req.session.user.id;

      var getuserstablesquery =
        "SELECT id, name FROM public.geo_dataset WHERE created_by = '" +
        currentuser +
        "' AND is_deleted = FALSE;";
      var cquery = client.query(getuserstablesquery, function(
        error,
        rows,
        fields
      ) {
        if (error === null) {
          var totaltables = rows.rows.length;
          var tableset = rows.rows;
          var resultarray = new Array();

          function recursiveTableCheck() {
            if (tableset.length === 0) {
              res.send({ status: 'success', data: resultarray });
            } else {
              var currenttableidobject = tableset.shift();
              var checktablequery =
                'SELECT id FROM public.table_access_rules WHERE (table_id = ' +
                currenttableidobject.id +
                ' AND is_deleted = FALSE AND waiting_for_approval = TRUE);';

              var cquery = client.query(checktablequery, function(
                error,
                rows,
                fields
              ) {
                if (error === null) {
                  if (rows.rows.length > 0) {
                    resultarray.push(currenttableidobject);
                  }

                  recursiveTableCheck();
                } else {
                  log(error);
                  log(checktablequery);
                  res.send({ status: 'error' });
                }
              });
            }
          } //end recursiveTableCheck()

          recursiveTableCheck();
        } else {
          log(error);
          log(getuserstablesquery);
          res.send({ status: 'error' });
        } //end if
      });
      break;
    case 'getaccesmodeuserslistfortablefromdatabase':
      var id = _url.query['id'];
      codeSampleWithMongoDBIteraction(id, 'FALSE');
      break;
    case 'getaccesmodequeries':
      var id = _url.query['id'];
      codeSampleWithMongoDBIteraction(id, 'TRUE');
      break;
  }
}

/**
 * Creates MAP file for the theme field
 *
 * @param {*} req Request object
 * @param {*} res Response object
 */
function createMapForField(req, res) {
  let { user_id } = prepareParams(req);

  var qparams = req.body;
  var metaid = req.body.metaid;
  var unique = _qv(qparams, 'unique');

  var mapstyle = '';
  if ('mapstyle' in qparams) mapstyle = qparams['mapstyle'];

  var req_date;
  if ('req_date' in qparams) req_date = qparams['req_date'];

  var from_color = _qv(qparams, 'from_color');
  var to_color = _qv(qparams, 'to_color');

  var fieldname = _qv(qparams, 'fieldname');
  var map_file_dir = config.modules.geothemes.mapfiledir;
  if (!fs.existsSync(map_file_dir)) {
    log(
      'You should set up mapfiledir in geothemes config, current state of map_file_dir=' +
        this.map_file_dir
    );
    res.send({
      status: 'error',
      wms_link: '',
      msg: 'You should set up mapfiledir in geothemes config!'
    });
    return;
  }

  var g_map = new geo_map(unique, map_file_dir, user_id, client, meta_manager);
  if (is_number(metaid)) {
    get_table_json(req, metaid, function(mdobj) {
      if (mdobj == null) {
        let reply = {
          status: 'error',
          wms_link: wms_link,
          wms_layer_name: wms_layer_name
        };
        log(reply);
        res.send(reply);
        return;
      }

      var replyarr = new Array();
      var wms_link = '';
      var selection_wms_link = '';
      var wms_layer_name = '';
      var static_map = false;
      var minZoom = mdobj.minZoom;
      var maxZoom = mdobj.maxZoom;
      var opacity = mdobj.opacity;
      if (opacity === undefined) opacity = 1;

      if (
        mdobj.wms_link != undefined &&
        mdobj.wms_link != null &&
        mdobj.wms_link != '' &&
        user_id == ''
      ) {
        wms_link = mdobj.wms_link;
        wms_layer_name = mdobj.wms_layer_name;
        static_map = true;

        res.send({
          status: 'success',
          wms_link: toMapserverPath(wms_link),
          wms_layer_name: wms_layer_name,
          selection_wms_link: toMapserverPath(selection_wms_link),
          static_map: static_map,
          minZoom: minZoom,
          maxZoom: maxZoom,
          opacity: opacity
        });
      } else {
        var selection_array = _qv(qparams, 'selected_array');

        function write_mapfile(fieldname, query) {
          g_map.createMapForDB(
            fieldname,
            mapstyle,
            mdobj,
            selection_array,
            query,
            function(error, wms_link) {
              selection_wms_link = wms_link;
              wms_layer_name = fieldname;

              let reply;
              if (error)
                reply = {
                  status: 'error',
                  wms_link: toMapserverPath(wms_link),
                  wms_layer_name: wms_layer_name
                };
              else
                reply = {
                  status: 'success',
                  wms_link: toMapserverPath(wms_link),
                  wms_layer_name: wms_layer_name,
                  selection_wms_link: toMapserverPath(selection_wms_link),
                  static_map: static_map,
                  minZoom: minZoom,
                  maxZoom: maxZoom,
                  opacity: opacity
                };

              res.send(reply);
            }
          );
        }

        var g_table = new geo_table(
          user_id,
          mdobj.dataset_id,
          meta_manager,
          mdobj,
          { convertGeometries: false }
        );

        var sLimit = '';
        var s_order = '';
        if (_qv(qparams, 'mapDisplayLength') != '') {
          iDisplayStart = _qv(qparams, 'iDisplayStart');
          iDisplayLength = _qv(qparams, 'iDisplayLength');
          if (iDisplayStart != '' && iDisplayLength != '') {
            sLimit = 'LIMIT ' + iDisplayLength + ' OFFSET ' + iDisplayStart;
          } else {
            sLimit = 'LIMIT 10000 OFFSET 0';
          }

          for (var i = 0; i < mdobj.columns.length; i++) {
            fn = _qv(qparams, 'calc_' + mdobj.columns[i].fieldname);
            if (fn.indexOf('distance') != -1) {
              s_order = g_table.buildselect(
                [mdobj.columns[i].fieldname],
                qparams,
                []
              );
              if (s_order.indexOf(' AS ') != -1)
                s_order = s_order.substring(0, s_order.indexOf(' AS '));
              s_order += ' ASC ';
              qparams['calc_' + mdobj.columns[i].fieldname] = '';
            }
          }
        }

        var column = getcolumn(fieldname, mdobj);
        if (column) {
          var pfld;
          if (
            column.parentfield !== undefined &&
            column.parentfield != '' &&
            column.parentfield != 'id' &&
            pfld != null &&
            pfld.widget.properties.reltype != 'bijection'
          ) {
            qparams['g_id'] = 'count';
            qparams['g_' + pfld.fieldname] = 'groupby';
            sql = g_table.documentquery(
              ['id', pfld.fieldname],
              qparams,
              sLimit,
              function(error, result) {
                if (error === null) {
                  if (result.aaData.length > 0) {
                    maxcnt = 0;
                    for (var l = 0; l < result.aaData.length; l++) {
                      if (result.aaData[l].id > maxcnt)
                        maxcnt = result.aaData[l].id;
                    }

                    mapstyle = get_map_range_style(
                      'id',
                      column.type,
                      0,
                      maxcnt
                    );
                    console.log(mapstyle);
                    subsql = g_table.documentquery(
                      ['id', pfld.fieldname],
                      qparams,
                      '',
                      undefined
                    );
                    sql =
                      'select m.id AS id, g.' +
                      column.sqlname +
                      ' AS ' +
                      column.fieldname +
                      ' from ' +
                      column.sqltablename +
                      ' g, (' +
                      subsql +
                      ') m where m.' +
                      pfld.fieldname +
                      '=g.id';

                    write_mapfile(pfld.fieldname, sql);
                  }
                } else {
                  console.log(error);
                }
              }
            );
          } else {
            let sql = g_table.documentquery([], qparams, '', undefined);
            if (sLimit != '') {
              if (s_order != '') sql = sql + ' ORDER BY ' + s_order;
              sql = sql + ' ' + sLimit;
            }

            write_mapfile(column.fieldname, sql);
          }
        }
      }
    });
  } else {
    var filesource = getpath(req, metaid);
    g_map.createMapForFile(
      filesource,
      mapstyle,
      from_color,
      to_color,
      req_date,
      function(result) {
        if (result.status == 'error')
          reply = {
            status: 'error',
            wms_link: toMapserverPath(result.wms_link)
          };
        else
          reply = {
            status: 'success',
            wms_link: toMapserverPath(result.wms_link),
            type: result.type
          };

        res.send(reply);
      }
    );
  }
}

/**
 * Creates MAP file for the file
 *
 * @param {*} req Request object
 * @param {*} res Response object
 */
function createMapForFile(req, res) {
  let { user_id } = prepareParams(req);
  var filehash = req.body.filehesh;

  let dir = filehash.slice(3);
  dir = dir.replace(/-/g, '+');
  dir = dir.replace(/_/g, '/');
  dir = dir.replace(/\./g, '=');
  var b = new Buffer(dir, 'base64');
  let filesource = config.modules.rfm.mainrootdir + '/' + b.toString();

  var unique = req.body.unique;
  var filepath = config.modules.geothemes.mapfiledir + '/';

  var mapfilepath = filepath + unique + '.map';
  if (
    filesource
      .substring(filesource.lastIndexOf('.') + 1, filesource.length)
      .toUpperCase() == 'MTIF'
  ) {
    mapfilepath = filepath + 'UNIQUE_DUMMY' + '.map';
  }

  var g_map = new geo_map(
    unique,
    config.modules.geothemes.mapfiledir,
    user_id,
    client,
    meta_manager
  );
  g_map.createMapForFile(
    filesource,
    '',
    hexToRGB(req.body.from_color),
    hexToRGB(req.body.to_color),
    false,
    mapfilesobject => {
      res.send({
        status: 'success',
        mapfilesobject
      });
    }
  );
}

/**
 * Fetching stored layers
 *
 * @param {*} req Request object
 * @param {*} res Response object
 */
function layers(req, res) {
  var qparams = req.query;
  for (var attrname in req.body) {
    qparams[attrname] = req.body[attrname];
  }

  var id = _qv(qparams, 'id');
  if (id == '' || id == '#') id = '0';

  try {
    var finalsql = 'select * from public.dataset_tree where parent_id=' + id;
    var query = client.query(finalsql, function(error, sqlresult, unofields) {
      if (id == '0') id = '#';

      if (error === null) {
        var output = new Array();
        for (var i = 0; i < sqlresult.rows.length; i++) {
          var f = {
            id: '' + sqlresult.rows[i].id,
            parent: id,
            data: sqlresult.rows[i].name,
            children: true,
            state: 'closed',
            attr: {
              id: '' + sqlresult.rows[i].id,
              rel: 'folder'
            }
          };

          if (sqlresult.rows[i].node_type == 1) {
            f.type = 'file';
          } else {
            f.type = 'map';
          }

          if (
            sqlresult.rows[i].dataset_id != null &&
            sqlresult.rows[i].dataset_id != undefined
          ) {
            f.attr.dataset_id = sqlresult.rows[i].dataset_id;
          } else {
            f.attr.dataset_id = -1;
          }

          output.push(f);
        }

        log(output);
        res.send(output);
      } else {
        log(error);
        res.send({ error: error });
      }
    });
  } catch (e) {
    log('error');
    res.send({ error: 'something is wrong' });
  }
}

export default {
  bldata_list,
  bldata_add,
  bldata_update,
  bldata_delete,
  bldata_import,
  bldata_clearTable,
  filedata_list,
  filedata_meta,
  filedata_info,
  user_list,
  genJSON,
  gettablesGeothemes,
  processTableJson,
  droptable,
  bl_file,
  ajaxHubGeothemes,
  createMapForField,
  createMapForFile,
  layers
};
