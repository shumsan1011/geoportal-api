import jwt from 'jsonwebtoken';
import config from '../../config/config';
import crypto from '../utils/crypto';
import db from '../lib/db';

const client = db.getClient();

/**
 * Returns jwt token if valid username and password is provided
 * @param req
 * @param res
 * @returns {*}
 */
function login(req, res) {
  const hashedPassword = crypto.hashmd5(req.body.password, config.passwordSalt);
  client.query(`SELECT * FROM users WHERE username = '${req.body.username}' AND password = '${hashedPassword}';`).then(result => {
    if (result.rowCount === 1) {
      let user = result.rows.pop();

      const accessToken = jwt.sign({
        id: user.id,
        username: user.username
      }, config.jwtSecret);

      const refreshToken = jwt.sign({
        refresh: true,
        id: user.id,
        email: user.email,
        language: user.language,
        username: user.username
      }, config.jwtSecret);

      res.json({ accessToken, refreshToken });
    } else {
      res.boom.unauthorized('Invalid credentials were provided');
    }
  });
}

/**
 * This is a protected route. Will return random number only if jwt token is provided in header.
 * @param req
 * @param res
 * @returns {*}
 */
function getRandomNumber(req, res) {
  return res.json({
    decoded: req.decoded,
    num: Math.random() * 100
  });
}

export default { login, getRandomNumber };
