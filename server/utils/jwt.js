const jwt = require('jsonwebtoken');

const publicRoutes = [
  '/api/auth/login',
  '/api/geoThemes/user_list/list',
  '/api/geoThemes/dataset/list',
  '/api/geoThemes/dataset/add',
  '/api/health-check',
  '/api/users/activate',
  '/api/users/reset-password'
];

/**
 * Checks if the valid authentication token is provided.
 */
module.exports.checkAuthToken = (req, res, next) => {
  const token = req.headers.Authorization || req.headers.authorization;
  let oneOfPublicRoutes = false;
  publicRoutes.map(item => {
    if (req.originalUrl.startsWith(item)) {
      oneOfPublicRoutes = true;
      return false;
    }
  });

  if (oneOfPublicRoutes) {
    if (token) {
      jwt.verify(token.replace('Bearer ', ''), process.env.JWT_SECRET, (err, decoded) => {
        if (!err) {
          req.user = decoded;
        }

        return next();
      });
    } else {
      return next();
    }
  } else {
    if (token) {
      jwt.verify(token.replace('Bearer ', ''), process.env.JWT_SECRET, (err, decoded) => {
        if (err) {
          res.json({
            success: false,
            message: 'Failed to authenticate.'
          });
        } else {
          req.user = decoded;
          return next();
        }
      });
    } else {
      return res.status(403).send({
        success: false,
        message: 'Authentication is required to access this route.'
      });
    }
  }
};
