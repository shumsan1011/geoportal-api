const crypto = require('crypto');

/**
 * Create a md5 hash from string and key / salt
 */
module.exports.hashmd5 = (string, salt) => {
  const hashedsalt = crypto.createHash('md5').update(salt).digest('hex');
  const hashedpassword = crypto.createHash('md5').update(string).digest('hex');
  const overallhash = crypto.createHash('md5').update(hashedsalt + hashedpassword).digest('hex');
  return overallhash;
};
