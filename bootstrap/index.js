require('dotenv').config();

import db from './../server/lib/db';
import { meta_manager } from './../server/lib/geoThemes/geo_meta_table';
import colors from 'colors';
import { setTimeout } from 'timers';

const ENV_ID_PRODUCTION = 'production';
const ENV_ID_TEST = 'test';
const DROP_TABLES_TIMEOUT = 2000;

const client = db.getClient();

/**
 * Bootstrapping the Geoportal REST API application.
 * 
 * The bootstrapping is the process of deploying the required database structures and alongside data, as well
 * as performing other database checks.
 * The bootstrapping process depends on the current environment:
 * - NODE_ENV=[production|development|..]: database checkups are performed in read-only mode;
 * - NODE_ENV=test: database is completely destroyed in order to create fresh environment;
 */

// Checking the database name not to erase the production database occasionally
const TESTING_DATABASE_NAME = 'geoportaltest_new';

const _log = (msg) => {
  console.log('Geoportal bootstrap: '.magenta + msg);
};

const bootstrap = (app) => {
  const bootstrapResult = new Promise((resolve, reject) => {
    _log('started'.green);
    const performFurtherBootstrap = () => {
      if (process.env.NODE_ENV !== ENV_ID_PRODUCTION) {
        _log('checking PostGIS version');

        client.query('SELECT PostGIS_full_version();').then((result) => {
          if (result.rowCount === 1) {
            _log(result.rows[0].postgis_full_version);

            /**
             * Initialize tables that are required for API
             * to work in the database using the geoThemes module
             */
            const initializeTables = (forceTableDeletion = false) => {
              meta_manager.init(forceTableDeletion).then(() => {
                _log('meta manager was initialized');
                _log('finished successfully'.green);
                resolve(app);
              }, () => {
                throw 'Unable to initialize meta manager';
              });
            };

            if (process.env.NODE_ENV === ENV_ID_TEST) {
              _log(`about to drop all tables in ${DROP_TABLES_TIMEOUT} ms`.red);
              setTimeout(() => {
                initializeTables(true);
              }, DROP_TABLES_TIMEOUT);
            } else {
              initializeTables();
            }
          } else {
            throw 'Unable to retrieve the PostGIS version';
          }         
        });
      } else {
        throw 'Not implemented yet';
      }
    };

    let client = db.getClient();
    if (client._connected) {
      performFurtherBootstrap();
    } else {
      client.connect().then(() => {
        performFurtherBootstrap();
      });
    }
  });

  return bootstrapResult;
}

export default bootstrap;
