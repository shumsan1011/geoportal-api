const Joi = require('joi');

require('dotenv').config();

const envVarsSchema = Joi.object({
  PORT: Joi.number().default(3000),
  JWT_SECRET: Joi.string().required().description('JWT Secret required to sign'),
  PASSWORD_SALT: Joi.string().required(),
  NODE_ENV: Joi.string().required(),
  DB_HOST: Joi.string().required(),
  DB_NAME: Joi.string().required(),
  DB_USER: Joi.string().required(),
  DB_PASSWORD: Joi.string().required(),
  DB_DIALECT: Joi.string().required()
}).unknown().required();

const { error, value: envVars } = Joi.validate(process.env, envVarsSchema);
if (error) {
  throw new Error(`Config validation error: ${error.message}`);
}

const config = {
  env: envVars.NODE_ENV,
  port: envVars.PORT,
  passwordSalt: envVars.PASSWORD_SALT,
  jwtSecret: envVars.JWT_SECRET,
  systemAccountId: 2,
  // URL of REST API
  server: {
    url: 'http://84.237.16.41:3333'
  },
  // Used for generating links for user activation
  primaryClientURL: 'http://84.237.16.41:4200',
  db: {
    host: envVars.DB_HOST,
    user: envVars.DB_USER,
    password: envVars.DB_PASSWORD,
    name: envVars.DB_NAME,
    dialect: envVars.DB_DIALECT
  },
  mail: {
    from: 'geoportal@alexshumilov.ru',
    smtp: {
      host: envVars.GEOPORTAL_MAIL_SMTP_HOST,
      port: 25,
      secure: false,
      auth: {
        user: envVars.GEOPORTAL_MAIL_USER,
        pass: envVars.GEOPORTAL_MAIL_PASSWORD
      }
    }
  },
  modules: {
    geothemes: {
      remoteHostMapFilesPath: '/s/temp/mapfiles',
      mapfiledir: 'S:/temp/mapfiles',
      mapserver_path: 'http://84.237.16.55/cgi-bin/mapserv',
      widgetdirectory: '\\themes\\core\\cleanslate\\public\\js\\widgets'
    },
    rfm: {
      mainrootdir: 'S:/users',
      ftpsconfigs: 'C:\\Program Files (x86)\\FileZilla Server/FileZilla Server.xml'
    },
    themeeditor: {
      mapfilestore: 'S:/temp/mapfiles'
    },
    servicesManager: {
      executablepath: __dirname + '/../server/lib/servicesManager/bin/executer.exe',
      cwd: __dirname + '/../server/lib/servicesManager/bin',
      verboseoutput: true
    },
    filelink: {
      maxlinklifetime: '0',
      maxfetchqueries: '0',
      temporaryfilesfolder: 'S:/temp/shared'
    }
  }
};

module.exports = config;
