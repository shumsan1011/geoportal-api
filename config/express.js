import express from 'express';
import logger from 'morgan';
import boom from 'express-boom';
import bodyParser from 'body-parser';
import xmlparser from 'express-xml-bodyparser';
import cookieParser from 'cookie-parser';
import compress from 'compression';
import methodOverride from 'method-override';
import cors from 'cors';
import httpStatus from 'http-status';
import expressWinston from 'express-winston';
import helmet from 'helmet';
import winston from 'winston';
import routes from '../server/routes/index';
import config from './config';

const app = express();

// parse body params and attache them to req.body
app.use(bodyParser.json());
app.use(xmlparser());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(cookieParser());
app.use(compress());
app.use(methodOverride());

// secure apps by setting various HTTP headers
app.use(helmet());

// enable CORS - Cross Origin Resource Sharing
app.use(cors());

// enable Boom replies
app.use(boom());

// wrapper for Boom replies that accept only APIError object
app.use((req, res, next) =>{
  res.apiError = (apiError, description = false) => {
    res.boom.badRequest(apiError.message, {
      errorId: apiError.id,
      description
    });
  };

  next();
});

expressWinston.requestWhitelist.push('body');

// Enable detailed API logging in dev env
if (config.env === 'production') {
  app.use(expressWinston.logger({
    transports: [
      new winston.transports.File({
        filename: 'logs/combined.log',
        level: 'info'
      }),
      new winston.transports.Console({
        json: false,
        colorize: true
      })
    ]
  }));
}

// Mount all routes on /api path
app.use('/api', routes);
app.use((req, res) =>{
  res.boom.notFound();
});

// Error handler
if (config.env === 'production') {
  const logger = expressWinston.errorLogger({
    transports: [
      new winston.transports.File({
        filename: 'logs/error.log'
      }),
      new winston.transports.Console({
        json: true,
        colorize: true
      })
    ],
    dumpExceptions: true,
    showStack: true,
    prettyPrint: true
  });

  app.use(logger);

  process.on('uncaughtException', (error) => {
    const localLogger = new (winston.Logger)({
      transports: [
        new winston.transports.File({
          filename: 'logs/error.log'
        }),
      ],
      level: 'error'
    });

    console.log(error);
    localLogger.error(error);
    setTimeout(() => {
      process.exit(1);
    }, 1000);
  });
} else if (config.env === 'test' || config.env === 'development') {
  app.use((err, req, res, next) => {
    console.log(err);
    next(err, req, res, next);
  });
}

export default app;
