import config from './config/config';
import expressApp from './config/express';
import bootstrap from './bootstrap';

if (!module.parent) {
  bootstrap().then(() => {
    expressApp.listen(config.port, () => {
      console.info(`Server started on port ${config.port} (${config.env})`);
    });
  }).catch(() => {
    throw 'Unable to start the server';
  });
}

const app = () => bootstrap(expressApp);

export default app;